package org.nrg.dcm.id;

import org.nrg.dcm.ChainExtractor;
import org.nrg.dcm.id.ClassicDicomObjectIdentifier;
import org.nrg.dcm.id.CompositeDicomObjectIdentifier;
import org.nrg.dcm.id.CCIRAttributeExtractor;


public final class CCIRDicomObjectIdentifier extends CompositeDicomObjectIdentifier {
	
	CCIRDicomObjectIdentifier(CCIRProjectIdentifier   project, 
							  CCIRAttributeExtractor  subject, 
							  CCIRAttributeExtractor  session) {
		super(project, subject, session, new ChainExtractor(ClassicDicomObjectIdentifier.getAAExtractors()));
	}
}
