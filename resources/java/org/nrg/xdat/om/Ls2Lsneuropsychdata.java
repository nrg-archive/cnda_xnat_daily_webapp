/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lsneuropsychdata extends BaseLs2Lsneuropsychdata {

	public Ls2Lsneuropsychdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lsneuropsychdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsneuropsychdata(UserI user)
	 **/
	public Ls2Lsneuropsychdata()
	{}

	public Ls2Lsneuropsychdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
