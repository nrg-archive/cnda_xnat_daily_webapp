// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Aug 04 12:50:13 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class PipePipelinedetailsElement extends BasePipePipelinedetailsElement {

	public PipePipelinedetailsElement(ItemI item)
	{
		super(item);
	}

	public PipePipelinedetailsElement(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePipePipelinedetailsElement(UserI user)
	 **/
	public PipePipelinedetailsElement()
	{}

	public PipePipelinedetailsElement(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
