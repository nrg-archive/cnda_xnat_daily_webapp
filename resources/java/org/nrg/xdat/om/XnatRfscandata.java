/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatRfscandata extends BaseXnatRfscandata {

	public XnatRfscandata(ItemI item)
	{
		super(item);
	}

	public XnatRfscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatRfscandata(UserI user)
	 **/
	public XnatRfscandata()
	{}

	public XnatRfscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
