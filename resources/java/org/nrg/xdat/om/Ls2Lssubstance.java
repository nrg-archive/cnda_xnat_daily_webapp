/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lssubstance extends BaseLs2Lssubstance {

	public Ls2Lssubstance(ItemI item)
	{
		super(item);
	}

	public Ls2Lssubstance(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lssubstance(UserI user)
	 **/
	public Ls2Lssubstance()
	{}

	public Ls2Lssubstance(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
