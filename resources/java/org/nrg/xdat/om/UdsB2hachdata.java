/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB2hachdata extends BaseUdsB2hachdata {

	public UdsB2hachdata(ItemI item)
	{
		super(item);
	}

	public UdsB2hachdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB2hachdata(UserI user)
	 **/
	public UdsB2hachdata()
	{}

	public UdsB2hachdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
