/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class KblackVasdata extends BaseKblackVasdata {

	public KblackVasdata(ItemI item)
	{
		super(item);
	}

	public KblackVasdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseKblackVasdata(UserI user)
	 **/
	public KblackVasdata()
	{}

	public KblackVasdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
