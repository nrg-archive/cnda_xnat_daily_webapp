/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class IqIqassessmentdata extends BaseIqIqassessmentdata {

	public IqIqassessmentdata(ItemI item)
	{
		super(item);
	}

	public IqIqassessmentdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIqIqassessmentdata(UserI user)
	 **/
	public IqIqassessmentdata()
	{}

	public IqIqassessmentdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
