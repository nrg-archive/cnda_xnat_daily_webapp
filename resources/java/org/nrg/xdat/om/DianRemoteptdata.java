/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianRemoteptdata extends BaseDianRemoteptdata {

	public DianRemoteptdata(ItemI item)
	{
		super(item);
	}

	public DianRemoteptdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianRemoteptdata(UserI user)
	 **/
	public DianRemoteptdata()
	{}

	public DianRemoteptdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
