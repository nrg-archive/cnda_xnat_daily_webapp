/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatAybocsdata extends BaseXnatAybocsdata {

	public XnatAybocsdata(ItemI item)
	{
		super(item);
	}

	public XnatAybocsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatAybocsdata(UserI user)
	 **/
	public XnatAybocsdata()
	{}

	public XnatAybocsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
