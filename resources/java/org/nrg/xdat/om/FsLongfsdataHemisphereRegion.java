/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsLongfsdataHemisphereRegion extends BaseFsLongfsdataHemisphereRegion {

	public FsLongfsdataHemisphereRegion(ItemI item)
	{
		super(item);
	}

	public FsLongfsdataHemisphereRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsLongfsdataHemisphereRegion(UserI user)
	 **/
	public FsLongfsdataHemisphereRegion()
	{}

	public FsLongfsdataHemisphereRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
