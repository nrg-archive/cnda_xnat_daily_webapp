/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfAdverseevent extends BaseSfAdverseevent {

	public SfAdverseevent(ItemI item)
	{
		super(item);
	}

	public SfAdverseevent(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfAdverseevent(UserI user)
	 **/
	public SfAdverseevent()
	{}

	public SfAdverseevent(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
