/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class ClinPregdataPreg extends BaseClinPregdataPreg {

	public ClinPregdataPreg(ItemI item)
	{
		super(item);
	}

	public ClinPregdataPreg(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseClinPregdataPreg(UserI user)
	 **/
	public ClinPregdataPreg()
	{}

	public ClinPregdataPreg(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
