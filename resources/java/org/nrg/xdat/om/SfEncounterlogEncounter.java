/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SfEncounterlogEncounter extends BaseSfEncounterlogEncounter {

	public SfEncounterlogEncounter(ItemI item)
	{
		super(item);
	}

	public SfEncounterlogEncounter(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSfEncounterlogEncounter(UserI user)
	 **/
	public SfEncounterlogEncounter()
	{}

	public SfEncounterlogEncounter(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
