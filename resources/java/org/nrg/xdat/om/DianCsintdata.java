/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianCsintdata extends BaseDianCsintdata {

	public DianCsintdata(ItemI item)
	{
		super(item);
	}

	public DianCsintdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianCsintdata(UserI user)
	 **/
	public DianCsintdata()
	{}

	public DianCsintdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
