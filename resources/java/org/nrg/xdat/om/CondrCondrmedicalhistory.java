/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CondrCondrmedicalhistory extends BaseCondrCondrmedicalhistory {

	public CondrCondrmedicalhistory(ItemI item)
	{
		super(item);
	}

	public CondrCondrmedicalhistory(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrCondrmedicalhistory(UserI user)
	 **/
	public CondrCondrmedicalhistory()
	{}

	public CondrCondrmedicalhistory(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
