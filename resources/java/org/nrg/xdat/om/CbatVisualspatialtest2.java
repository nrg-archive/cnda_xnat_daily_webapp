/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatVisualspatialtest2 extends BaseCbatVisualspatialtest2 {

	public CbatVisualspatialtest2(ItemI item)
	{
		super(item);
	}

	public CbatVisualspatialtest2(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatVisualspatialtest2(UserI user)
	 **/
	public CbatVisualspatialtest2()
	{}

	public CbatVisualspatialtest2(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
