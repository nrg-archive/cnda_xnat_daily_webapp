/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class Ls2Lshealthdata extends BaseLs2Lshealthdata {

	public Ls2Lshealthdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lshealthdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lshealthdata(UserI user)
	 **/
	public Ls2Lshealthdata()
	{}

	public Ls2Lshealthdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
