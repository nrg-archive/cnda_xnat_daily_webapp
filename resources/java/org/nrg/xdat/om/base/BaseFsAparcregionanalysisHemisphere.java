/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsAparcregionanalysisHemisphere extends AutoFsAparcregionanalysisHemisphere {

	public BaseFsAparcregionanalysisHemisphere(ItemI item)
	{
		super(item);
	}

	public BaseFsAparcregionanalysisHemisphere(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAparcregionanalysisHemisphere(UserI user)
	 **/
	public BaseFsAparcregionanalysisHemisphere()
	{}

	public BaseFsAparcregionanalysisHemisphere(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
