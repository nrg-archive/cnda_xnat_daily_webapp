/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseIpipIpipcsdata extends AutoIpipIpipcsdata {

	public BaseIpipIpipcsdata(ItemI item)
	{
		super(item);
	}

	public BaseIpipIpipcsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIpipIpipcsdata(UserI user)
	 **/
	public BaseIpipIpipcsdata()
	{}

	public BaseIpipIpipcsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
