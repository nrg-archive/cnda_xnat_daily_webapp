/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lssubstance extends AutoLs2Lssubstance {

	public BaseLs2Lssubstance(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lssubstance(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lssubstance(UserI user)
	 **/
	public BaseLs2Lssubstance()
	{}

	public BaseLs2Lssubstance(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
