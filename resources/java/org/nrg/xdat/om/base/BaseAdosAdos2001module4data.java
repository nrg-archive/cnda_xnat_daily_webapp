/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseAdosAdos2001module4data extends AutoAdosAdos2001module4data {

	public BaseAdosAdos2001module4data(ItemI item)
	{
		super(item);
	}

	public BaseAdosAdos2001module4data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdosAdos2001module4data(UserI user)
	 **/
	public BaseAdosAdos2001module4data()
	{}

	public BaseAdosAdos2001module4data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
