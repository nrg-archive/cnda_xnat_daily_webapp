/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lsillness extends AutoLs2Lsillness {

	public BaseLs2Lsillness(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lsillness(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lsillness(UserI user)
	 **/
	public BaseLs2Lsillness()
	{}

	public BaseLs2Lsillness(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
