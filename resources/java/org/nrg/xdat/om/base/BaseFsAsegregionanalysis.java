/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseFsAsegregionanalysis extends AutoFsAsegregionanalysis {

	public BaseFsAsegregionanalysis(ItemI item)
	{
		super(item);
	}

	public BaseFsAsegregionanalysis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAsegregionanalysis(UserI user)
	 **/
	public BaseFsAsegregionanalysis()
	{}

	public BaseFsAsegregionanalysis(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
