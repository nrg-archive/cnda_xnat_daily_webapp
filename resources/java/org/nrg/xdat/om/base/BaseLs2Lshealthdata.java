/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseLs2Lshealthdata extends AutoLs2Lshealthdata {

	public BaseLs2Lshealthdata(ItemI item)
	{
		super(item);
	}

	public BaseLs2Lshealthdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseLs2Lshealthdata(UserI user)
	 **/
	public BaseLs2Lshealthdata()
	{}

	public BaseLs2Lshealthdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
