/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCondrMetsLesioncollection extends AutoCondrMetsLesioncollection {

	public BaseCondrMetsLesioncollection(ItemI item)
	{
		super(item);
	}

	public BaseCondrMetsLesioncollection(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrMetsLesioncollection(UserI user)
	 **/
	public BaseCondrMetsLesioncollection()
	{}

	public BaseCondrMetsLesioncollection(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
