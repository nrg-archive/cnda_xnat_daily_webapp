/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCbatVisualspatialtest1 extends AutoCbatVisualspatialtest1 {

	public BaseCbatVisualspatialtest1(ItemI item)
	{
		super(item);
	}

	public BaseCbatVisualspatialtest1(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatVisualspatialtest1(UserI user)
	 **/
	public BaseCbatVisualspatialtest1()
	{}

	public BaseCbatVisualspatialtest1(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
