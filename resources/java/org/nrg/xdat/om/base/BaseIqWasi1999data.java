/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseIqWasi1999data extends AutoIqWasi1999data {

	public BaseIqWasi1999data(ItemI item)
	{
		super(item);
	}

	public BaseIqWasi1999data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIqWasi1999data(UserI user)
	 **/
	public BaseIqWasi1999data()
	{}

	public BaseIqWasi1999data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
