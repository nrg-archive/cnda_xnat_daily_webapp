/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianConteligdata extends XnatSubjectassessordata implements org.nrg.xdat.model.DianConteligdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianConteligdata.class);
	public static String SCHEMA_ELEMENT_NAME="dian:conteligData";

	public AutoDianConteligdata(ItemI item)
	{
		super(item);
	}

	public AutoDianConteligdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianConteligdata(UserI user)
	 **/
	public AutoDianConteligdata(){}

	public AutoDianConteligdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:conteligData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.DianConteligdataContelig> _Conteliglist_contelig =null;

	/**
	 * conteligList/contelig
	 * @return Returns an List of org.nrg.xdat.om.DianConteligdataContelig
	 */
	public <A extends org.nrg.xdat.model.DianConteligdataConteligI> List<A> getConteliglist_contelig() {
		try{
			if (_Conteliglist_contelig==null){
				_Conteliglist_contelig=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("conteligList/contelig"));
			}
			return (List<A>) _Conteliglist_contelig;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.DianConteligdataContelig>();}
	}

	/**
	 * Sets the value for conteligList/contelig.
	 * @param v Value to Set.
	 */
	public void setConteliglist_contelig(ItemI v) throws Exception{
		_Conteliglist_contelig =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/conteligList/contelig",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/conteligList/contelig",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * conteligList/contelig
	 * Adds org.nrg.xdat.model.DianConteligdataConteligI
	 */
	public <A extends org.nrg.xdat.model.DianConteligdataConteligI> void addConteliglist_contelig(A item) throws Exception{
	setConteliglist_contelig((ItemI)item);
	}

	/**
	 * Removes the conteligList/contelig of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeConteliglist_contelig(int index) throws java.lang.IndexOutOfBoundsException {
		_Conteliglist_contelig =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/conteligList/contelig",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianConteligdata> getAllDianConteligdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianConteligdata> al = new ArrayList<org.nrg.xdat.om.DianConteligdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianConteligdata> getDianConteligdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianConteligdata> al = new ArrayList<org.nrg.xdat.om.DianConteligdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianConteligdata> getDianConteligdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianConteligdata> al = new ArrayList<org.nrg.xdat.om.DianConteligdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianConteligdata getDianConteligdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:conteligData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianConteligdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //conteligList/contelig
	        for(org.nrg.xdat.model.DianConteligdataConteligI childConteliglist_contelig : this.getConteliglist_contelig()){
	            if (childConteliglist_contelig!=null){
	              for(ResourceFile rf: ((DianConteligdataContelig)childConteliglist_contelig).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("conteligList/contelig[" + ((DianConteligdataContelig)childConteliglist_contelig).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("conteligList/contelig/" + ((DianConteligdataContelig)childConteliglist_contelig).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
