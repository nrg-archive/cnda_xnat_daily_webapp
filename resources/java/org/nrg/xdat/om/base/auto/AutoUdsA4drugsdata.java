/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsA4drugsdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsA4drugsdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsA4drugsdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:a4drugsData";

	public AutoUdsA4drugsdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsA4drugsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsA4drugsdata(UserI user)
	 **/
	public AutoUdsA4drugsdata(){}

	public AutoUdsA4drugsdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:a4drugsData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs> _A4drugslist_a4drugs =null;

	/**
	 * a4drugsList/a4drugs
	 * @return Returns an List of org.nrg.xdat.om.UdsA4drugsdataA4drugs
	 */
	public <A extends org.nrg.xdat.model.UdsA4drugsdataA4drugsI> List<A> getA4drugslist_a4drugs() {
		try{
			if (_A4drugslist_a4drugs==null){
				_A4drugslist_a4drugs=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("a4drugsList/a4drugs"));
			}
			return (List<A>) _A4drugslist_a4drugs;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.UdsA4drugsdataA4drugs>();}
	}

	/**
	 * Sets the value for a4drugsList/a4drugs.
	 * @param v Value to Set.
	 */
	public void setA4drugslist_a4drugs(ItemI v) throws Exception{
		_A4drugslist_a4drugs =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/a4drugsList/a4drugs",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/a4drugsList/a4drugs",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * a4drugsList/a4drugs
	 * Adds org.nrg.xdat.model.UdsA4drugsdataA4drugsI
	 */
	public <A extends org.nrg.xdat.model.UdsA4drugsdataA4drugsI> void addA4drugslist_a4drugs(A item) throws Exception{
	setA4drugslist_a4drugs((ItemI)item);
	}

	/**
	 * Removes the a4drugsList/a4drugs of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeA4drugslist_a4drugs(int index) throws java.lang.IndexOutOfBoundsException {
		_A4drugslist_a4drugs =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/a4drugsList/a4drugs",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsA4drugsdata> getAllUdsA4drugsdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA4drugsdata> al = new ArrayList<org.nrg.xdat.om.UdsA4drugsdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA4drugsdata> getUdsA4drugsdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA4drugsdata> al = new ArrayList<org.nrg.xdat.om.UdsA4drugsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsA4drugsdata> getUdsA4drugsdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsA4drugsdata> al = new ArrayList<org.nrg.xdat.om.UdsA4drugsdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsA4drugsdata getUdsA4drugsdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:a4drugsData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsA4drugsdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //a4drugsList/a4drugs
	        for(org.nrg.xdat.model.UdsA4drugsdataA4drugsI childA4drugslist_a4drugs : this.getA4drugslist_a4drugs()){
	            if (childA4drugslist_a4drugs!=null){
	              for(ResourceFile rf: ((UdsA4drugsdataA4drugs)childA4drugslist_a4drugs).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("a4drugsList/a4drugs[" + ((UdsA4drugsdataA4drugs)childA4drugslist_a4drugs).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("a4drugsList/a4drugs/" + ((UdsA4drugsdataA4drugs)childA4drugslist_a4drugs).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
