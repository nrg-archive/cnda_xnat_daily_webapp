/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoIqIqassessmentdata extends XnatSubjectassessordata implements org.nrg.xdat.model.IqIqassessmentdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoIqIqassessmentdata.class);
	public static String SCHEMA_ELEMENT_NAME="iq:iqAssessmentData";

	public AutoIqIqassessmentdata(ItemI item)
	{
		super(item);
	}

	public AutoIqIqassessmentdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoIqIqassessmentdata(UserI user)
	 **/
	public AutoIqIqassessmentdata(){}

	public AutoIqIqassessmentdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "iq:iqAssessmentData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Fsiq=null;

	/**
	 * @return Returns the fsiq.
	 */
	public Integer getFsiq() {
		try{
			if (_Fsiq==null){
				_Fsiq=getIntegerProperty("fsiq");
				return _Fsiq;
			}else {
				return _Fsiq;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for fsiq.
	 * @param v Value to Set.
	 */
	public void setFsiq(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/fsiq",v);
		_Fsiq=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Verbaliq=null;

	/**
	 * @return Returns the verbalIq.
	 */
	public Integer getVerbaliq() {
		try{
			if (_Verbaliq==null){
				_Verbaliq=getIntegerProperty("verbalIq");
				return _Verbaliq;
			}else {
				return _Verbaliq;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for verbalIq.
	 * @param v Value to Set.
	 */
	public void setVerbaliq(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/verbalIq",v);
		_Verbaliq=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Performanceiq=null;

	/**
	 * @return Returns the performanceIq.
	 */
	public Integer getPerformanceiq() {
		try{
			if (_Performanceiq==null){
				_Performanceiq=getIntegerProperty("performanceIq");
				return _Performanceiq;
			}else {
				return _Performanceiq;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for performanceIq.
	 * @param v Value to Set.
	 */
	public void setPerformanceiq(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/performanceIq",v);
		_Performanceiq=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Iqsrc=null;

	/**
	 * @return Returns the iqSrc.
	 */
	public String getIqsrc(){
		try{
			if (_Iqsrc==null){
				_Iqsrc=getStringProperty("iqSrc");
				return _Iqsrc;
			}else {
				return _Iqsrc;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for iqSrc.
	 * @param v Value to Set.
	 */
	public void setIqsrc(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/iqSrc",v);
		_Iqsrc=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.IqAbstractiqtest _Iqtest =null;

	/**
	 * iqTest
	 * @return org.nrg.xdat.om.IqAbstractiqtest
	 */
	public org.nrg.xdat.om.IqAbstractiqtest getIqtest() {
		try{
			if (_Iqtest==null){
				_Iqtest=((IqAbstractiqtest)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("iqTest")));
				return _Iqtest;
			}else {
				return _Iqtest;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for iqTest.
	 * @param v Value to Set.
	 */
	public void setIqtest(ItemI v) throws Exception{
		_Iqtest =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/iqTest",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/iqTest",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * iqTest
	 * set org.nrg.xdat.model.IqAbstractiqtestI
	 */
	public <A extends org.nrg.xdat.model.IqAbstractiqtestI> void setIqtest(A item) throws Exception{
	setIqtest((ItemI)item);
	}

	/**
	 * Removes the iqTest.
	 * */
	public void removeIqtest() {
		_Iqtest =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/iqTest",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _IqtestFK=null;

	/**
	 * @return Returns the iq:iqAssessmentData/iqtest_iq_abstractiqtest_id.
	 */
	public Integer getIqtestFK(){
		try{
			if (_IqtestFK==null){
				_IqtestFK=getIntegerProperty("iq:iqAssessmentData/iqtest_iq_abstractiqtest_id");
				return _IqtestFK;
			}else {
				return _IqtestFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for iq:iqAssessmentData/iqtest_iq_abstractiqtest_id.
	 * @param v Value to Set.
	 */
	public void setIqtestFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/iqtest_iq_abstractiqtest_id",v);
		_IqtestFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.IqIqassessmentdata> getAllIqIqassessmentdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqIqassessmentdata> al = new ArrayList<org.nrg.xdat.om.IqIqassessmentdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IqIqassessmentdata> getIqIqassessmentdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqIqassessmentdata> al = new ArrayList<org.nrg.xdat.om.IqIqassessmentdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.IqIqassessmentdata> getIqIqassessmentdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.IqIqassessmentdata> al = new ArrayList<org.nrg.xdat.om.IqIqassessmentdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static IqIqassessmentdata getIqIqassessmentdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("iq:iqAssessmentData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (IqIqassessmentdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //iqTest
	        IqAbstractiqtest childIqtest = (IqAbstractiqtest)this.getIqtest();
	            if (childIqtest!=null){
	              for(ResourceFile rf: ((IqAbstractiqtest)childIqtest).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("iqTest[" + ((IqAbstractiqtest)childIqtest).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("iqTest/" + ((IqAbstractiqtest)childIqtest).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
