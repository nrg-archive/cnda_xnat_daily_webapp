/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:59 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lshealthdata extends XnatSubjectassessordata implements org.nrg.xdat.model.Ls2LshealthdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lshealthdata.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsHealthData";

	public AutoLs2Lshealthdata(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lshealthdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lshealthdata(UserI user)
	 **/
	public AutoLs2Lshealthdata(){}

	public AutoLs2Lshealthdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsHealthData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Rating=null;

	/**
	 * @return Returns the rating.
	 */
	public Integer getRating() {
		try{
			if (_Rating==null){
				_Rating=getIntegerProperty("rating");
				return _Rating;
			}else {
				return _Rating;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rating.
	 * @param v Value to Set.
	 */
	public void setRating(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rating",v);
		_Rating=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Satisfaction=null;

	/**
	 * @return Returns the satisfaction.
	 */
	public Integer getSatisfaction() {
		try{
			if (_Satisfaction==null){
				_Satisfaction=getIntegerProperty("satisfaction");
				return _Satisfaction;
			}else {
				return _Satisfaction;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for satisfaction.
	 * @param v Value to Set.
	 */
	public void setSatisfaction(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/satisfaction",v);
		_Satisfaction=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Interferes=null;

	/**
	 * @return Returns the interferes.
	 */
	public Integer getInterferes() {
		try{
			if (_Interferes==null){
				_Interferes=getIntegerProperty("interferes");
				return _Interferes;
			}else {
				return _Interferes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for interferes.
	 * @param v Value to Set.
	 */
	public void setInterferes(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/interferes",v);
		_Interferes=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit> _Activitylimits_activity =null;

	/**
	 * activityLimits/activity
	 * @return Returns an List of org.nrg.xdat.om.Ls2Lsactivitylimit
	 */
	public <A extends org.nrg.xdat.model.Ls2LsactivitylimitI> List<A> getActivitylimits_activity() {
		try{
			if (_Activitylimits_activity==null){
				_Activitylimits_activity=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("activityLimits/activity"));
			}
			return (List<A>) _Activitylimits_activity;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.Ls2Lsactivitylimit>();}
	}

	/**
	 * Sets the value for activityLimits/activity.
	 * @param v Value to Set.
	 */
	public void setActivitylimits_activity(ItemI v) throws Exception{
		_Activitylimits_activity =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/activityLimits/activity",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/activityLimits/activity",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * activityLimits/activity
	 * Adds org.nrg.xdat.model.Ls2LsactivitylimitI
	 */
	public <A extends org.nrg.xdat.model.Ls2LsactivitylimitI> void addActivitylimits_activity(A item) throws Exception{
	setActivitylimits_activity((ItemI)item);
	}

	/**
	 * Removes the activityLimits/activity of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeActivitylimits_activity(int index) throws java.lang.IndexOutOfBoundsException {
		_Activitylimits_activity =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/activityLimits/activity",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Double _Activitylimits_avelimit=null;

	/**
	 * @return Returns the activityLimits/aveLimit.
	 */
	public Double getActivitylimits_avelimit() {
		try{
			if (_Activitylimits_avelimit==null){
				_Activitylimits_avelimit=getDoubleProperty("activityLimits/aveLimit");
				return _Activitylimits_avelimit;
			}else {
				return _Activitylimits_avelimit;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for activityLimits/aveLimit.
	 * @param v Value to Set.
	 */
	public void setActivitylimits_avelimit(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/activityLimits/aveLimit",v);
		_Activitylimits_avelimit=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Headinjury=null;

	/**
	 * @return Returns the headInjury.
	 */
	public Integer getHeadinjury() {
		try{
			if (_Headinjury==null){
				_Headinjury=getIntegerProperty("headInjury");
				return _Headinjury;
			}else {
				return _Headinjury;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for headInjury.
	 * @param v Value to Set.
	 */
	public void setHeadinjury(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/headInjury",v);
		_Headinjury=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hypertension_past=null;

	/**
	 * @return Returns the hypertension/past.
	 */
	public Integer getHypertension_past() {
		try{
			if (_Hypertension_past==null){
				_Hypertension_past=getIntegerProperty("hypertension/past");
				return _Hypertension_past;
			}else {
				return _Hypertension_past;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hypertension/past.
	 * @param v Value to Set.
	 */
	public void setHypertension_past(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hypertension/past",v);
		_Hypertension_past=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hypertension_now=null;

	/**
	 * @return Returns the hypertension/now.
	 */
	public Integer getHypertension_now() {
		try{
			if (_Hypertension_now==null){
				_Hypertension_now=getIntegerProperty("hypertension/now");
				return _Hypertension_now;
			}else {
				return _Hypertension_now;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hypertension/now.
	 * @param v Value to Set.
	 */
	public void setHypertension_now(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hypertension/now",v);
		_Hypertension_now=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hypertension_medication=null;

	/**
	 * @return Returns the hypertension/medication.
	 */
	public Integer getHypertension_medication() {
		try{
			if (_Hypertension_medication==null){
				_Hypertension_medication=getIntegerProperty("hypertension/medication");
				return _Hypertension_medication;
			}else {
				return _Hypertension_medication;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hypertension/medication.
	 * @param v Value to Set.
	 */
	public void setHypertension_medication(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hypertension/medication",v);
		_Hypertension_medication=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Postmenopausal=null;

	/**
	 * @return Returns the postMenopausal.
	 */
	public Integer getPostmenopausal() {
		try{
			if (_Postmenopausal==null){
				_Postmenopausal=getIntegerProperty("postMenopausal");
				return _Postmenopausal;
			}else {
				return _Postmenopausal;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for postMenopausal.
	 * @param v Value to Set.
	 */
	public void setPostmenopausal(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/postMenopausal",v);
		_Postmenopausal=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Estreplcther=null;

	/**
	 * @return Returns the estReplcTher.
	 */
	public Integer getEstreplcther() {
		try{
			if (_Estreplcther==null){
				_Estreplcther=getIntegerProperty("estReplcTher");
				return _Estreplcther;
			}else {
				return _Estreplcther;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for estReplcTher.
	 * @param v Value to Set.
	 */
	public void setEstreplcther(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/estReplcTher",v);
		_Estreplcther=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.Ls2Lsmedication> _Medications_medication =null;

	/**
	 * medications/medication
	 * @return Returns an List of org.nrg.xdat.om.Ls2Lsmedication
	 */
	public <A extends org.nrg.xdat.model.Ls2LsmedicationI> List<A> getMedications_medication() {
		try{
			if (_Medications_medication==null){
				_Medications_medication=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("medications/medication"));
			}
			return (List<A>) _Medications_medication;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.Ls2Lsmedication>();}
	}

	/**
	 * Sets the value for medications/medication.
	 * @param v Value to Set.
	 */
	public void setMedications_medication(ItemI v) throws Exception{
		_Medications_medication =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/medications/medication",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/medications/medication",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * medications/medication
	 * Adds org.nrg.xdat.model.Ls2LsmedicationI
	 */
	public <A extends org.nrg.xdat.model.Ls2LsmedicationI> void addMedications_medication(A item) throws Exception{
	setMedications_medication((ItemI)item);
	}

	/**
	 * Removes the medications/medication of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeMedications_medication(int index) throws java.lang.IndexOutOfBoundsException {
		_Medications_medication =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/medications/medication",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Medications_others=null;

	/**
	 * @return Returns the medications/others.
	 */
	public Integer getMedications_others() {
		try{
			if (_Medications_others==null){
				_Medications_others=getIntegerProperty("medications/others");
				return _Medications_others;
			}else {
				return _Medications_others;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for medications/others.
	 * @param v Value to Set.
	 */
	public void setMedications_others(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/medications/others",v);
		_Medications_others=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.Ls2Lsillness> _Illnesses_illness =null;

	/**
	 * illnesses/illness
	 * @return Returns an List of org.nrg.xdat.om.Ls2Lsillness
	 */
	public <A extends org.nrg.xdat.model.Ls2LsillnessI> List<A> getIllnesses_illness() {
		try{
			if (_Illnesses_illness==null){
				_Illnesses_illness=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("illnesses/illness"));
			}
			return (List<A>) _Illnesses_illness;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.Ls2Lsillness>();}
	}

	/**
	 * Sets the value for illnesses/illness.
	 * @param v Value to Set.
	 */
	public void setIllnesses_illness(ItemI v) throws Exception{
		_Illnesses_illness =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/illnesses/illness",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/illnesses/illness",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * illnesses/illness
	 * Adds org.nrg.xdat.model.Ls2LsillnessI
	 */
	public <A extends org.nrg.xdat.model.Ls2LsillnessI> void addIllnesses_illness(A item) throws Exception{
	setIllnesses_illness((ItemI)item);
	}

	/**
	 * Removes the illnesses/illness of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeIllnesses_illness(int index) throws java.lang.IndexOutOfBoundsException {
		_Illnesses_illness =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/illnesses/illness",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Bonefrac=null;

	/**
	 * @return Returns the boneFrac.
	 */
	public Integer getBonefrac() {
		try{
			if (_Bonefrac==null){
				_Bonefrac=getIntegerProperty("boneFrac");
				return _Bonefrac;
			}else {
				return _Bonefrac;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for boneFrac.
	 * @param v Value to Set.
	 */
	public void setBonefrac(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/boneFrac",v);
		_Bonefrac=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Surgery=null;

	/**
	 * @return Returns the surgery.
	 */
	public Integer getSurgery() {
		try{
			if (_Surgery==null){
				_Surgery=getIntegerProperty("surgery");
				return _Surgery;
			}else {
				return _Surgery;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for surgery.
	 * @param v Value to Set.
	 */
	public void setSurgery(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/surgery",v);
		_Surgery=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hospital=null;

	/**
	 * @return Returns the hospital.
	 */
	public Integer getHospital() {
		try{
			if (_Hospital==null){
				_Hospital=getIntegerProperty("hospital");
				return _Hospital;
			}else {
				return _Hospital;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for hospital.
	 * @param v Value to Set.
	 */
	public void setHospital(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/hospital",v);
		_Hospital=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private ArrayList<org.nrg.xdat.om.Ls2Lssubstance> _Substanceuse_substance =null;

	/**
	 * substanceUse/substance
	 * @return Returns an List of org.nrg.xdat.om.Ls2Lssubstance
	 */
	public <A extends org.nrg.xdat.model.Ls2LssubstanceI> List<A> getSubstanceuse_substance() {
		try{
			if (_Substanceuse_substance==null){
				_Substanceuse_substance=org.nrg.xdat.base.BaseElement.WrapItems(getChildItems("substanceUse/substance"));
			}
			return (List<A>) _Substanceuse_substance;
		} catch (Exception e1) {return (List<A>) new ArrayList<org.nrg.xdat.om.Ls2Lssubstance>();}
	}

	/**
	 * Sets the value for substanceUse/substance.
	 * @param v Value to Set.
	 */
	public void setSubstanceuse_substance(ItemI v) throws Exception{
		_Substanceuse_substance =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/substanceUse/substance",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/substanceUse/substance",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * substanceUse/substance
	 * Adds org.nrg.xdat.model.Ls2LssubstanceI
	 */
	public <A extends org.nrg.xdat.model.Ls2LssubstanceI> void addSubstanceuse_substance(A item) throws Exception{
	setSubstanceuse_substance((ItemI)item);
	}

	/**
	 * Removes the substanceUse/substance of the given index.
	 * @param index Index of child to remove.
	 */
	public void removeSubstanceuse_substance(int index) throws java.lang.IndexOutOfBoundsException {
		_Substanceuse_substance =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/substanceUse/substance",index);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lshealthdata> getAllLs2Lshealthdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lshealthdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lshealthdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lshealthdata> getLs2LshealthdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lshealthdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lshealthdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lshealthdata> getLs2LshealthdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lshealthdata> al = new ArrayList<org.nrg.xdat.om.Ls2Lshealthdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lshealthdata getLs2LshealthdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsHealthData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lshealthdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //activityLimits/activity
	        for(org.nrg.xdat.model.Ls2LsactivitylimitI childActivitylimits_activity : this.getActivitylimits_activity()){
	            if (childActivitylimits_activity!=null){
	              for(ResourceFile rf: ((Ls2Lsactivitylimit)childActivitylimits_activity).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("activityLimits/activity[" + ((Ls2Lsactivitylimit)childActivitylimits_activity).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("activityLimits/activity/" + ((Ls2Lsactivitylimit)childActivitylimits_activity).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //medications/medication
	        for(org.nrg.xdat.model.Ls2LsmedicationI childMedications_medication : this.getMedications_medication()){
	            if (childMedications_medication!=null){
	              for(ResourceFile rf: ((Ls2Lsmedication)childMedications_medication).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("medications/medication[" + ((Ls2Lsmedication)childMedications_medication).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("medications/medication/" + ((Ls2Lsmedication)childMedications_medication).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //illnesses/illness
	        for(org.nrg.xdat.model.Ls2LsillnessI childIllnesses_illness : this.getIllnesses_illness()){
	            if (childIllnesses_illness!=null){
	              for(ResourceFile rf: ((Ls2Lsillness)childIllnesses_illness).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("illnesses/illness[" + ((Ls2Lsillness)childIllnesses_illness).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("illnesses/illness/" + ((Ls2Lsillness)childIllnesses_illness).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	        //substanceUse/substance
	        for(org.nrg.xdat.model.Ls2LssubstanceI childSubstanceuse_substance : this.getSubstanceuse_substance()){
	            if (childSubstanceuse_substance!=null){
	              for(ResourceFile rf: ((Ls2Lssubstance)childSubstanceuse_substance).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("substanceUse/substance[" + ((Ls2Lssubstance)childSubstanceuse_substance).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("substanceUse/substance/" + ((Ls2Lssubstance)childSubstanceuse_substance).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	        }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
