/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoUdsB2hachdata extends XnatSubjectassessordata implements org.nrg.xdat.model.UdsB2hachdataI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoUdsB2hachdata.class);
	public static String SCHEMA_ELEMENT_NAME="uds:b2hachData";

	public AutoUdsB2hachdata(ItemI item)
	{
		super(item);
	}

	public AutoUdsB2hachdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoUdsB2hachdata(UserI user)
	 **/
	public AutoUdsB2hachdata(){}

	public AutoUdsB2hachdata(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "uds:b2hachData";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Abrupt=null;

	/**
	 * @return Returns the ABRUPT.
	 */
	public Integer getAbrupt() {
		try{
			if (_Abrupt==null){
				_Abrupt=getIntegerProperty("ABRUPT");
				return _Abrupt;
			}else {
				return _Abrupt;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ABRUPT.
	 * @param v Value to Set.
	 */
	public void setAbrupt(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ABRUPT",v);
		_Abrupt=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Stepwise=null;

	/**
	 * @return Returns the STEPWISE.
	 */
	public Integer getStepwise() {
		try{
			if (_Stepwise==null){
				_Stepwise=getIntegerProperty("STEPWISE");
				return _Stepwise;
			}else {
				return _Stepwise;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STEPWISE.
	 * @param v Value to Set.
	 */
	public void setStepwise(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STEPWISE",v);
		_Stepwise=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Somatic=null;

	/**
	 * @return Returns the SOMATIC.
	 */
	public Integer getSomatic() {
		try{
			if (_Somatic==null){
				_Somatic=getIntegerProperty("SOMATIC");
				return _Somatic;
			}else {
				return _Somatic;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for SOMATIC.
	 * @param v Value to Set.
	 */
	public void setSomatic(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/SOMATIC",v);
		_Somatic=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Emot=null;

	/**
	 * @return Returns the EMOT.
	 */
	public Integer getEmot() {
		try{
			if (_Emot==null){
				_Emot=getIntegerProperty("EMOT");
				return _Emot;
			}else {
				return _Emot;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for EMOT.
	 * @param v Value to Set.
	 */
	public void setEmot(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/EMOT",v);
		_Emot=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hxhyper=null;

	/**
	 * @return Returns the HXHYPER.
	 */
	public Integer getHxhyper() {
		try{
			if (_Hxhyper==null){
				_Hxhyper=getIntegerProperty("HXHYPER");
				return _Hxhyper;
			}else {
				return _Hxhyper;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HXHYPER.
	 * @param v Value to Set.
	 */
	public void setHxhyper(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HXHYPER",v);
		_Hxhyper=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hxstroke=null;

	/**
	 * @return Returns the HXSTROKE.
	 */
	public Integer getHxstroke() {
		try{
			if (_Hxstroke==null){
				_Hxstroke=getIntegerProperty("HXSTROKE");
				return _Hxstroke;
			}else {
				return _Hxstroke;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HXSTROKE.
	 * @param v Value to Set.
	 */
	public void setHxstroke(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HXSTROKE",v);
		_Hxstroke=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Foclsym=null;

	/**
	 * @return Returns the FOCLSYM.
	 */
	public Integer getFoclsym() {
		try{
			if (_Foclsym==null){
				_Foclsym=getIntegerProperty("FOCLSYM");
				return _Foclsym;
			}else {
				return _Foclsym;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FOCLSYM.
	 * @param v Value to Set.
	 */
	public void setFoclsym(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FOCLSYM",v);
		_Foclsym=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Foclsign=null;

	/**
	 * @return Returns the FOCLSIGN.
	 */
	public Integer getFoclsign() {
		try{
			if (_Foclsign==null){
				_Foclsign=getIntegerProperty("FOCLSIGN");
				return _Foclsign;
			}else {
				return _Foclsign;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FOCLSIGN.
	 * @param v Value to Set.
	 */
	public void setFoclsign(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FOCLSIGN",v);
		_Foclsign=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Hachin=null;

	/**
	 * @return Returns the HACHIN.
	 */
	public Integer getHachin() {
		try{
			if (_Hachin==null){
				_Hachin=getIntegerProperty("HACHIN");
				return _Hachin;
			}else {
				return _Hachin;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for HACHIN.
	 * @param v Value to Set.
	 */
	public void setHachin(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/HACHIN",v);
		_Hachin=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvdcog=null;

	/**
	 * @return Returns the CVDCOG.
	 */
	public Integer getCvdcog() {
		try{
			if (_Cvdcog==null){
				_Cvdcog=getIntegerProperty("CVDCOG");
				return _Cvdcog;
			}else {
				return _Cvdcog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDCOG.
	 * @param v Value to Set.
	 */
	public void setCvdcog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDCOG",v);
		_Cvdcog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Strokcog=null;

	/**
	 * @return Returns the STROKCOG.
	 */
	public Integer getStrokcog() {
		try{
			if (_Strokcog==null){
				_Strokcog=getIntegerProperty("STROKCOG");
				return _Strokcog;
			}else {
				return _Strokcog;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for STROKCOG.
	 * @param v Value to Set.
	 */
	public void setStrokcog(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/STROKCOG",v);
		_Strokcog=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvdimag=null;

	/**
	 * @return Returns the CVDIMAG.
	 */
	public Integer getCvdimag() {
		try{
			if (_Cvdimag==null){
				_Cvdimag=getIntegerProperty("CVDIMAG");
				return _Cvdimag;
			}else {
				return _Cvdimag;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDIMAG.
	 * @param v Value to Set.
	 */
	public void setCvdimag(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDIMAG",v);
		_Cvdimag=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvdimag1=null;

	/**
	 * @return Returns the CVDIMAG1.
	 */
	public Integer getCvdimag1() {
		try{
			if (_Cvdimag1==null){
				_Cvdimag1=getIntegerProperty("CVDIMAG1");
				return _Cvdimag1;
			}else {
				return _Cvdimag1;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDIMAG1.
	 * @param v Value to Set.
	 */
	public void setCvdimag1(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDIMAG1",v);
		_Cvdimag1=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvdimag2=null;

	/**
	 * @return Returns the CVDIMAG2.
	 */
	public Integer getCvdimag2() {
		try{
			if (_Cvdimag2==null){
				_Cvdimag2=getIntegerProperty("CVDIMAG2");
				return _Cvdimag2;
			}else {
				return _Cvdimag2;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDIMAG2.
	 * @param v Value to Set.
	 */
	public void setCvdimag2(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDIMAG2",v);
		_Cvdimag2=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvdimag3=null;

	/**
	 * @return Returns the CVDIMAG3.
	 */
	public Integer getCvdimag3() {
		try{
			if (_Cvdimag3==null){
				_Cvdimag3=getIntegerProperty("CVDIMAG3");
				return _Cvdimag3;
			}else {
				return _Cvdimag3;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDIMAG3.
	 * @param v Value to Set.
	 */
	public void setCvdimag3(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDIMAG3",v);
		_Cvdimag3=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Cvdimag4=null;

	/**
	 * @return Returns the CVDIMAG4.
	 */
	public Integer getCvdimag4() {
		try{
			if (_Cvdimag4==null){
				_Cvdimag4=getIntegerProperty("CVDIMAG4");
				return _Cvdimag4;
			}else {
				return _Cvdimag4;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDIMAG4.
	 * @param v Value to Set.
	 */
	public void setCvdimag4(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDIMAG4",v);
		_Cvdimag4=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Cvdimagx=null;

	/**
	 * @return Returns the CVDIMAGX.
	 */
	public String getCvdimagx(){
		try{
			if (_Cvdimagx==null){
				_Cvdimagx=getStringProperty("CVDIMAGX");
				return _Cvdimagx;
			}else {
				return _Cvdimagx;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for CVDIMAGX.
	 * @param v Value to Set.
	 */
	public void setCvdimagx(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/CVDIMAGX",v);
		_Cvdimagx=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.UdsB2hachdata> getAllUdsB2hachdatas(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB2hachdata> al = new ArrayList<org.nrg.xdat.om.UdsB2hachdata>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB2hachdata> getUdsB2hachdatasByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB2hachdata> al = new ArrayList<org.nrg.xdat.om.UdsB2hachdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.UdsB2hachdata> getUdsB2hachdatasByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.UdsB2hachdata> al = new ArrayList<org.nrg.xdat.om.UdsB2hachdata>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static UdsB2hachdata getUdsB2hachdatasById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("uds:b2hachData/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (UdsB2hachdata) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
