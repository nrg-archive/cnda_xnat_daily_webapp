/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:59 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lsmedication extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.Ls2LsmedicationI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lsmedication.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsMedication";

	public AutoLs2Lsmedication(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lsmedication(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lsmedication(UserI user)
	 **/
	public AutoLs2Lsmedication(){}

	public AutoLs2Lsmedication(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsMedication";
	}

	//FIELD

	private String _Lsmedication=null;

	/**
	 * @return Returns the lsMedication.
	 */
	public String getLsmedication(){
		try{
			if (_Lsmedication==null){
				_Lsmedication=getStringProperty("lsMedication");
				return _Lsmedication;
			}else {
				return _Lsmedication;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lsMedication.
	 * @param v Value to Set.
	 */
	public void setLsmedication(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lsMedication",v);
		_Lsmedication=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ls2LsmedicationId=null;

	/**
	 * @return Returns the ls2_lsMedication_id.
	 */
	public Integer getLs2LsmedicationId() {
		try{
			if (_Ls2LsmedicationId==null){
				_Ls2LsmedicationId=getIntegerProperty("ls2_lsMedication_id");
				return _Ls2LsmedicationId;
			}else {
				return _Ls2LsmedicationId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ls2_lsMedication_id.
	 * @param v Value to Set.
	 */
	public void setLs2LsmedicationId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ls2_lsMedication_id",v);
		_Ls2LsmedicationId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsmedication> getAllLs2Lsmedications(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsmedication> al = new ArrayList<org.nrg.xdat.om.Ls2Lsmedication>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsmedication> getLs2LsmedicationsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsmedication> al = new ArrayList<org.nrg.xdat.om.Ls2Lsmedication>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsmedication> getLs2LsmedicationsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsmedication> al = new ArrayList<org.nrg.xdat.om.Ls2Lsmedication>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lsmedication getLs2LsmedicationsByLs2LsmedicationId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsMedication/ls2_lsMedication_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lsmedication) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
