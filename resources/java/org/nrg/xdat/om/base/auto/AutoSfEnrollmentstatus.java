/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:59 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoSfEnrollmentstatus extends XnatSubjectassessordata implements org.nrg.xdat.model.SfEnrollmentstatusI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoSfEnrollmentstatus.class);
	public static String SCHEMA_ELEMENT_NAME="sf:enrollmentStatus";

	public AutoSfEnrollmentstatus(ItemI item)
	{
		super(item);
	}

	public AutoSfEnrollmentstatus(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoSfEnrollmentstatus(UserI user)
	 **/
	public AutoSfEnrollmentstatus(){}

	public AutoSfEnrollmentstatus(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "sf:enrollmentStatus";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Enrollmentdate=null;

	/**
	 * @return Returns the enrollmentDate.
	 */
	public Object getEnrollmentdate(){
		try{
			if (_Enrollmentdate==null){
				_Enrollmentdate=getProperty("enrollmentDate");
				return _Enrollmentdate;
			}else {
				return _Enrollmentdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for enrollmentDate.
	 * @param v Value to Set.
	 */
	public void setEnrollmentdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/enrollmentDate",v);
		_Enrollmentdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Initialdiagnosis=null;

	/**
	 * @return Returns the initialDiagnosis.
	 */
	public String getInitialdiagnosis(){
		try{
			if (_Initialdiagnosis==null){
				_Initialdiagnosis=getStringProperty("initialDiagnosis");
				return _Initialdiagnosis;
			}else {
				return _Initialdiagnosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for initialDiagnosis.
	 * @param v Value to Set.
	 */
	public void setInitialdiagnosis(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/initialDiagnosis",v);
		_Initialdiagnosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Initialdiagnosisdate=null;

	/**
	 * @return Returns the initialDiagnosisDate.
	 */
	public Object getInitialdiagnosisdate(){
		try{
			if (_Initialdiagnosisdate==null){
				_Initialdiagnosisdate=getProperty("initialDiagnosisDate");
				return _Initialdiagnosisdate;
			}else {
				return _Initialdiagnosisdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for initialDiagnosisDate.
	 * @param v Value to Set.
	 */
	public void setInitialdiagnosisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/initialDiagnosisDate",v);
		_Initialdiagnosisdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Pathologicdiagnosis=null;

	/**
	 * @return Returns the pathologicDiagnosis.
	 */
	public Boolean getPathologicdiagnosis() {
		try{
			if (_Pathologicdiagnosis==null){
				_Pathologicdiagnosis=getBooleanProperty("pathologicDiagnosis");
				return _Pathologicdiagnosis;
			}else {
				return _Pathologicdiagnosis;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pathologicDiagnosis.
	 * @param v Value to Set.
	 */
	public void setPathologicdiagnosis(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/pathologicDiagnosis",v);
		_Pathologicdiagnosis=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Pathologicdiagnosistype=null;

	/**
	 * @return Returns the pathologicDiagnosisType.
	 */
	public String getPathologicdiagnosistype(){
		try{
			if (_Pathologicdiagnosistype==null){
				_Pathologicdiagnosistype=getStringProperty("pathologicDiagnosisType");
				return _Pathologicdiagnosistype;
			}else {
				return _Pathologicdiagnosistype;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pathologicDiagnosisType.
	 * @param v Value to Set.
	 */
	public void setPathologicdiagnosistype(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pathologicDiagnosisType",v);
		_Pathologicdiagnosistype=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Pathologicdiagnosisdate=null;

	/**
	 * @return Returns the pathologicDiagnosisDate.
	 */
	public Object getPathologicdiagnosisdate(){
		try{
			if (_Pathologicdiagnosisdate==null){
				_Pathologicdiagnosisdate=getProperty("pathologicDiagnosisDate");
				return _Pathologicdiagnosisdate;
			}else {
				return _Pathologicdiagnosisdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pathologicDiagnosisDate.
	 * @param v Value to Set.
	 */
	public void setPathologicdiagnosisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/pathologicDiagnosisDate",v);
		_Pathologicdiagnosisdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Previouslytreated=null;

	/**
	 * @return Returns the previouslyTreated.
	 */
	public Boolean getPreviouslytreated() {
		try{
			if (_Previouslytreated==null){
				_Previouslytreated=getBooleanProperty("previouslyTreated");
				return _Previouslytreated;
			}else {
				return _Previouslytreated;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for previouslyTreated.
	 * @param v Value to Set.
	 */
	public void setPreviouslytreated(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/previouslyTreated",v);
		_Previouslytreated=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Detailspriortoenrollment=null;

	/**
	 * @return Returns the detailsPriorToEnrollment.
	 */
	public String getDetailspriortoenrollment(){
		try{
			if (_Detailspriortoenrollment==null){
				_Detailspriortoenrollment=getStringProperty("detailsPriorToEnrollment");
				return _Detailspriortoenrollment;
			}else {
				return _Detailspriortoenrollment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for detailsPriorToEnrollment.
	 * @param v Value to Set.
	 */
	public void setDetailspriortoenrollment(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/detailsPriorToEnrollment",v);
		_Detailspriortoenrollment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Systemicdiagnosisdate=null;

	/**
	 * @return Returns the systemicDiagnosisDate.
	 */
	public Object getSystemicdiagnosisdate(){
		try{
			if (_Systemicdiagnosisdate==null){
				_Systemicdiagnosisdate=getProperty("systemicDiagnosisDate");
				return _Systemicdiagnosisdate;
			}else {
				return _Systemicdiagnosisdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for systemicDiagnosisDate.
	 * @param v Value to Set.
	 */
	public void setSystemicdiagnosisdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/systemicDiagnosisDate",v);
		_Systemicdiagnosisdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Recurrentatenrollment=null;

	/**
	 * @return Returns the recurrentAtEnrollment.
	 */
	public Boolean getRecurrentatenrollment() {
		try{
			if (_Recurrentatenrollment==null){
				_Recurrentatenrollment=getBooleanProperty("recurrentAtEnrollment");
				return _Recurrentatenrollment;
			}else {
				return _Recurrentatenrollment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for recurrentAtEnrollment.
	 * @param v Value to Set.
	 */
	public void setRecurrentatenrollment(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/recurrentAtEnrollment",v);
		_Recurrentatenrollment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Recurrentatenrollmentnotes=null;

	/**
	 * @return Returns the recurrentAtEnrollmentNotes.
	 */
	public String getRecurrentatenrollmentnotes(){
		try{
			if (_Recurrentatenrollmentnotes==null){
				_Recurrentatenrollmentnotes=getStringProperty("recurrentAtEnrollmentNotes");
				return _Recurrentatenrollmentnotes;
			}else {
				return _Recurrentatenrollmentnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for recurrentAtEnrollmentNotes.
	 * @param v Value to Set.
	 */
	public void setRecurrentatenrollmentnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/recurrentAtEnrollmentNotes",v);
		_Recurrentatenrollmentnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Exitdate=null;

	/**
	 * @return Returns the exitDate.
	 */
	public Object getExitdate(){
		try{
			if (_Exitdate==null){
				_Exitdate=getProperty("exitDate");
				return _Exitdate;
			}else {
				return _Exitdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for exitDate.
	 * @param v Value to Set.
	 */
	public void setExitdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/exitDate",v);
		_Exitdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Exitreason=null;

	/**
	 * @return Returns the exitReason.
	 */
	public String getExitreason(){
		try{
			if (_Exitreason==null){
				_Exitreason=getStringProperty("exitReason");
				return _Exitreason;
			}else {
				return _Exitreason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for exitReason.
	 * @param v Value to Set.
	 */
	public void setExitreason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/exitReason",v);
		_Exitreason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Exitnotes=null;

	/**
	 * @return Returns the exitNotes.
	 */
	public String getExitnotes(){
		try{
			if (_Exitnotes==null){
				_Exitnotes=getStringProperty("exitNotes");
				return _Exitnotes;
			}else {
				return _Exitnotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for exitNotes.
	 * @param v Value to Set.
	 */
	public void setExitnotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/exitNotes",v);
		_Exitnotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Object _Deathdate=null;

	/**
	 * @return Returns the deathDate.
	 */
	public Object getDeathdate(){
		try{
			if (_Deathdate==null){
				_Deathdate=getProperty("deathDate");
				return _Deathdate;
			}else {
				return _Deathdate;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for deathDate.
	 * @param v Value to Set.
	 */
	public void setDeathdate(Object v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/deathDate",v);
		_Deathdate=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Autopsied=null;

	/**
	 * @return Returns the autopsied.
	 */
	public Boolean getAutopsied() {
		try{
			if (_Autopsied==null){
				_Autopsied=getBooleanProperty("autopsied");
				return _Autopsied;
			}else {
				return _Autopsied;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for autopsied.
	 * @param v Value to Set.
	 */
	public void setAutopsied(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/autopsied",v);
		_Autopsied=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Autopsiedoptions=null;

	/**
	 * @return Returns the autopsiedOptions.
	 */
	public String getAutopsiedoptions(){
		try{
			if (_Autopsiedoptions==null){
				_Autopsiedoptions=getStringProperty("autopsiedOptions");
				return _Autopsiedoptions;
			}else {
				return _Autopsiedoptions;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for autopsiedOptions.
	 * @param v Value to Set.
	 */
	public void setAutopsiedoptions(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/autopsiedOptions",v);
		_Autopsiedoptions=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Autopsynotes=null;

	/**
	 * @return Returns the autopsyNotes.
	 */
	public String getAutopsynotes(){
		try{
			if (_Autopsynotes==null){
				_Autopsynotes=getStringProperty("autopsyNotes");
				return _Autopsynotes;
			}else {
				return _Autopsynotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for autopsyNotes.
	 * @param v Value to Set.
	 */
	public void setAutopsynotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/autopsyNotes",v);
		_Autopsynotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.SfEnrollmentstatus> getAllSfEnrollmentstatuss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEnrollmentstatus> al = new ArrayList<org.nrg.xdat.om.SfEnrollmentstatus>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfEnrollmentstatus> getSfEnrollmentstatussByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEnrollmentstatus> al = new ArrayList<org.nrg.xdat.om.SfEnrollmentstatus>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.SfEnrollmentstatus> getSfEnrollmentstatussByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.SfEnrollmentstatus> al = new ArrayList<org.nrg.xdat.om.SfEnrollmentstatus>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static SfEnrollmentstatus getSfEnrollmentstatussById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("sf:enrollmentStatus/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (SfEnrollmentstatus) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
