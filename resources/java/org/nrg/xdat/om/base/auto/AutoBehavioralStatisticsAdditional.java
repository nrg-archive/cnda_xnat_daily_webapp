/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoBehavioralStatisticsAdditional extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.BehavioralStatisticsAdditionalI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoBehavioralStatisticsAdditional.class);
	public static String SCHEMA_ELEMENT_NAME="behavioral:statistics_additional";

	public AutoBehavioralStatisticsAdditional(ItemI item)
	{
		super(item);
	}

	public AutoBehavioralStatisticsAdditional(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoBehavioralStatisticsAdditional(UserI user)
	 **/
	public AutoBehavioralStatisticsAdditional(){}

	public AutoBehavioralStatisticsAdditional(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "behavioral:statistics_additional";
	}

	//FIELD

	private Double _Additional=null;

	/**
	 * @return Returns the additional.
	 */
	public Double getAdditional() {
		try{
			if (_Additional==null){
				_Additional=getDoubleProperty("additional");
				return _Additional;
			}else {
				return _Additional;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for additional.
	 * @param v Value to Set.
	 */
	public void setAdditional(Double v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/additional",v);
		_Additional=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Name=null;

	/**
	 * @return Returns the name.
	 */
	public String getName(){
		try{
			if (_Name==null){
				_Name=getStringProperty("name");
				return _Name;
			}else {
				return _Name;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/name",v);
		_Name=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _BehavioralStatisticsAdditionalId=null;

	/**
	 * @return Returns the behavioral_statistics_additional_id.
	 */
	public Integer getBehavioralStatisticsAdditionalId() {
		try{
			if (_BehavioralStatisticsAdditionalId==null){
				_BehavioralStatisticsAdditionalId=getIntegerProperty("behavioral_statistics_additional_id");
				return _BehavioralStatisticsAdditionalId;
			}else {
				return _BehavioralStatisticsAdditionalId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for behavioral_statistics_additional_id.
	 * @param v Value to Set.
	 */
	public void setBehavioralStatisticsAdditionalId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/behavioral_statistics_additional_id",v);
		_BehavioralStatisticsAdditionalId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> getAllBehavioralStatisticsAdditionals(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> al = new ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> getBehavioralStatisticsAdditionalsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> al = new ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> getBehavioralStatisticsAdditionalsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional> al = new ArrayList<org.nrg.xdat.om.BehavioralStatisticsAdditional>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static BehavioralStatisticsAdditional getBehavioralStatisticsAdditionalsByBehavioralStatisticsAdditionalId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("behavioral:statistics_additional/behavioral_statistics_additional_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (BehavioralStatisticsAdditional) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
