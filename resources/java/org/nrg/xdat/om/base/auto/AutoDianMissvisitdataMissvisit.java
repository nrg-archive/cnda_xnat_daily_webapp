/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoDianMissvisitdataMissvisit extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.DianMissvisitdataMissvisitI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoDianMissvisitdataMissvisit.class);
	public static String SCHEMA_ELEMENT_NAME="dian:missvisitData_missvisit";

	public AutoDianMissvisitdataMissvisit(ItemI item)
	{
		super(item);
	}

	public AutoDianMissvisitdataMissvisit(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoDianMissvisitdataMissvisit(UserI user)
	 **/
	public AutoDianMissvisitdataMissvisit(){}

	public AutoDianMissvisitdataMissvisit(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "dian:missvisitData_missvisit";
	}

	//FIELD

	private Integer _Recno=null;

	/**
	 * @return Returns the RECNO.
	 */
	public Integer getRecno() {
		try{
			if (_Recno==null){
				_Recno=getIntegerProperty("RECNO");
				return _Recno;
			}else {
				return _Recno;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RECNO.
	 * @param v Value to Set.
	 */
	public void setRecno(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RECNO",v);
		_Recno=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Partial=null;

	/**
	 * @return Returns the PARTIAL.
	 */
	public String getPartial(){
		try{
			if (_Partial==null){
				_Partial=getStringProperty("PARTIAL");
				return _Partial;
			}else {
				return _Partial;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARTIAL.
	 * @param v Value to Set.
	 */
	public void setPartial(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARTIAL",v);
		_Partial=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Missother=null;

	/**
	 * @return Returns the MISSOTHER.
	 */
	public String getMissother(){
		try{
			if (_Missother==null){
				_Missother=getStringProperty("MISSOTHER");
				return _Missother;
			}else {
				return _Missother;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for MISSOTHER.
	 * @param v Value to Set.
	 */
	public void setMissother(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/MISSOTHER",v);
		_Missother=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Reason=null;

	/**
	 * @return Returns the REASON.
	 */
	public String getReason(){
		try{
			if (_Reason==null){
				_Reason=getStringProperty("REASON");
				return _Reason;
			}else {
				return _Reason;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for REASON.
	 * @param v Value to Set.
	 */
	public void setReason(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/REASON",v);
		_Reason=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Partcom=null;

	/**
	 * @return Returns the PARTCOM.
	 */
	public String getPartcom(){
		try{
			if (_Partcom==null){
				_Partcom=getStringProperty("PARTCOM");
				return _Partcom;
			}else {
				return _Partcom;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for PARTCOM.
	 * @param v Value to Set.
	 */
	public void setPartcom(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/PARTCOM",v);
		_Partcom=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Followup=null;

	/**
	 * @return Returns the FOLLOWUP.
	 */
	public Integer getFollowup() {
		try{
			if (_Followup==null){
				_Followup=getIntegerProperty("FOLLOWUP");
				return _Followup;
			}else {
				return _Followup;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for FOLLOWUP.
	 * @param v Value to Set.
	 */
	public void setFollowup(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/FOLLOWUP",v);
		_Followup=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Resched=null;

	/**
	 * @return Returns the RESCHED.
	 */
	public String getResched(){
		try{
			if (_Resched==null){
				_Resched=getStringProperty("RESCHED");
				return _Resched;
			}else {
				return _Resched;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RESCHED.
	 * @param v Value to Set.
	 */
	public void setResched(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RESCHED",v);
		_Resched=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Reschedo=null;

	/**
	 * @return Returns the RESCHEDO.
	 */
	public String getReschedo(){
		try{
			if (_Reschedo==null){
				_Reschedo=getStringProperty("RESCHEDO");
				return _Reschedo;
			}else {
				return _Reschedo;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for RESCHEDO.
	 * @param v Value to Set.
	 */
	public void setReschedo(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/RESCHEDO",v);
		_Reschedo=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _DianMissvisitdataMissvisitId=null;

	/**
	 * @return Returns the dian_missvisitData_missvisit_id.
	 */
	public Integer getDianMissvisitdataMissvisitId() {
		try{
			if (_DianMissvisitdataMissvisitId==null){
				_DianMissvisitdataMissvisitId=getIntegerProperty("dian_missvisitData_missvisit_id");
				return _DianMissvisitdataMissvisitId;
			}else {
				return _DianMissvisitdataMissvisitId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for dian_missvisitData_missvisit_id.
	 * @param v Value to Set.
	 */
	public void setDianMissvisitdataMissvisitId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/dian_missvisitData_missvisit_id",v);
		_DianMissvisitdataMissvisitId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit> getAllDianMissvisitdataMissvisits(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit> al = new ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit> getDianMissvisitdataMissvisitsByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit> al = new ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit> getDianMissvisitdataMissvisitsByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit> al = new ArrayList<org.nrg.xdat.om.DianMissvisitdataMissvisit>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static DianMissvisitdataMissvisit getDianMissvisitdataMissvisitsByDianMissvisitdataMissvisitId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("dian:missvisitData_missvisit/dian_missvisitData_missvisit_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (DianMissvisitdataMissvisit) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
