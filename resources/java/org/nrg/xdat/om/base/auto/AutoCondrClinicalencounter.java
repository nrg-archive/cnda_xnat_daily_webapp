/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoCondrClinicalencounter extends XnatSubjectassessordata implements org.nrg.xdat.model.CondrClinicalencounterI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoCondrClinicalencounter.class);
	public static String SCHEMA_ELEMENT_NAME="condr:clinicalEncounter";

	public AutoCondrClinicalencounter(ItemI item)
	{
		super(item);
	}

	public AutoCondrClinicalencounter(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoCondrClinicalencounter(UserI user)
	 **/
	public AutoCondrClinicalencounter(){}

	public AutoCondrClinicalencounter(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "condr:clinicalEncounter";
	}
	 private org.nrg.xdat.om.XnatSubjectassessordata _Subjectassessordata =null;

	/**
	 * subjectAssessorData
	 * @return org.nrg.xdat.om.XnatSubjectassessordata
	 */
	public org.nrg.xdat.om.XnatSubjectassessordata getSubjectassessordata() {
		try{
			if (_Subjectassessordata==null){
				_Subjectassessordata=((XnatSubjectassessordata)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("subjectAssessorData")));
				return _Subjectassessordata;
			}else {
				return _Subjectassessordata;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for subjectAssessorData.
	 * @param v Value to Set.
	 */
	public void setSubjectassessordata(ItemI v) throws Exception{
		_Subjectassessordata =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * subjectAssessorData
	 * set org.nrg.xdat.model.XnatSubjectassessordataI
	 */
	public <A extends org.nrg.xdat.model.XnatSubjectassessordataI> void setSubjectassessordata(A item) throws Exception{
	setSubjectassessordata((ItemI)item);
	}

	/**
	 * Removes the subjectAssessorData.
	 * */
	public void removeSubjectassessordata() {
		_Subjectassessordata =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/subjectAssessorData",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Karnofskyperformancestatus=null;

	/**
	 * @return Returns the karnofskyPerformanceStatus.
	 */
	public Integer getKarnofskyperformancestatus() {
		try{
			if (_Karnofskyperformancestatus==null){
				_Karnofskyperformancestatus=getIntegerProperty("karnofskyPerformanceStatus");
				return _Karnofskyperformancestatus;
			}else {
				return _Karnofskyperformancestatus;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for karnofskyPerformanceStatus.
	 * @param v Value to Set.
	 */
	public void setKarnofskyperformancestatus(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/karnofskyPerformanceStatus",v);
		_Karnofskyperformancestatus=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Rpaclass=null;

	/**
	 * @return Returns the rpaClass.
	 */
	public String getRpaclass(){
		try{
			if (_Rpaclass==null){
				_Rpaclass=getStringProperty("rpaClass");
				return _Rpaclass;
			}else {
				return _Rpaclass;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for rpaClass.
	 * @param v Value to Set.
	 */
	public void setRpaclass(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/rpaClass",v);
		_Rpaclass=null;
		} catch (Exception e1) {logger.error(e1);}
	}
	 private org.nrg.xdat.om.SfDeficits _Newdeficits =null;

	/**
	 * newDeficits
	 * @return org.nrg.xdat.om.SfDeficits
	 */
	public org.nrg.xdat.om.SfDeficits getNewdeficits() {
		try{
			if (_Newdeficits==null){
				_Newdeficits=((SfDeficits)org.nrg.xdat.base.BaseElement.GetGeneratedItem((XFTItem)getProperty("newDeficits")));
				return _Newdeficits;
			}else {
				return _Newdeficits;
			}
		} catch (Exception e1) {return null;}
	}

	/**
	 * Sets the value for newDeficits.
	 * @param v Value to Set.
	 */
	public void setNewdeficits(ItemI v) throws Exception{
		_Newdeficits =null;
		try{
			if (v instanceof XFTItem)
			{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/newDeficits",v,true);
			}else{
				getItem().setChild(SCHEMA_ELEMENT_NAME + "/newDeficits",v.getItem(),true);
			}
		} catch (Exception e1) {logger.error(e1);throw e1;}
	}

	/**
	 * newDeficits
	 * set org.nrg.xdat.model.SfDeficitsI
	 */
	public <A extends org.nrg.xdat.model.SfDeficitsI> void setNewdeficits(A item) throws Exception{
	setNewdeficits((ItemI)item);
	}

	/**
	 * Removes the newDeficits.
	 * */
	public void removeNewdeficits() {
		_Newdeficits =null;
		try{
			getItem().removeChild(SCHEMA_ELEMENT_NAME + "/newDeficits",0);
		} catch (FieldNotFoundException e1) {logger.error(e1);}
		catch (java.lang.IndexOutOfBoundsException e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _NewdeficitsFK=null;

	/**
	 * @return Returns the condr:clinicalEncounter/newdeficits_sf_deficits_id.
	 */
	public Integer getNewdeficitsFK(){
		try{
			if (_NewdeficitsFK==null){
				_NewdeficitsFK=getIntegerProperty("condr:clinicalEncounter/newdeficits_sf_deficits_id");
				return _NewdeficitsFK;
			}else {
				return _NewdeficitsFK;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for condr:clinicalEncounter/newdeficits_sf_deficits_id.
	 * @param v Value to Set.
	 */
	public void setNewdeficitsFK(Integer v) {
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/newdeficits_sf_deficits_id",v);
		_NewdeficitsFK=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Clinicaldeterioration=null;

	/**
	 * @return Returns the clinicalDeterioration.
	 */
	public Boolean getClinicaldeterioration() {
		try{
			if (_Clinicaldeterioration==null){
				_Clinicaldeterioration=getBooleanProperty("clinicalDeterioration");
				return _Clinicaldeterioration;
			}else {
				return _Clinicaldeterioration;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalDeterioration.
	 * @param v Value to Set.
	 */
	public void setClinicaldeterioration(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/clinicalDeterioration",v);
		_Clinicaldeterioration=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Steroids=null;

	/**
	 * @return Returns the steroids.
	 */
	public Boolean getSteroids() {
		try{
			if (_Steroids==null){
				_Steroids=getBooleanProperty("steroids");
				return _Steroids;
			}else {
				return _Steroids;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for steroids.
	 * @param v Value to Set.
	 */
	public void setSteroids(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/steroids",v);
		_Steroids=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Steroiddose=null;

	/**
	 * @return Returns the steroidDose.
	 */
	public String getSteroiddose(){
		try{
			if (_Steroiddose==null){
				_Steroiddose=getStringProperty("steroidDose");
				return _Steroiddose;
			}else {
				return _Steroiddose;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for steroidDose.
	 * @param v Value to Set.
	 */
	public void setSteroiddose(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/steroidDose",v);
		_Steroiddose=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Steroiddoseincreasing=null;

	/**
	 * @return Returns the steroidDoseIncreasing.
	 */
	public Boolean getSteroiddoseincreasing() {
		try{
			if (_Steroiddoseincreasing==null){
				_Steroiddoseincreasing=getBooleanProperty("steroidDoseIncreasing");
				return _Steroiddoseincreasing;
			}else {
				return _Steroiddoseincreasing;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for steroidDoseIncreasing.
	 * @param v Value to Set.
	 */
	public void setSteroiddoseincreasing(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/steroidDoseIncreasing",v);
		_Steroiddoseincreasing=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Progression=null;

	/**
	 * @return Returns the progression.
	 */
	public Boolean getProgression() {
		try{
			if (_Progression==null){
				_Progression=getBooleanProperty("progression");
				return _Progression;
			}else {
				return _Progression;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for progression.
	 * @param v Value to Set.
	 */
	public void setProgression(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/progression",v);
		_Progression=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Pseudoprogression=null;

	/**
	 * @return Returns the pseudoProgression.
	 */
	public Boolean getPseudoprogression() {
		try{
			if (_Pseudoprogression==null){
				_Pseudoprogression=getBooleanProperty("pseudoProgression");
				return _Pseudoprogression;
			}else {
				return _Pseudoprogression;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for pseudoProgression.
	 * @param v Value to Set.
	 */
	public void setPseudoprogression(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/pseudoProgression",v);
		_Pseudoprogression=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Response=null;

	/**
	 * @return Returns the response.
	 */
	public String getResponse(){
		try{
			if (_Response==null){
				_Response=getStringProperty("response");
				return _Response;
			}else {
				return _Response;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for response.
	 * @param v Value to Set.
	 */
	public void setResponse(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/response",v);
		_Response=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Boolean _Changeintreatment=null;

	/**
	 * @return Returns the changeInTreatment.
	 */
	public Boolean getChangeintreatment() {
		try{
			if (_Changeintreatment==null){
				_Changeintreatment=getBooleanProperty("changeInTreatment");
				return _Changeintreatment;
			}else {
				return _Changeintreatment;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for changeInTreatment.
	 * @param v Value to Set.
	 */
	public void setChangeintreatment(Object v){
		try{
		setBooleanProperty(SCHEMA_ELEMENT_NAME + "/changeInTreatment",v);
		_Changeintreatment=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Changeintreatmentto=null;

	/**
	 * @return Returns the changeInTreatmentTo.
	 */
	public String getChangeintreatmentto(){
		try{
			if (_Changeintreatmentto==null){
				_Changeintreatmentto=getStringProperty("changeInTreatmentTo");
				return _Changeintreatmentto;
			}else {
				return _Changeintreatmentto;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for changeInTreatmentTo.
	 * @param v Value to Set.
	 */
	public void setChangeintreatmentto(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/changeInTreatmentTo",v);
		_Changeintreatmentto=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Clinicalencounternotes=null;

	/**
	 * @return Returns the clinicalEncounterNotes.
	 */
	public String getClinicalencounternotes(){
		try{
			if (_Clinicalencounternotes==null){
				_Clinicalencounternotes=getStringProperty("clinicalEncounterNotes");
				return _Clinicalencounternotes;
			}else {
				return _Clinicalencounternotes;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for clinicalEncounterNotes.
	 * @param v Value to Set.
	 */
	public void setClinicalencounternotes(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/clinicalEncounterNotes",v);
		_Clinicalencounternotes=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.CondrClinicalencounter> getAllCondrClinicalencounters(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrClinicalencounter> al = new ArrayList<org.nrg.xdat.om.CondrClinicalencounter>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrClinicalencounter> getCondrClinicalencountersByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrClinicalencounter> al = new ArrayList<org.nrg.xdat.om.CondrClinicalencounter>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.CondrClinicalencounter> getCondrClinicalencountersByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.CondrClinicalencounter> al = new ArrayList<org.nrg.xdat.om.CondrClinicalencounter>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static CondrClinicalencounter getCondrClinicalencountersById(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("condr:clinicalEncounter/id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (CondrClinicalencounter) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}

	public org.w3c.dom.Document toJoinedXML() throws Exception
	{
		ArrayList al = new ArrayList();
		al.add(this.getItem());
		al.add(org.nrg.xft.search.ItemSearch.GetItem("xnat:subjectData.ID",this.getItem().getProperty("xnat:mrSessionData.subject_ID"),getItem().getUser(),false));
		al.trimToSize();
		return org.nrg.xft.schema.Wrappers.XMLWrapper.XMLWriter.ItemListToDOM(al);
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	        //subjectAssessorData
	        XnatSubjectassessordata childSubjectassessordata = (XnatSubjectassessordata)this.getSubjectassessordata();
	            if (childSubjectassessordata!=null){
	              for(ResourceFile rf: ((XnatSubjectassessordata)childSubjectassessordata).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("subjectAssessorData[" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("subjectAssessorData/" + ((XnatSubjectassessordata)childSubjectassessordata).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	        //newDeficits
	        SfDeficits childNewdeficits = (SfDeficits)this.getNewdeficits();
	            if (childNewdeficits!=null){
	              for(ResourceFile rf: ((SfDeficits)childNewdeficits).getFileResources(rootPath, localLoop)) {
	                 rf.setXpath("newDeficits[" + ((SfDeficits)childNewdeficits).getItem().getPKString() + "]/" + rf.getXpath());
	                 rf.setXdatPath("newDeficits/" + ((SfDeficits)childNewdeficits).getItem().getPKString() + "/" + rf.getXpath());
	                 _return.add(rf);
	              }
	            }
	
	        localLoop = preventLoop;
	
	return _return;
}
}
