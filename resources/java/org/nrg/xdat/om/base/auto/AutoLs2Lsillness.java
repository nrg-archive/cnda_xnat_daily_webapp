/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:59 CDT 2015
 *
 */
package org.nrg.xdat.om.base.auto;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;
import org.nrg.xft.utils.ResourceFile;
import org.nrg.xft.exception.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class AutoLs2Lsillness extends org.nrg.xdat.base.BaseElement implements org.nrg.xdat.model.Ls2LsillnessI {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(AutoLs2Lsillness.class);
	public static String SCHEMA_ELEMENT_NAME="ls2:lsIllness";

	public AutoLs2Lsillness(ItemI item)
	{
		super(item);
	}

	public AutoLs2Lsillness(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use AutoLs2Lsillness(UserI user)
	 **/
	public AutoLs2Lsillness(){}

	public AutoLs2Lsillness(Hashtable properties,UserI user)
	{
		super(properties,user);
	}

	public String getSchemaElementName(){
		return "ls2:lsIllness";
	}

	//FIELD

	private String _Lsillness=null;

	/**
	 * @return Returns the lsIllness.
	 */
	public String getLsillness(){
		try{
			if (_Lsillness==null){
				_Lsillness=getStringProperty("lsIllness");
				return _Lsillness;
			}else {
				return _Lsillness;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for lsIllness.
	 * @param v Value to Set.
	 */
	public void setLsillness(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/lsIllness",v);
		_Lsillness=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Status=null;

	/**
	 * @return Returns the status.
	 */
	public String getStatus(){
		try{
			if (_Status==null){
				_Status=getStringProperty("status");
				return _Status;
			}else {
				return _Status;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for status.
	 * @param v Value to Set.
	 */
	public void setStatus(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/status",v);
		_Status=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private String _Note=null;

	/**
	 * @return Returns the note.
	 */
	public String getNote(){
		try{
			if (_Note==null){
				_Note=getStringProperty("note");
				return _Note;
			}else {
				return _Note;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for note.
	 * @param v Value to Set.
	 */
	public void setNote(String v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/note",v);
		_Note=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	//FIELD

	private Integer _Ls2LsillnessId=null;

	/**
	 * @return Returns the ls2_lsIllness_id.
	 */
	public Integer getLs2LsillnessId() {
		try{
			if (_Ls2LsillnessId==null){
				_Ls2LsillnessId=getIntegerProperty("ls2_lsIllness_id");
				return _Ls2LsillnessId;
			}else {
				return _Ls2LsillnessId;
			}
		} catch (Exception e1) {logger.error(e1);return null;}
	}

	/**
	 * Sets the value for ls2_lsIllness_id.
	 * @param v Value to Set.
	 */
	public void setLs2LsillnessId(Integer v){
		try{
		setProperty(SCHEMA_ELEMENT_NAME + "/ls2_lsIllness_id",v);
		_Ls2LsillnessId=null;
		} catch (Exception e1) {logger.error(e1);}
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsillness> getAllLs2Lsillnesss(org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsillness> al = new ArrayList<org.nrg.xdat.om.Ls2Lsillness>();

		try{
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetAllItems(SCHEMA_ELEMENT_NAME,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsillness> getLs2LsillnesssByField(String xmlPath, Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsillness> al = new ArrayList<org.nrg.xdat.om.Ls2Lsillness>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(xmlPath,value,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static ArrayList<org.nrg.xdat.om.Ls2Lsillness> getLs2LsillnesssByField(org.nrg.xft.search.CriteriaCollection criteria, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		ArrayList<org.nrg.xdat.om.Ls2Lsillness> al = new ArrayList<org.nrg.xdat.om.Ls2Lsillness>();
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems(criteria,user,preLoad);
			al = org.nrg.xdat.base.BaseElement.WrapItems(items.getItems());
		} catch (Exception e) {
			logger.error("",e);
		}

		al.trimToSize();
		return al;
	}

	public static Ls2Lsillness getLs2LsillnesssByLs2LsillnessId(Object value, org.nrg.xft.security.UserI user,boolean preLoad)
	{
		try {
			org.nrg.xft.collections.ItemCollection items = org.nrg.xft.search.ItemSearch.GetItems("ls2:lsIllness/ls2_lsIllness_id",value,user,preLoad);
			ItemI match = items.getFirst();
			if (match!=null)
				return (Ls2Lsillness) org.nrg.xdat.base.BaseElement.GetGeneratedItem(match);
			else
				 return null;
		} catch (Exception e) {
			logger.error("",e);
		}

		return null;
	}

	public static ArrayList wrapItems(ArrayList items)
	{
		ArrayList al = new ArrayList();
		al = org.nrg.xdat.base.BaseElement.WrapItems(items);
		al.trimToSize();
		return al;
	}

	public static ArrayList wrapItems(org.nrg.xft.collections.ItemCollection items)
	{
		return wrapItems(items.getItems());
	}
	public ArrayList<ResourceFile> getFileResources(String rootPath, boolean preventLoop){
ArrayList<ResourceFile> _return = new ArrayList<ResourceFile>();
	 boolean localLoop = preventLoop;
	        localLoop = preventLoop;
	
	return _return;
}
}
