/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCondrBraincolldataTumorloc extends AutoCondrBraincolldataTumorloc {

	public BaseCondrBraincolldataTumorloc(ItemI item)
	{
		super(item);
	}

	public BaseCondrBraincolldataTumorloc(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCondrBraincolldataTumorloc(UserI user)
	 **/
	public BaseCondrBraincolldataTumorloc()
	{}

	public BaseCondrBraincolldataTumorloc(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
