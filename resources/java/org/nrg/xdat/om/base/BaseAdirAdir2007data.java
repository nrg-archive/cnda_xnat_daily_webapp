/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseAdirAdir2007data extends AutoAdirAdir2007data {

	public BaseAdirAdir2007data(ItemI item)
	{
		super(item);
	}

	public BaseAdirAdir2007data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdirAdir2007data(UserI user)
	 **/
	public BaseAdirAdir2007data()
	{}

	public BaseAdirAdir2007data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
