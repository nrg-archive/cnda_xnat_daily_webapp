/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianAdversedataAdverse extends AutoDianAdversedataAdverse {

	public BaseDianAdversedataAdverse(ItemI item)
	{
		super(item);
	}

	public BaseDianAdversedataAdverse(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianAdversedataAdverse(UserI user)
	 **/
	public BaseDianAdversedataAdverse()
	{}

	public BaseDianAdversedataAdverse(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
