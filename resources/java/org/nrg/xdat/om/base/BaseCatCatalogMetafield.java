/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCatCatalogMetafield extends AutoCatCatalogMetafield {

	public BaseCatCatalogMetafield(ItemI item)
	{
		super(item);
	}

	public BaseCatCatalogMetafield(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCatCatalogMetafield(UserI user)
	 **/
	public BaseCatCatalogMetafield()
	{}

	public BaseCatCatalogMetafield(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
