/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianConteligdataContelig extends AutoDianConteligdataContelig {

	public BaseDianConteligdataContelig(ItemI item)
	{
		super(item);
	}

	public BaseDianConteligdataContelig(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianConteligdataContelig(UserI user)
	 **/
	public BaseDianConteligdataContelig()
	{}

	public BaseDianConteligdataContelig(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
