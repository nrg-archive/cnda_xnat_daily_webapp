/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaPsychometricsdata extends AutoCndaPsychometricsdata {

	public BaseCndaPsychometricsdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaPsychometricsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPsychometricsdata(UserI user)
	 **/
	public BaseCndaPsychometricsdata()
	{}

	public BaseCndaPsychometricsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
