/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsB3updrsdata extends AutoUdsB3updrsdata {

	public BaseUdsB3updrsdata(ItemI item)
	{
		super(item);
	}

	public BaseUdsB3updrsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB3updrsdata(UserI user)
	 **/
	public BaseUdsB3updrsdata()
	{}

	public BaseUdsB3updrsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
