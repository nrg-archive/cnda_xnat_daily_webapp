/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaPettimecoursedataRegionActivity extends AutoCndaPettimecoursedataRegionActivity {

	public BaseCndaPettimecoursedataRegionActivity(ItemI item)
	{
		super(item);
	}

	public BaseCndaPettimecoursedataRegionActivity(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataRegionActivity(UserI user)
	 **/
	public BaseCndaPettimecoursedataRegionActivity()
	{}

	public BaseCndaPettimecoursedataRegionActivity(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
