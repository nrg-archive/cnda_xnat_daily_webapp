/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseCndaDiagnosesdata extends AutoCndaDiagnosesdata {

	public BaseCndaDiagnosesdata(ItemI item)
	{
		super(item);
	}

	public BaseCndaDiagnosesdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaDiagnosesdata(UserI user)
	 **/
	public BaseCndaDiagnosesdata()
	{}

	public BaseCndaDiagnosesdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
