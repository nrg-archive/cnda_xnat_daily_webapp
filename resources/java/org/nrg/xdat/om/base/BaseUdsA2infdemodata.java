/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseUdsA2infdemodata extends AutoUdsA2infdemodata {

	public BaseUdsA2infdemodata(ItemI item)
	{
		super(item);
	}

	public BaseUdsA2infdemodata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsA2infdemodata(UserI user)
	 **/
	public BaseUdsA2infdemodata()
	{}

	public BaseUdsA2infdemodata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
