/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseOptiOisscandata extends AutoOptiOisscandata {

	public BaseOptiOisscandata(ItemI item)
	{
		super(item);
	}

	public BaseOptiOisscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseOptiOisscandata(UserI user)
	 **/
	public BaseOptiOisscandata()
	{}

	public BaseOptiOisscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
