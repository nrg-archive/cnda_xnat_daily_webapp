/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseDianLocalcsfdata extends AutoDianLocalcsfdata {

	public BaseDianLocalcsfdata(ItemI item)
	{
		super(item);
	}

	public BaseDianLocalcsfdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianLocalcsfdata(UserI user)
	 **/
	public BaseDianLocalcsfdata()
	{}

	public BaseDianLocalcsfdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
