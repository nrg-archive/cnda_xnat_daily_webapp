/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseSrsSrsver2data extends AutoSrsSrsver2data {

	public BaseSrsSrsver2data(ItemI item)
	{
		super(item);
	}

	public BaseSrsSrsver2data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSrsSrsver2data(UserI user)
	 **/
	public BaseSrsSrsver2data()
	{}

	public BaseSrsSrsver2data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
