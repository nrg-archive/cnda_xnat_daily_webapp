/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class AdrcAdrcclinicaldata extends BaseAdrcAdrcclinicaldata {

	public AdrcAdrcclinicaldata(ItemI item)
	{
		super(item);
	}

	public AdrcAdrcclinicaldata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdrcAdrcclinicaldata(UserI user)
	 **/
	public AdrcAdrcclinicaldata()
	{}

	public AdrcAdrcclinicaldata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
