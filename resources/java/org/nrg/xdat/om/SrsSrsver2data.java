/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class SrsSrsver2data extends BaseSrsSrsver2data {

	public SrsSrsver2data(ItemI item)
	{
		super(item);
	}

	public SrsSrsver2data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseSrsSrsver2data(UserI user)
	 **/
	public SrsSrsver2data()
	{}

	public SrsSrsver2data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
