/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class IqAbstractiqtest extends BaseIqAbstractiqtest {

	public IqAbstractiqtest(ItemI item)
	{
		super(item);
	}

	public IqAbstractiqtest(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseIqAbstractiqtest(UserI user)
	 **/
	public IqAbstractiqtest()
	{}

	public IqAbstractiqtest(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
