/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatResourcecatalog extends BaseXnatResourcecatalog {

	public XnatResourcecatalog(ItemI item)
	{
		super(item);
	}

	public XnatResourcecatalog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatResourcecatalog(UserI user)
	 **/
	public XnatResourcecatalog()
	{}

	public XnatResourcecatalog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
