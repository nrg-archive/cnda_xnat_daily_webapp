/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatVisualspatialtest1 extends BaseCbatVisualspatialtest1 {

	public CbatVisualspatialtest1(ItemI item)
	{
		super(item);
	}

	public CbatVisualspatialtest1(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatVisualspatialtest1(UserI user)
	 **/
	public CbatVisualspatialtest1()
	{}

	public CbatVisualspatialtest1(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
