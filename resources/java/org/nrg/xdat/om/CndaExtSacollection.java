/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CndaExtSacollection extends BaseCndaExtSacollection {

	public CndaExtSacollection(ItemI item)
	{
		super(item);
	}

	public CndaExtSacollection(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaExtSacollection(UserI user)
	 **/
	public CndaExtSacollection()
	{}

	public CndaExtSacollection(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
