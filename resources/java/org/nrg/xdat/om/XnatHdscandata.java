/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatHdscandata extends BaseXnatHdscandata {

	public XnatHdscandata(ItemI item)
	{
		super(item);
	}

	public XnatHdscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatHdscandata(UserI user)
	 **/
	public XnatHdscandata()
	{}

	public XnatHdscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
