/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class CbatCvoe extends BaseCbatCvoe {

	public CbatCvoe(ItemI item)
	{
		super(item);
	}

	public CbatCvoe(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCbatCvoe(UserI user)
	 **/
	public CbatCvoe()
	{}

	public CbatCvoe(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
