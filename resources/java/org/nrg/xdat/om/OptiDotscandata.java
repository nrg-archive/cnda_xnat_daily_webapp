/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class OptiDotscandata extends BaseOptiDotscandata {

	public OptiDotscandata(ItemI item)
	{
		super(item);
	}

	public OptiDotscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseOptiDotscandata(UserI user)
	 **/
	public OptiDotscandata()
	{}

	public OptiDotscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
