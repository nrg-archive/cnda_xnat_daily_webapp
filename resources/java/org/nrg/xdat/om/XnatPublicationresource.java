/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatPublicationresource extends BaseXnatPublicationresource {

	public XnatPublicationresource(ItemI item)
	{
		super(item);
	}

	public XnatPublicationresource(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatPublicationresource(UserI user)
	 **/
	public XnatPublicationresource()
	{}

	public XnatPublicationresource(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
