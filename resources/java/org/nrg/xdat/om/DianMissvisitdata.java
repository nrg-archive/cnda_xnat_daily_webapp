/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianMissvisitdata extends BaseDianMissvisitdata {

	public DianMissvisitdata(ItemI item)
	{
		super(item);
	}

	public DianMissvisitdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianMissvisitdata(UserI user)
	 **/
	public DianMissvisitdata()
	{}

	public DianMissvisitdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
