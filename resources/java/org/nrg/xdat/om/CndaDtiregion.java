//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:40 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.text.NumberFormat;
import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaDtiregion extends BaseCndaDtiregion {

	public CndaDtiregion(ItemI item)
	{
		super(item);
	}

	public CndaDtiregion(UserI user)
	{
		super(user);
	}

	public CndaDtiregion()
	{}

	public CndaDtiregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	/**
	 * @return
	 */
	public ArrayList getManualVolumetryRegions()
	{
	    try {
            return (ArrayList)this.getSubregions_subregion();
        } catch (Exception e) {
            logger.error("",e);
            return new ArrayList();
        }
	}
	
	public CndaManualvolumetryregion getVolumetryRegionByName(String name)
	{
	    Iterator iter = getManualVolumetryRegions().iterator();
	    while (iter.hasNext())
	    {
	        CndaManualvolumetryregion region = (CndaManualvolumetryregion)iter.next();
	        if (region.getName().equalsIgnoreCase(name))
	        {
	            return region;
	        }
	    }
	    return null;
	}
	
	public CndaManualvolumetryregion getVolumetryRegionByName(String name, String hemisphere)
	{
	    Iterator iter = getManualVolumetryRegions().iterator();
	    while (iter.hasNext())
	    {
	        CndaManualvolumetryregion region = (CndaManualvolumetryregion)iter.next();
	        if (region.getName().equalsIgnoreCase(name)
	                && region.getHemisphere().equalsIgnoreCase(hemisphere))
	        {
	            return region;
	        }
	    }
	    return null;
	}
	
	public String getAnisotropyDisplay()
	{
	    Double f= this.getManisotropy();
	    NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
		return formatter.format(f);
	}
	
	public String getDiffusivityDisplay()
	{
	    Double f= this.getMdiffusivity();
	    NumberFormat formatter = NumberFormat.getInstance();
		formatter.setGroupingUsed(false);
		formatter.setMaximumFractionDigits(2);
		formatter.setMinimumFractionDigits(2);
		return formatter.format(f);
	}

}
