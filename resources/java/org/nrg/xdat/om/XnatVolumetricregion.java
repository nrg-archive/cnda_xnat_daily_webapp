/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:38:39 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class XnatVolumetricregion extends BaseXnatVolumetricregion {

	public XnatVolumetricregion(ItemI item)
	{
		super(item);
	}

	public XnatVolumetricregion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatVolumetricregion(UserI user)
	 **/
	public XnatVolumetricregion()
	{}

	public XnatVolumetricregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
