/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class FsAsegregionanalysis extends BaseFsAsegregionanalysis {

	public FsAsegregionanalysis(ItemI item)
	{
		super(item);
	}

	public FsAsegregionanalysis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAsegregionanalysis(UserI user)
	 **/
	public FsAsegregionanalysis()
	{}

	public FsAsegregionanalysis(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
