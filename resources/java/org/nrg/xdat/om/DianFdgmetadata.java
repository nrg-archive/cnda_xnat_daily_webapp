/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class DianFdgmetadata extends BaseDianFdgmetadata {

	public DianFdgmetadata(ItemI item)
	{
		super(item);
	}

	public DianFdgmetadata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseDianFdgmetadata(UserI user)
	 **/
	public DianFdgmetadata()
	{}

	public DianFdgmetadata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
