/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class UdsB6suppdata extends BaseUdsB6suppdata {

	public UdsB6suppdata(ItemI item)
	{
		super(item);
	}

	public UdsB6suppdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseUdsB6suppdata(UserI user)
	 **/
	public UdsB6suppdata()
	{}

	public UdsB6suppdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
