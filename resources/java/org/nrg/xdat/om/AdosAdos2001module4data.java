/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:40:58 CDT 2015
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class AdosAdos2001module4data extends BaseAdosAdos2001module4data {

	public AdosAdos2001module4data(ItemI item)
	{
		super(item);
	}

	public AdosAdos2001module4data(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseAdosAdos2001module4data(UserI user)
	 **/
	public AdosAdos2001module4data()
	{}

	public AdosAdos2001module4data(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
