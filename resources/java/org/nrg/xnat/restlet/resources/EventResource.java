package org.nrg.xnat.restlet.resources;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.hibernate.HibernateException;
import org.nrg.action.ClientException;
import org.nrg.action.ServerException;
import org.nrg.automation.entities.Event;
import org.nrg.automation.services.EventService;
import org.nrg.xdat.XDAT;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.DBPoolException;
import org.restlet.Context;
import org.restlet.data.*;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class EventResource extends AutomationResource {

    public EventResource(Context context, Request request, Response response) throws ResourceException {
        super(context, request, response);

        getVariants().add(new Variant(MediaType.APPLICATION_JSON));
        getVariants().add(new Variant(MediaType.TEXT_HTML));
        getVariants().add(new Variant(MediaType.TEXT_XML));
        getVariants().add(new Variant(MediaType.TEXT_PLAIN));

        _service = XDAT.getContextService().getBean(EventService.class);

        try {
            if (getRequest().getAttributes().containsKey(EVENT_ID)) {
                _eventId = URLDecoder.decode((String) getRequest().getAttributes().get(EVENT_ID), "UTF-8");
            } else {
                _eventId = null;
            }
        } catch (UnsupportedEncodingException exception) {
            // This is the stupidest exception ever. From the docs:
            //
            // The supplied encoding is used to determine what characters are represented by any consecutive sequences
            // of the form "%xy". Note: The World Wide Web Consortium Recommendation states that UTF-8 should be used.
            // Not doing so may introduce incompatibilities.
            //
            // So in other words you have to specify an encoding then handle the exception if you specify an unsupported
            // encoding, except that the only encoding you should use is "UTF-8" and you should specify that every time.
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "Something stupid happened. Sorry about that.", exception);
        }

        if (request.getMethod().equals(Method.PUT) || request.getMethod().equals(Method.DELETE)) {
            if (!user.isSiteAdmin()) {
                throw new ResourceException(Status.CLIENT_ERROR_FORBIDDEN, "Only site administrators can create, update, or delete an event.");
            }
            if (StringUtils.isBlank(_eventId)) {
                throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "You must specify the event ID parameter on the REST URI when creating, updating, or deleting an event.");
            }
        }

        if (_log.isDebugEnabled()) {
            if (StringUtils.isNotBlank(_eventId)) {
                _log.debug("Servicing event request for event ID " + _eventId + " for user " + user.getLogin());
            } else {
                _log.debug("Retrieving available events for user " + user.getLogin());
            }
        }
    }

    @Override
    public boolean allowPut() {
        return true;
    }

    @Override
    public boolean allowDelete() {
        return true;
    }

    @Override
    protected String getResourceType() {
        return "Event";
    }

    @Override
    protected String getResourceId() {
        return _eventId;
    }

    @Override
    public Representation represent(Variant variant) throws ResourceException {
        final MediaType mediaType = overrideVariant(variant);

        try {
            if (StringUtils.isNotBlank(_eventId)) {
                // They're requesting a specific event, so return that to them.
                final XFTTable table = getEventsTable();
                final Map<Object, Object> eventMap = table.convertToHashtable("event_id", "event_label");
                if (!eventMap.containsKey(_eventId)) {
                    throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, "No event of ID " + _eventId + " was found.");
                } else {
                    final String label = (String) eventMap.get(_eventId);
                    final Map<String, String> event = new HashMap<>();
                    event.put("event_id", _eventId);
                    event.put("event_label", label);
                    return new StringRepresentation(MAPPER.writeValueAsString(event), mediaType);
                }
            } else {
                // They're asking for list of existing script events, so give them that.
                final XFTTable table = getEventsTable();
                return representTable(table, mediaType, null);
            }
        } catch (IOException e) {
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "An error occurred marshalling the event data to JSON", e);
        } catch (SQLException | DBPoolException e) {
            throw new ResourceException(Status.SERVER_ERROR_INTERNAL, "An error occurred accessing the database.", e);
        }
    }

    @Override
    public void handlePut() {
        try {
            if (_log.isDebugEnabled()) {
                _log.debug("Preparing to PUT event: " + _eventId);
            }
            putEvent();
        } catch (ClientException e) {
            _log.error("Client error occurred trying to store an event: " + _eventId, e);
            getResponse().setStatus(e.getStatus(), e.getMessage());
        } catch (ServerException e) {
            _log.error("Server error occurred trying to store an event: " + _eventId, e);
            getResponse().setStatus(e.getStatus(), e.getMessage());
        }
    }

    @Override
    public void handleDelete() {
        try {
            if (_log.isDebugEnabled()) {
                _log.debug("Preparing to DELETE event: " + _eventId);
            }
            deleteEvent();
        } catch (ClientException e) {
            _log.error("Client error occurred trying to delete an event: " + _eventId, e);
            getResponse().setStatus(e.getStatus(), e.getMessage());
        } catch (ServerException e) {
            _log.error("Server error occurred trying to delete an event: " + _eventId, e);
            getResponse().setStatus(e.getStatus(), e.getMessage());
        }
    }

    private void putEvent() throws ClientException, ServerException {
        final Representation entity = getRequest().getEntity();
        if (entity.getSize() == 0) {
            logger.warn("Unable to find event parameters: no data sent?");
            getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST, "Unable to find event parameters: no data sent?");
            return;
        }
        MediaType mediaType = entity.getMediaType();
        if (!mediaType.equals(MediaType.APPLICATION_WWW_FORM) && !mediaType.equals(MediaType.APPLICATION_JSON)) {
            throw new ClientException(Status.CLIENT_ERROR_UNSUPPORTED_MEDIA_TYPE, "This function currently only supports " + MediaType.APPLICATION_WWW_FORM + " and " + MediaType.APPLICATION_JSON);
        }

        final Properties properties;
        if (mediaType.equals(MediaType.APPLICATION_WWW_FORM)) {
            try {
                final List<NameValuePair> formMap = URLEncodedUtils.parse(entity.getText(), DEFAULT_CHARSET);
                properties = new Properties();
                for (final NameValuePair entry : formMap) {
                    properties.setProperty(entry.getName(), entry.getValue());
                }
            } catch (IOException e) {
                throw new ServerException(Status.SERVER_ERROR_INTERNAL, "An error occurred trying to read the submitted form body.", e);
            }
        } else {
            try {
                final String text = entity.getText();
                properties = MAPPER.readValue(text, Properties.class);
            } catch (IOException e) {
                throw new ServerException(Status.SERVER_ERROR_INTERNAL, "An error occurred processing the script properties", e);
            }
        }

        final String eventId = properties.getProperty("event_id");
        final String eventLabel = properties.getProperty("event_label");

        final Event event;
        final boolean created;
        if (_service.hasEvent(_eventId)) {
            event = _service.getByEventId(eventId);
            boolean isDirty = false;
            if (!StringUtils.equals(eventId, event.getEventId())) {
                isDirty = true;
                event.setEventId(eventId);
            }
            if (!StringUtils.equals(eventLabel, event.getEventLabel())) {
                isDirty = true;
                event.setEventLabel(eventLabel);
            }
            if (isDirty) {
                _service.update(event);
            }
            created = false;
        } else {
            if (StringUtils.isNotBlank(eventId) && !StringUtils.equals(_eventId, eventId)) {
                throw new ClientException(Status.CLIENT_ERROR_BAD_REQUEST, "You're trying to create a new event with two event IDs: the form parameter must be the same as the event ID parameter on the REST URI.");
            } else {
                event = _service.create(_eventId, eventLabel);
            }
            created = true;
        }

        recordAutomationEvent(event.getEventId(), SITE_SCOPE, created ? "Create" : "Update", Event.class);
    }

    private void deleteEvent() throws ClientException, ServerException {
        if (!_service.hasEvent(_eventId)) {
            throw new ClientException(Status.CLIENT_ERROR_NOT_FOUND, "Couldn't find an event with the event ID " + _eventId);
        }
        try {
            _service.delete(_service.getByEventId(_eventId));
            recordAutomationEvent(_eventId, SITE_SCOPE, "Delete", Event.class);
        } catch (HibernateException e) {
            throw new ServerException(Status.SERVER_ERROR_INTERNAL, "An error occurred trying to delete the event " + _eventId);
        }
    }

    private XFTTable getEventsTable() throws SQLException, DBPoolException {
        final EventService eventService = XDAT.getContextService().getBean(EventService.class);
        final List<Event> events = eventService.getAll();
        final XFTTable table = new XFTTable();
        table.initTable(new String[] { "event_label", "event_id" });

        for (final Event event : events) {
            table.insertRow(new String[] { event.getEventLabel(), event.getEventId() });
        }

        table.sort("event_label", "ASC");
        return table;
    }

    private static final Logger _log = LoggerFactory.getLogger(EventResource.class);
    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private static final String EVENT_ID = "EVENT_ID";

    private final EventService _service;
    private final String _eventId;
}
