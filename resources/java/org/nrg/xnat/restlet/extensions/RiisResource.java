package org.nrg.xnat.restlet.extensions;

import org.nrg.xdat.XDAT;
import org.nrg.xft.XFTTable;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.nrg.riis.services.RiisService;
import org.nrg.riis.entities.RiisLoginRecord;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;

import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.List;

@XnatRestlet({"/services/riis"})
public final class RiisResource extends SecureResource {
   
   // RiisService object - handles Storing and retrieving Login Records sent from the RIIS System. 
   private final RiisService _riisService;
   
   
   // Expected Date format that we are receiving from the user.
   private final SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy_HH:mm:ss");
   
   // This is the maximum number of login records to display, on screen, at one time. 
   // If the database has over 100K rows it will take forever to display
   // So limit it to 2000 in the gui.  If you need more than that go look at 
   // the database table in pgadmin
   private final int MAX_LOGIN_RECORDS = 1500; 
   
   public RiisResource(Context context, Request request, Response response) { 
      super(context, request, response);
      setModifiable(true);
      
      getVariants().add(new Variant(MediaType.APPLICATION_JSON));
      getVariants().add(new Variant(MediaType.TEXT_HTML));
      getVariants().add(new Variant(MediaType.TEXT_XML));
      
      //Retrieve the RIIS Service. 
      _riisService = XDAT.getContextService().getBean(RiisService.class);
   }
   
   @Override
   public void handleDelete(){ 
      //Don't allow delete
      this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN); 
   }
   
   @Override
   public void handlePut(){
      //Don't allow put
      this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN); 
   }
   
   @Override
   public void handlePost() {
      if(_riisService.canUserUseRiis(user.getLogin())){
         //Make sure we recieve the query variables that we are looking for. 
         if(this.containsQueryVariable("projectID") && this.containsQueryVariable("loginTime") && this.containsQueryVariable("aeTitle")){     
            try{
               //Store RIIS Login information and set status.
               _riisService.storeLoginRecord(this.getQueryVariable("projectID"), f.parse(this.getQueryVariable("loginTime")), this.getQueryVariable("aeTitle"));
               this.getResponse().setStatus(Status.SUCCESS_OK,"Login Info Recieved!");
           }
           catch (Exception e){
              // Parse Exception was thrown while parsing the date. (i.e. someone entered a loginTime with Alpha characters.)
              this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Invalid loginTime.");
           }
         } else {
            //These are not the query variables we are looking for.
            this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,"Please specify projectId and loginTime.");
         }
      } else {
         this.getResponse().setStatus(Status.CLIENT_ERROR_UNAUTHORIZED,"Bad Username. Either you entered the wrong username or you are not authorized to use this service. If you believe you received this message in error, please contact your XNAT administrator.");
      }
   }
   
   
 //TODO 
 // Make the get url work like this 
 // /services/riis/ae/{AE_TITLE}?projectID=test1&Date=9/12/12
 // /services/riis?projectID=test1...
 // /services/riis/ae/CNDA_BAY3?projectID...
 // 
   @Override
   public Representation getRepresentation(Variant variant) { 
     final MediaType mt = overrideVariant(variant);
     
     int count =  MAX_LOGIN_RECORDS;
     try{
        if (this.containsQueryVariable("count") && !this.getQueryVariable("count").isEmpty()){
           count = Integer.parseInt(this.getQueryVariable("count"));
        }
     }catch(Exception e){ }
     
     // Get the XFTTable containing the requested Login Records
     XFTTable t = null;
     if (this.containsQueryVariable("aeTitle")) { //Get Login Records for the given aeTitle.
        t = this.getXFTTable(_riisService.getLoginRecordsForAeTitle(this.getQueryVariable("aeTitle"),count));
     } 
     else if(this.containsQueryVariable("projectID")) { //Get Login Records for the given projectID
        t = this.getXFTTable(_riisService.getLoginRecordsForProjectId(this.getQueryVariable("projectID"),count));
     }
     else { //Get All Login Records.
        t = this.getXFTTable(_riisService.getLoginRecords(count));
     } 
       
     if(t == null){ // If the table is null then there is nothing to display to the user.
        this.getResponse().setStatus(Status.SUCCESS_NO_CONTENT,"No Login Records Currently Avaliable.");
        return null;
     }
     
     // Display the login records
     return representTable(t, mt, new Hashtable<String,Object>());
   }
   
   private XFTTable getXFTTable(List <RiisLoginRecord> l){
      // Don't continue if the Login Record List is null or empty
      if(l==null || l.size() == 0){ return null; }
      
      // Build a new XFTTable
      XFTTable t = new XFTTable();
      t.initTable(new String[] {"Project ID", "Login Time", "Ae Title"});
      for(RiisLoginRecord r : l){
         if(null != r){ t.insertRow(new Object[] {r.getProjectId(), r.getLoginTime(), r.getAeTitle()}); }
      }
      return t;
   }
}
