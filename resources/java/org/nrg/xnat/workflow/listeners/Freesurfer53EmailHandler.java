package org.nrg.xnat.workflow.listeners;

import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xft.event.Event;
import org.nrg.xnat.workflow.PipelineEmailHandlerAbst;

import java.util.Map;

/**
 * Created by flavin on 3/2/15.
 */
public class Freesurfer53EmailHandler extends PipelineEmailHandlerAbst {
    final static Logger logger = Logger.getLogger(Freesurfer53EmailHandler.class);

    private final String PIPELINE_NAME = "Freesurfer/Freesurfer_5.3.xml";
    private final String PIPELINE_NAME_PRETTY = "Freesurfer 5.3";


    public void handleEvent(Event e, WrkWorkflowdata wrk) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("pipelineName",PIPELINE_NAME_PRETTY);
        if (completed(e)) {
            standardPipelineEmailImpl(e, wrk, PIPELINE_NAME, DEFAULT_TEMPLATE_SUCCESS, "processed with "+PIPELINE_NAME_PRETTY, "processed.lst", params);
        } else if (failed(e)) {
            standardPipelineEmailImpl(e, wrk, PIPELINE_NAME, DEFAULT_TEMPLATE_FAILURE, DEFAULT_SUBJECT_FAILURE, "processed.lst", params);
        }
    }
}
