package org.nrg.xnat.workflow.listeners;

import com.google.common.collect.Maps;
import org.apache.log4j.Logger;
import org.nrg.xdat.om.WrkWorkflowdata;
import org.nrg.xft.event.Event;
import org.nrg.xnat.workflow.PipelineEmailHandlerAbst;

import java.util.Map;

public class RegisterMREmailHandler extends PipelineEmailHandlerAbst {
    final static Logger logger = Logger.getLogger(RegisterMREmailHandler.class);

    private final String PIPELINE_NAME = "RegisterMR/RegisterMRToAtlas.xml";
    private final String PIPELINE_NAME_PRETTY = "RegisterMR";


    public void handleEvent(Event e, WrkWorkflowdata wrk) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("pipelineName",PIPELINE_NAME_PRETTY);
        if (completed(e)) {
            standardPipelineEmailImpl(e, wrk, PIPELINE_NAME, DEFAULT_TEMPLATE_SUCCESS, "registered to atlas", "processed.lst", params);
        } else if (failed(e)) {
            standardPipelineEmailImpl(e, wrk, PIPELINE_NAME, DEFAULT_TEMPLATE_FAILURE, DEFAULT_SUBJECT_FAILURE, "processed.lst", params);
        }
    }
}
