package org.nrg.xnat.turbine.modules.screens;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xnat.turbine.modules.screens.DefaultPipelineScreen;

public class PipelineScreen_ManualPetProcessing extends DefaultPipelineScreen
{
    /**
     * Place all the data object in the context
     * for use in the template.
     */
	static Logger logger = Logger.getLogger(PipelineScreen_ManualPetProcessing.class);

    public void finalProcessing(RunData data, Context context)
    {
        XnatPetsessiondata pet = new XnatPetsessiondata(item);

        if (pet.getScans_scan() == null || pet.getScans_scan().size() == 0) {
            data.setMessage("No files uploaded for the session. Please upload files first");
            return;
        }

        ArrayList associatedMrs = getMrItems(data,pet.getSubjectId());

        context.put("mrs",associatedMrs);
        context.put("pet", pet);
        context.put("step",1);
        context.put("step1Disabled","disabled");
        boolean registered = pet.hasMPRAGEInAtlasSpace();
        if (!registered) {
            context.put("step2Disabled","disabled");
            context.put("step3Disabled","disabled");
        }else {
            if (pet.getRegions_region()!= null && pet.getRegions_region().size()>0) {
                context.put("step2Disabled","disabled");
                context.put("step3Disabled","");
            }else {
                context.put("step2Disabled","");
                context.put("step3Disabled","disabled");
            }
        }
        if (registered) {
            data.setMessage("NOTE: This session has already been atlas registered. Rebuilding will result in deletion of ALL ROI files and time course analysis");
        }
        context.put("projectSettings", projectParameters);
    }



    private boolean earlierInTime(String time1, String time2) {
        boolean rtn = false;
        if (time1 == null || time2 == null) return true;
        String t1[] = time1.split(":");
        String t2[] = time2.split(":");
        if (t1 != null && t2 != null ) {
            int h1 = Integer.parseInt(t1[0]);
            int m1 = Integer.parseInt(t1[1]);
            int s1 = Integer.parseInt(t1[2]);
            int h2 = Integer.parseInt(t2[0]);
            int m2 = Integer.parseInt(t2[1]);
            int s2 = Integer.parseInt(t2[2]);
            if (h1 < h2 ) {return true;}
            if (h1 > h2) {return false;}
            if (m1 < m2) {return true;}
            if (m1 > m2) {return false;}
            if (s1 < s2) {return true;}
            if (s1 > s2) {return false;}
            return true;
        }
        return rtn;
    }

    private ArrayList getMrItems(RunData data, String search_value) {
        ArrayList rtn = new ArrayList();
        XnatMrsessiondata om = null;
        try {
            ItemSearch search = new ItemSearch();
            search.setUser(TurbineUtils.getUser(data));
            String elementName = "xnat:mrSessionData";
            SchemaElementI gwe = SchemaElement.GetElement(elementName);
            search.setElement(elementName);
            search.addCriteria("xnat:mrSessionData.subject_ID",search_value);
            boolean b = false;
            b= gwe.isPreLoad();
            search.setAllowMultiples(b);
            ItemCollection items = search.exec();
            ArrayList mrsByDate = items.getItems("xnat:mrSessionData.Date","DESC");
            if (mrsByDate.size() > 0)
            {
                for (int i =0; i <mrsByDate.size(); i++) {
                    ItemI item = (ItemI)mrsByDate.get(i);
                    om = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
                    rtn.add(om);
                }
                return rtn;
            }else{
                return rtn;
            }
        }catch(Exception e) {e.printStackTrace();}
        return rtn;
    }


}
