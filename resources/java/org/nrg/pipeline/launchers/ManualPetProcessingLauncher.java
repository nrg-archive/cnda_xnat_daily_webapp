/*
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 *
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.PipelineFileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatRegionresourceI;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatRegionresource;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;

public class ManualPetProcessingLauncher extends PipelineLauncher{

	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ManualPetProcessingLauncher.class);

	public boolean launchRoi(RunData data, Context context) {
		boolean rtn = true;
		try {
		  ItemI data_item = TurbineUtils.GetItemBySearch(data);
	       XnatPetsessiondata pet = new XnatPetsessiondata(data_item);

	       String pXml =  data.getParameters().get("pipelineName");
	       XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncherForExperiment(data, context,pet);
	        xnatPipelineLauncher.setPipelineName(pXml);


	        LinkedHashMap<String, String> roiList = new LinkedHashMap<String,String>();

	       addRoiRootParameter(data,pet,roiList);
//	       ArrayList assessed = pet.getAssessedRegions();
//	       addRoiRootParameter(data,assessed,roiList);

	       Parameters parameters = Parameters.Factory.newInstance();

	       ParameterData param = parameters.addNewParameter();
	     	param.setName("pet_xnatId");
	     	Values val = param.addNewValues();
	     	val.setUnique(pet.getId());

	     	param = parameters.addNewParameter();
	     	param.setName("petlabel");
	     	val = param.addNewValues();
	     	val.setUnique(pet.getLabel());

	     	param = parameters.addNewParameter();
	     	param.setName("pet_archivedir");
	     	val = param.addNewValues();
	     	val.setUnique(pet.getArchivePath());


	     	param = parameters.addNewParameter();
	     	param.setName("subject");
	     	val = param.addNewValues();
	     	val.setUnique(pet.getSubjectId());

	     	String T1=null;
	     	List<XnatAbstractresourceI> resources = pet.getResources_resource();
	     	for (int i=0; i< resources.size(); i++) {
	     		XnatResourcecatalog resource = (XnatResourcecatalog)resources.get(i);
	     		if (resource.getLabel().equalsIgnoreCase("MPRAGE_222")) {
	     			String content = resource.getContent();
	     			int k = content.lastIndexOf("_MPRAGE222");
	     			T1=content.substring(0, k)+"_mpr";
	     			break;
	     		}
	     	}

			param = parameters.addNewParameter();
	    	param.setName("tracer");
	    	param.addNewValues().setUnique(pet.getTracer_name());


	        param = parameters.addNewParameter();
	     	param.setName("t1");
	     	val = param.addNewValues();
	     	val.setUnique(T1);

	     	String petScanId = data.getParameters().get("pet_scanId");

/*	     	param = parameters.addNewParameter();
	     	param.setName("isstat");
	     	val = param.addNewValues();
	     	val.setUnique(pet.getScanById(petScanId).getFrames().intValue()>6?"0":"1");*/

	     	int isecat = isEcat(pet,petScanId);
	     	param = parameters.addNewParameter();
	     	param.setName("isecat");
	     	val = param.addNewValues();
	     	val.setUnique(""+isecat);


	     	param = parameters.addNewParameter();
	     	param.setName("ROINAMES");
	     	val = param.addNewValues();
	     	Set set = roiList.entrySet();
	        // Get an iterator
	        Iterator iter = set.iterator();
	        // Display elements
	        while(iter.hasNext()) {
	           Map.Entry me = (Map.Entry)iter.next();
	     		String regionName=(String)me.getKey();
	     		val.addList("TOT_"+regionName.replace(" ", ""));
	     	}
	     	param = parameters.addNewParameter();
	     	param.setName("ROIROOTS");
	     	val = param.addNewValues();
	     	set = roiList.entrySet();
	        // Get an iterator
	        iter = set.iterator();
	        // Display elements
	        while(iter.hasNext()) {
	           Map.Entry me = (Map.Entry)iter.next();
	     		String regionRoot=(String)me.getValue();
	     		val.addList(regionRoot);
	     	}

			String[] paramNames = {"mst", "mdt", "hl","pet_scanId","isstat"};
			for (int i=0; i< paramNames.length; i++) {
				String paramName = paramNames[i];
				String value = (String)org.nrg.xdat.turbine.utils.TurbineUtils.GetPassedParameter(paramName,data);
				if (value != null) {
					param = parameters.addNewParameter();
			        param.setName(paramName);
			        param.addNewValues().setUnique(value);
				}
			}


	   	String paramFileName = getName(pXml );
 		Date date = new Date();
 	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
 	    String s = formatter.format(date);

 		paramFileName += "_params_" + s + ".xml";

 		String buildDir = PipelineFileUtils.getBuildDir(pet.getProject(), true);
 		buildDir +=   "pib"  ;
 		xnatPipelineLauncher.setBuildDir(buildDir);
 		xnatPipelineLauncher.setNeedsBuildDir(false);

 		String paramFilePath = saveParameters(buildDir+File.separator + pet.getLabel(),paramFileName,parameters);
 	    xnatPipelineLauncher.setParameterFile(paramFilePath);

	    rtn  = xnatPipelineLauncher.launch();

		}catch(Exception e) {
			logger.error("Couldnt launch ROI", e);
			rtn = false;
		}
		return rtn;
	}


	private int isEcat(XnatPetsessiondata pet, String id) {
		int _rtn=0;
		XnatImagescandata scan = pet.getScanById(id);
		List<XnatAbstractresourceI> files = scan.getFile();
		for (int i=0;i<files.size();i++) {
			XnatAbstractresource file = (XnatAbstractresource)files.get(i);
			if (file.getContent().equalsIgnoreCase("RAW")) {
				if (file.getFormat().equals("ECAT"))
					_rtn = 1;
			  break;
			}else if (file.getContent().equalsIgnoreCase("Dynamic emission_RAW")) {
				if (file.getFormat().equals("ECAT") || file.getFormat().equals("MATRIX72v"))
					_rtn = 1;
			  break;
			}
		}
		return _rtn;
	}


	private XnatRegionresource getRegion(XnatPetsessiondata pet,String regionName) {
		XnatRegionresource region = null;
		List<XnatRegionresourceI> regions = pet.getRegions_region();
		for (int i = 0; i < regions.size(); i++) {
		   if (regions.get(i).getName().equalsIgnoreCase(regionName)) {
			   region = (XnatRegionresource)regions.get(i);
			   break;
		   }
		}
		return region;
	}


	  private void addRoiRootParameter(RunData data,XnatPetsessiondata pet, LinkedHashMap<String,String> rois) throws IOException{
		  String[] roisByOrder={"brainstem", "cerebellum","mcbp1","mcbp2","mcbp3","mcbp4"};

		  Hashtable<String,String> roisByOrderHash = new Hashtable<String,String>();

          //Expect to see the Brainstem first
          //THen Reference region
          //Then the other MCBP regions

		  for (int i=0; i<roisByOrder.length; i++) {
			  String regionName = data.getParameters().get(roisByOrder[i]);
			  XnatRegionresource roiByOrderRegion =   (XnatRegionresource)getRegion(pet,regionName);
			  if (roiByOrderRegion != null) {
				  String uri = ((XnatResource)roiByOrderRegion.getFile()).getUri();
				  if (uri != null) {
		               String roi_root = extractRoot(uri);
		               if (roi_root != null) {
		                   rois.put(roiByOrderRegion.getName(), roi_root);
		                   roisByOrderHash.put(roiByOrderRegion.getName(), "1");
		               }
		           }
			  }

		  }

		  List<XnatRegionresourceI> list = pet.getRegions_region();
		  for (int i = 0; i < list.size(); i++) {
	           XnatRegionresource region = (XnatRegionresource)list.get(i);
	           if (!roisByOrderHash.containsKey(region.getName())) {
	        	   String uri = ((XnatResource)region.getFile()).getUri();
	        	   if (uri != null) {
		               String roi_root = extractRoot(uri);
		               if (roi_root != null) {
		                   rois.put(region.getName(), roi_root);
		               }
		           }
	           }
	       }
	   }


	  private String extractRoot(String uri) {
	       String rtn = null;
	       if (uri == null) return rtn;
	       int slash = uri.lastIndexOf("/");
	       if (slash != -1) {
	           rtn = uri.substring(slash+1);
	       }else {
	           rtn = uri;
	       }
	       int dot = rtn.indexOf(".");
	       if (dot != -1) {
	           rtn = rtn.substring(0, dot);
	       }
	       return rtn;
	   }

	public boolean launch(RunData data, Context context) {
		boolean rtn = true;
		try {
	        ItemI data_item = TurbineUtils.GetItemBySearch(data);
	        XnatPetsessiondata pet = new XnatPetsessiondata(data_item);
	        /*String path = getPathToDynamicEmission(pet);
	        if (path == null) {
	             rtn = false;
	             return rtn;
	        }*/

	        String pXml =  data.getParameters().get("pipelineName");
	        XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncherForExperiment(data, context, pet);
	        xnatPipelineLauncher.setPipelineName(pXml);

	        String mrsessionId = data.getParameters().get("mprage_sessionId");
	        String mrscanId = data.getParameters().get("mprage_scanid#"+mrsessionId);
	        String isIma =  isIma(data,mrsessionId, mrscanId);


	        Parameters parameters = Parameters.Factory.newInstance();

	 	  ParameterData param = parameters.addNewParameter();
	     	param.setName("atlas");
	     	param.addNewValues().setUnique(data.getParameters().get("atlas"));

	    	param = parameters.addNewParameter();
	     	param.setName("pet_xnatId");
	     	param.addNewValues().setUnique(pet.getId());


	    	param = parameters.addNewParameter();
	     	param.setName("mr_xnatId");
	     	param.addNewValues().setUnique(mrsessionId);


	    	param = parameters.addNewParameter();
	     	param.setName("sessionId");
	     	param.addNewValues().setUnique(getMrSessionLabel(data,context,mrsessionId));

	    	param = parameters.addNewParameter();
	     	param.setName("pet_sessionId");
	     	param.addNewValues().setUnique(pet.getLabel());

	    	param = parameters.addNewParameter();
	     	param.setName("scan_id");
	     	param.addNewValues().setUnique(mrscanId);


	     	String paramFileName = getName(pXml );
	 		Date date = new Date();
	 	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	 	    String s = formatter.format(date);

	 		paramFileName += "_params_" + s + ".xml";

	 		String buildDir = PipelineFileUtils.getBuildDir(pet.getProject(), true);
	 		buildDir +=   "pib"  ;
	 		xnatPipelineLauncher.setBuildDir(buildDir);
	 		xnatPipelineLauncher.setNeedsBuildDir(false);

	 		String paramFilePath = saveParameters(buildDir+File.separator + pet.getLabel(),paramFileName,parameters);
	 	    xnatPipelineLauncher.setParameterFile(paramFilePath);
	       rtn = xnatPipelineLauncher.launch();

	       String destinationPage = data.getParameters().get("destinationpage");
	       data.getParameters().remove("pipelineName");

	        if (destinationPage != null) {
	            data.setRedirectURI(TurbineUtils.GetRelativeServerPath(data)+ "/app/template/" + destinationPage + "/search_field/" + data.getParameters().get("search_field") +  "/search_value/" +  data.getParameters().get("search_value")  + "/search_element/" +  data.getParameters().get("search_element"));
	        }else {
	            data.setMessage("<p><b>The build process was successfully launched.  Status email will be sent upon its completion.</b></p>");
	            data.setScreenTemplate("ClosePage.vm");
	        }}catch(Exception e){e.printStackTrace(); rtn = false;}
	        return rtn;
	}

	 private String isIma(RunData data, String sessionId, String scanId) {
	       String rtn = "0";
	       XnatMrsessiondata mr = null;
	       try {
	           ItemSearch search = new ItemSearch();
	           search.setUser(TurbineUtils.getUser(data));
	           String elementName = "xnat:mrSessionData";
	           SchemaElementI gwe = SchemaElement.GetElement(elementName);
	           search.setElement(elementName);
	           search.addCriteria("xnat:mrSessionData.ID",sessionId);
	           boolean b = false;
	           b= gwe.isPreLoad();
	           search.setAllowMultiples(b);
	           ItemCollection items = search.exec();
	           mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(items.getFirst());
	           XnatImagescandataI selectedScan = mr.getScanById(scanId);
		       if (((ItemI)selectedScan.getFile().get(0)).getXSIType().equals("xnat:dicomSeries") || ((ItemI)selectedScan.getFile().get(0)).getXSIType().equals("xnat:resourceCatalog") ) {
				 rtn = "0";
	 		   } else if (((ItemI)selectedScan.getFile().get(0)).getXSIType().equals("xnat:imageResourceSeries")) {
				 rtn = "1";
			   }

	       }catch(Exception e) {logger.error("getDcmRoot ",e);}
	       return rtn;

	   }






	   private String getMrSessionArchiveDir(RunData data, Context context, String mrid) throws Exception {
	       String rtn = null;
	       ItemSearch search = new ItemSearch();
	       search.setUser(TurbineUtils.getUser(data));
	       String elementName = "xnat:mrSessionData";
	       search.setElement(elementName);
	       search.addCriteria("xnat:mrSessionData.ID",mrid);
	       search.setAllowMultiples(false);
	       ItemCollection items = search.exec();
	       if (items.size() > 0)
	       {
	           ItemI item = items.getFirst();
	           XnatMrsessiondata mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
	           rtn = mr.getArchivePath();
	           if (rtn.endsWith(File.separator)) rtn = rtn.substring(0, rtn.length()-1);
	           return rtn;
	       }else{
	           return rtn;
	       }
	   }

	   private String getMrSessionLabel(RunData data, Context context, String mrid) throws Exception {
	       String rtn = null;
	       ItemSearch search = new ItemSearch();
	       search.setUser(TurbineUtils.getUser(data));
	       String elementName = "xnat:mrSessionData";
	       search.setElement(elementName);
	       search.addCriteria("xnat:mrSessionData.ID",mrid);
	       search.setAllowMultiples(false);
	       ItemCollection items = search.exec();
	       if (items.size() > 0)  {
	           ItemI item = items.getFirst();
	           XnatMrsessiondata mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
	           rtn = mr.getLabel();
	       }
	           return rtn;
	       }


}
