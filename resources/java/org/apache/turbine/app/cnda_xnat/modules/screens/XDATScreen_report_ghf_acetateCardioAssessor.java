/*
 * GENERATED FILE
 * Created on Mon Aug 09 15:50:02 CDT 2010
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.text.DecimalFormat;

import org.apache.commons.math.stat.descriptive.SummaryStatistics;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.GhfAcetatecardioassessor;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

/**
 * @author XDAT
 *
 */
public class XDATScreen_report_ghf_acetateCardioAssessor extends SecureReport {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_ghf_acetateCardioAssessor.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	private static final DecimalFormat threePlaces = new DecimalFormat("0.000");
	
	public void finalProcessing(RunData data, Context context) {		
		GhfAcetatecardioassessor gaca = (GhfAcetatecardioassessor) om;
		
		// format partial volume/spillover correction fractions to show 3 places
		context.put("fbb", threePlaces.format(gaca.getPvscorrectfracs_fbb()));
		context.put("fbm", threePlaces.format(gaca.getPvscorrectfracs_fbm()));
		context.put("fmb", threePlaces.format(gaca.getPvscorrectfracs_fmb()));
		context.put("fmm", threePlaces.format(gaca.getPvscorrectfracs_fmm()));
		
		// format Km to show 3 places
		context.put("kmroi1", threePlaces.format(gaca.getKm_roi1()));
		context.put("kmroi2", threePlaces.format(gaca.getKm_roi2()));
		context.put("kmroi3", threePlaces.format(gaca.getKm_roi3()));
		
		// calculate Km mean/stddev/cov
		calculateStats("km", context, gaca.getKm_roi1(), gaca.getKm_roi2(), gaca.getKm_roi3());
		
		// calculate RPP for each time point
		Double baselineheart_rpp = gaca.getBaselineheart_sbp() * gaca.getBaselineheart_hr();
		Double fifteenminheart_rpp = gaca.getFifteenminheart_sbp() * gaca.getFifteenminheart_hr();
		Double thirtyminheart_rpp = gaca.getThirtyminheart_sbp() * gaca.getThirtyminheart_hr();
		
		context.put("baselinerpp", baselineheart_rpp.toString());
		context.put("fifteenminrpp", fifteenminheart_rpp.toString());
		context.put("thirtyminrpp", thirtyminheart_rpp.toString());
		
		// calculate glucose mean/stddev/cov
		calculateStats("gluc", context,
					   gaca.getBaselineplasma_gluc(),
					   gaca.getFifteenminplasma_gluc());
		
		// calculate ffa mean/stddev/cov
		calculateStats("ffa", context,
					   gaca.getBaselineplasma_ffa(),
					   gaca.getFifteenminplasma_ffa());
		
		// calculate sbp mean/stddev/cov
		calculateStats("sbp", context,
					   gaca.getBaselineheart_sbp().doubleValue(),
					   gaca.getFifteenminheart_sbp().doubleValue(),
					   gaca.getThirtyminheart_sbp().doubleValue());
		
		// calculate dbp mean/stddev/cov
		calculateStats("dbp", context,
				   gaca.getBaselineheart_dbp().doubleValue(),
				   gaca.getFifteenminheart_dbp().doubleValue(),
				   gaca.getThirtyminheart_dbp().doubleValue());
		
		// calculate hr mean/stddev/cov
		calculateStats("hr", context,
				   gaca.getBaselineheart_hr().doubleValue(),
				   gaca.getFifteenminheart_hr().doubleValue(),
				   gaca.getThirtyminheart_hr().doubleValue());
		
		// calculate rpp mean/stddev/cov
		calculateStats("rpp", context,
				   baselineheart_rpp.doubleValue(),
				   fifteenminheart_rpp.doubleValue(),
				   thirtyminheart_rpp.doubleValue());
	}
	
	private void calculateStats(String prefix, Context context, double... values) {
		SummaryStatistics sumstats = new SummaryStatistics();
		
		for (Double val : values)
			sumstats.addValue(val);
		
		context.put(prefix + "mean", threePlaces.format(sumstats.getMean()));
		context.put(prefix + "stddev", threePlaces.format(sumstats.getStandardDeviation()));
		context.put(prefix + "cov", threePlaces.format((sumstats.getStandardDeviation() / sumstats.getMean()) * 100));
	}
}
