/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class BuildScreen_PIBBuildROI extends SecureReport {
    public void finalProcessing(RunData data, Context context)
    {
        XnatPetsessiondata pet = new XnatPetsessiondata(item);
        context.put("pet", pet);
        context.put("step",3);
        context.put("step1Disabled","");
        context.put("step2Disabled","");
        context.put("step3Disabled","disabled");

    }
    
}
