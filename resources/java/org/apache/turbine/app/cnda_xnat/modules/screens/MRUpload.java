//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * Created on May 27, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.io.File;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
/**
 * @author Tim
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class MRUpload extends SecureReport {

    public void preProcessing(RunData data, Context context)
    {
        TurbineUtils.InstanciatePassedItemForScreenUse(data,context);
    }
    
	public void finalProcessing(RunData data, Context context)
	{
		String dest = "";
		try {
		    if (context.get("om")==null)
	        {
	            String errorString = "An error has occurred.";
				errorString += "<p>Please contact the <a href=\"mailto:cnltech@iacmail.wustl.edu?subject=archive submission error\">CNL techdesk</a> to resolve the error.</p>";
				errorString += "<p><br>To continue managing, select from the following tasks:<p>";
				data.setMessage(errorString);

				data.setScreenTemplate("Error.vm");
				return;	
	        }        
	        
	        XnatMrsessiondata mr = (XnatMrsessiondata)context.get("om");
		    
		    dest = mr.getPrearchivepath(TurbineUtils.getUser(data));
			
			if (dest == null || dest.equalsIgnoreCase(""))
			{
				dest = "/home/iac3/big/test/";
			}		
		
			if (!dest.endsWith(File.separator))
			{
				dest = dest + File.separator;
			}
			
			if(mr != null)
			{
				dest += mr.getId() + File.separator;
			}
			context.put("dest",dest);	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
