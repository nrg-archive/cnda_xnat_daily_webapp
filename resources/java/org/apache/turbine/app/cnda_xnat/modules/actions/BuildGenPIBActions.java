/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.actions;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.launchers.PetProcessingLauncher;
import org.nrg.xdat.om.XnatImageresource;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatRegionresource;
import org.nrg.xdat.om.XnatRegionresourceLabel;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.event.EventUtils;
import org.nrg.xft.event.persist.PersistentWorkflowI;
import org.nrg.xft.event.persist.PersistentWorkflowUtils;

public class BuildGenPIBActions extends SecureAction

{

    static org.apache.log4j.Logger logger = Logger.getLogger(BuildGenPIBActions.class);



   public void doPerform(RunData data, Context context){
   }
   
   public void doBuild(RunData data, Context context) throws Exception{
	   PetProcessingLauncher stdBuild = new PetProcessingLauncher();
    	boolean success = stdBuild.launch(data, context);
    	if (success) {
    		data.setMessage("<p><b>The build process was successfully launched.  Status email will be sent upon its completion.</b></p>");
    	}else {
    		   data.setMessage("<p><b>Unable to launch pipeline. Please contact administrator at " +AdminUtils.getAdminEmailId() + " </b></p>");
    	}
	       data.setScreenTemplate("ClosePage.vm");
   }
   
   public void doBuildroi(RunData data, Context context) throws Exception {
	   PetProcessingLauncher stdBuild = new PetProcessingLauncher();
   		boolean success = stdBuild.launchRoi(data, context);
   		if (success) {
	       data.setMessage("<p><b>The build process was successfully queued.  Status email will be sent upon its completion.</b></p>");
   		}else {
 		   data.setMessage("<p><b>Unable to launch pipeline. Please contact administrator at " +AdminUtils.getAdminEmailId() + " </b></p>");
   		}
	       data.setScreenTemplate("ClosePage.vm");
   }
   
 
 
   
   public void doSaveroi(RunData data, Context context) throws Exception{

       XnatPetsessiondata formPet = (XnatPetsessiondata)data.getSession().getAttribute("pet_image_upload");
       data.getSession().removeAttribute("pet_image_upload");
       XDATUser user = TurbineUtils.getUser(data);
       ItemI data_item = TurbineUtils.GetItemBySearch(data);
       XnatPetsessiondata dbPet = new XnatPetsessiondata(data_item);
       
       PersistentWorkflowI wrk= PersistentWorkflowUtils.buildOpenWorkflow(user, dbPet.getItem(), EventUtils.newEventInstance(EventUtils.CATEGORY.DATA, EventUtils.TYPE.WEB_FORM, "ROIs Uploaded"));
       
       try {
		if (formPet.getRegions_region() != null && formPet.getRegions_region().size() > 0) {
		       for (int i = 0; i < formPet.getRegions_region().size(); i++) {
		           XnatRegionresource regionRsc = (XnatRegionresource)formPet.getRegions_region().get(i);
		           String userRegionName = data.getParameters().get("region#" + ((XnatImageresource)regionRsc.getFile()).getUri());
		           regionRsc.setName(userRegionName);
		           String hemisphere = data.getParameters().get("hemisphere#" + ((XnatImageresource)regionRsc.getFile()).getUri());
		           regionRsc.setHemisphere(hemisphere);
		           regionRsc.setSessionId(formPet.getId());
		           ArrayList matchingFiles = dbPet.getReconstructedFileByContent("MPRAGE_222");
		           if (matchingFiles != null && matchingFiles.size() > 0 ) {
		               regionRsc.setBaseimage((ItemI)matchingFiles.get(0));
		               ArrayList subRegions = (ArrayList)regionRsc.getSubregionlabels_label();
		               if (subRegions != null && subRegions.size() > 0) {
		                   for (int j = 0; j < subRegions.size(); j++) {
		                       XnatRegionresourceLabel regionlbl = (XnatRegionresourceLabel)subRegions.get(j); 
		                       String lbl = data.getParameters().get("label#" + ((XnatImageresource)regionRsc.getFile()).getUri() + "#" + regionlbl.getId());
		                       hemisphere = data.getParameters().get("label_hemisphere#" + ((XnatImageresource)regionRsc.getFile()).getUri() + "#" + regionlbl.getId());
		                       regionlbl.setLabel(lbl);
		                       regionlbl.setHemisphere(hemisphere);
		                   }
		                   dbPet.setRegions_region(regionRsc);
		                   dbPet.save(user,false,true,wrk.buildEvent());
		               }
		           }else {
		               throw new Exception("BuildGenPIBActions::doSaveroi couldt locate MPRAGE_222 image type for pet session " + dbPet.getId());
		           }
		       }
		       
		       PersistentWorkflowUtils.complete(wrk, wrk.buildEvent());
		       
		       data.setMessage("<p><b>The ROI files were successfully uploaded.</b></p>");
		       String buildpage = data.getParameters().get("buildpage");
		       String buildroipage = data.getParameters().get("buildroipage");
		       String createroipage = data.getParameters().get("createroipage");
		       data.setRedirectURI(TurbineUtils.GetRelativeServerPath(data) + "" + "/app/template/BuildGenPIBSelector.vm/search_value/"+dbPet.getId()+"/search_element/xnat:petSessionData/search_field/xnat:petSessionData.ID/popup/true/buildpage/" + buildpage + "/buildroipage/"+buildroipage + "/createroipage/"+createroipage);
		   }
	} catch (Exception e) {
		PersistentWorkflowUtils.fail(wrk, wrk.buildEvent());
		throw e;
	}
   }
   
   
}
