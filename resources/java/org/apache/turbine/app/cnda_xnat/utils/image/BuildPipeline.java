//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
package org.apache.turbine.app.cnda_xnat.utils.image;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTItem;


/**
 * Grab all the records in a table using a Peer, and
 * place the Vector of data objects in the context
 * where they can be displayed by a #foreach loop.
 *
 * @author <a href="mailto:jvanzyl@periapt.com">Jason van Zyl</a>
 */
public class BuildPipeline 
{
    public static void doSetup(RunData data, Context context) 
    {
    	
		try {
					XFTItem item = (XFTItem)TurbineUtils.GetItemBySearch(data);
					
					if (item ==null)
					{
					    System.out.println("NO MR FOUND.");
						data.setMessage("Technical Error! Please contact <a href=\"<mailto:" + AdminUtils.getAdminEmailId() + "?subject=archive submission error\">CNL techdesk</a>");
						data.setScreenTemplate("Error.vm");
						return;	
					}
	   			  						
					if (!TurbineUtils.getUser(data).canCreate(item) || TurbineUtils.getUser(data).canEdit(item) || TurbineUtils.getUser(data).canRead(item)) {
						String mesg = "You do not have Read or Create or Update permissions for MrSession. Please contact <mailto:" + AdminUtils.getAdminEmailId() +"?subject=CNDA Assistance: Expt Permissions>" ;
						System.out.println("\n :" + mesg + ": \n");
						data.setMessage(mesg);
						data.setScreenTemplate("PermissionError.vm");
						return;	
					}
	   			  
	   			  	//System.out.println("\n\n Entering BuildPipeline ArcSession Manage\n\n");
					//String arc = ArcProperties.getSessionPath(_sessionId);
					
					XnatMrsessiondata mr = new XnatMrsessiondata(item);
					
					//Below is equivalent to the info in Database Tables MrSession and MrScan
					//String file = arc + "/" + _sessionId  + "/" + _sessionId + ".xml";
					//arcSession.readSession(file);
				
					
		}catch(Exception te) {
			te.printStackTrace();						
		}
		
    }
 
    

}
