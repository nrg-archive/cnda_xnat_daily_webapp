<?xml version="1.0" encoding="UTF-8"?>

<xs:schema targetNamespace="http://nrg.wustl.edu/condr_mets" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:cnda_ext="http://nrg.wustl.edu/cnda_ext" xmlns:condr_mets="http://nrg.wustl.edu/condr_mets" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xs:import namespace="http://nrg.wustl.edu/cnda_ext" schemaLocation="../cnda_ext/cnda_ext.xsd"/>
	<xs:import namespace="http://nrg.wustl.edu/xnat" schemaLocation="../xnat/xnat.xsd"/>

	<xs:element name="LesionColl" type="condr_mets:lesionCollection" />
	<xs:element name="Lesion" type="condr_mets:lesionData" />
	
	<xs:element name="MetsRadEncColl" type="condr_mets:metsRadEncCollection" />
	<xs:element name="MetsRadEnc" type="condr_mets:metsRadEncData" />
	
	<xs:element name="MetsClinEncColl" type="condr_mets:metsClinEncCollection" />
	<xs:element name="MetsClinEnc" type="condr_mets:metsClinEncData" />

	<xs:complexType name="lesionCollection">
		<xs:annotation>
			<xs:documentation>Lesion Collection</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="cnda_ext:saCollection" />
		</xs:complexContent>
	</xs:complexType>
	
	<xs:complexType name="lesionData">
		<xs:annotation>
			<xs:documentation>Lesion Data</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="cnda_ext:saNode">
				<xs:sequence>
					<xs:element name="hemisphere" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Hemisphere</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="location" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Location</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="dateOfDiagnosis" minOccurs="0" type="xs:date">
						<xs:annotation>
							<xs:documentation>
								Date of Diagnosis
							</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="description" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Description</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="10000"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="metsRadEncCollection">
		<xs:annotation>
			<xs:documentation>Metastatic Radiosurgical Encounter Collection</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="cnda_ext:saCollection">
				<xs:sequence>
					<xs:element name="platform" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Radiosurgery Platform</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="encDate" minOccurs="0" type="xs:date">
						<xs:annotation>
							<xs:documentation>Encounter Date</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>	
		</xs:complexContent>
	</xs:complexType>
	
	<xs:complexType name="metsRadEncData">
		<xs:annotation>
			<xs:documentation>Metastatic Radiosurgical Encounter Data</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="cnda_ext:saNode">
				<xs:sequence>
					<xs:element name="treated" minOccurs="0" type="xs:boolean">
						<xs:annotation>
							<xs:documentation>Lesion Treated</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="treatmentVolume" minOccurs="0" type="xs:float">
						<xs:annotation>
							<xs:documentation>Treatment Volume</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="treatmentDose" minOccurs="0" type="xs:float">
						<xs:annotation>
							<xs:documentation>Treatment Dose</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="isodoseLine" minOccurs="0" type="xs:float">
						<xs:annotation>
							<xs:documentation>Isodose Line</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="lesionType" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Lesion Type</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="numberOfFractions" minOccurs="0" type="xs:integer">
						<xs:annotation>
							<xs:documentation>Number of Fractions</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="encNotes" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Encounter Notes</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="10000"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>

	<xs:complexType name="metsClinEncCollection">
		<xs:annotation>
			<xs:documentation>Metastatic Clinical Encounter Collection</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="cnda_ext:saCollection">
				<xs:sequence>
					<xs:element name="encDate" minOccurs="0" type="xs:date">
						<xs:annotation>
							<xs:documentation>Encounter Date</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="karPerfStatus" minOccurs="0" type="xs:integer">
						<xs:annotation>
							<xs:documentation>Karnofsky Performance Status</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="steroidDose" minOccurs="0" type="xs:float">
						<xs:annotation>
							<xs:documentation>Steroid Dose, Total mg/day</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	
	<xs:complexType name="metsClinEncData">
		<xs:annotation>
			<xs:documentation>Metastatic Clinical Encounter Data</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="cnda_ext:saNode">
				<xs:sequence>
					<xs:element name="tumorVolume" minOccurs="0" type="xs:float">
						<xs:annotation>
							<xs:documentation>Enhancing Tumor Volume (cc)</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="responseToTreatment" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Response to Treatment</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="progressionStatus" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Progression Status</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="treatmentPlan" minOccurs="0" type="xs:string">
						<xs:annotation>
							<xs:documentation>Treatement Plan</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="encNotes" minOccurs="0">
						<xs:annotation>
							<xs:documentation>Encounter Notes</xs:documentation>
						</xs:annotation>
						<xs:simpleType>
							<xs:restriction base="xs:string">
								<xs:maxLength value="10000"/>
							</xs:restriction>
						</xs:simpleType>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
</xs:schema>
