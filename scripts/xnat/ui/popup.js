/*!
 * XNAT pop-up windows
 */

var XNAT = getObject(XNAT);

(function(XNAT){

    var popup,
        window = this;

    // base popup function
    function popupCentered( /* url, title, w, h, y, params */ ) {

        // expected arguments:
        // url, title, w, h, y, params
        // ('y' is the divisor for vertical position)

        var url    = arguments[0],
            title  = arguments[1] || '',
            w      = arguments[2] || (window.innerWidth - 20),
            h      = arguments[3] || (window.innerHeight - 20),
            y      = arguments[4] || 2,
            params = arguments[5] || {},
            paramsLength=0,
            paramsArray=[];

        // the 'y' argument is optional:
        // if there are only 5 arguments,
        // the 'params' argument will be last
        if (arguments.length === 5 && !$.isNumeric(y)) {
            params = y;
            y = 2;
        }

        // pass a complete params string to explicitly use that
        // then convert the params string to a params object
        if (typeof params == 'string' && params > '') {

            paramsArray = params.replace(/\s/g,'').split(',');
            paramsLength = paramsArray.length;
            params={};

            for (var i=0, par=[]; i < paramsLength; i++){
                par = paramsArray[i].split('=');
                params[par[0]] = par[1]+'';
            }

        }

        // round to 'dec' decimal places
        function roundNumber(num, dec) {
            return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
        }

        //y = y || 2; // make sure we've got SOME value to work with

        params.width  = params.width  || w ;
        params.height = params.height || h ;
        params.left   = params.left   || roundNumber((screen.width / 2) - (w / 2), 0);
        params.top    = params.top    || roundNumber((screen.height / y) - (h / y), 0);

        paramsArray = []; // reset the array before (re)creating it

        //'scrollbars=yes, resizable=yes, toolbar=no, location=no, directories=no, status=no, copyhistory=yes';
        for (var param in params){
            if (params.hasOwnProperty(param)){
                paramsArray.push(param + '=' + params[param]);
            }
        }

        return window.top.open( url, title, paramsArray.join(',') );

    }
    window.popupCentered = popupCentered;

    popup = function(/* url, title, w, h, y, params */){
        return popupCentered.apply(null, arguments);
    };

    popup.viewXML = popup.xml = function(url, opts){
        opts = isString(opts) ? opts : 'status=yes,resizable=yes,scrollbars=yes,toolbar=no';
        return popupCentered(url, '', 960, 640, 4, opts)
    };

    XNAT.ui = getObject(XNAT.ui);
    XNAT.ui.popup = popup;

})(XNAT);