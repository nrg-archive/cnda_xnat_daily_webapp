/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:02 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function uds_d1dxData(){
this.xsiType="uds:d1dxData";

	this.getSchemaElementName=function(){
		return "d1dxData";
	}

	this.getFullSchemaElementName=function(){
		return "uds:d1dxData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Whodiddx=null;


	function getWhodiddx() {
		return this.Whodiddx;
	}
	this.getWhodiddx=getWhodiddx;


	function setWhodiddx(v){
		this.Whodiddx=v;
	}
	this.setWhodiddx=setWhodiddx;

	this.Normcog=null;


	function getNormcog() {
		return this.Normcog;
	}
	this.getNormcog=getNormcog;


	function setNormcog(v){
		this.Normcog=v;
	}
	this.setNormcog=setNormcog;

	this.Demented=null;


	function getDemented() {
		return this.Demented;
	}
	this.getDemented=getDemented;


	function setDemented(v){
		this.Demented=v;
	}
	this.setDemented=setDemented;

	this.Mciamem=null;


	function getMciamem() {
		return this.Mciamem;
	}
	this.getMciamem=getMciamem;


	function setMciamem(v){
		this.Mciamem=v;
	}
	this.setMciamem=setMciamem;

	this.Mciaplus=null;


	function getMciaplus() {
		return this.Mciaplus;
	}
	this.getMciaplus=getMciaplus;


	function setMciaplus(v){
		this.Mciaplus=v;
	}
	this.setMciaplus=setMciaplus;

	this.Mciaplan=null;


	function getMciaplan() {
		return this.Mciaplan;
	}
	this.getMciaplan=getMciaplan;


	function setMciaplan(v){
		this.Mciaplan=v;
	}
	this.setMciaplan=setMciaplan;

	this.Mciapatt=null;


	function getMciapatt() {
		return this.Mciapatt;
	}
	this.getMciapatt=getMciapatt;


	function setMciapatt(v){
		this.Mciapatt=v;
	}
	this.setMciapatt=setMciapatt;

	this.Mciapex=null;


	function getMciapex() {
		return this.Mciapex;
	}
	this.getMciapex=getMciapex;


	function setMciapex(v){
		this.Mciapex=v;
	}
	this.setMciapex=setMciapex;

	this.Mciapvis=null;


	function getMciapvis() {
		return this.Mciapvis;
	}
	this.getMciapvis=getMciapvis;


	function setMciapvis(v){
		this.Mciapvis=v;
	}
	this.setMciapvis=setMciapvis;

	this.Mcinon1=null;


	function getMcinon1() {
		return this.Mcinon1;
	}
	this.getMcinon1=getMcinon1;


	function setMcinon1(v){
		this.Mcinon1=v;
	}
	this.setMcinon1=setMcinon1;

	this.Mcin1lan=null;


	function getMcin1lan() {
		return this.Mcin1lan;
	}
	this.getMcin1lan=getMcin1lan;


	function setMcin1lan(v){
		this.Mcin1lan=v;
	}
	this.setMcin1lan=setMcin1lan;

	this.Mcin1att=null;


	function getMcin1att() {
		return this.Mcin1att;
	}
	this.getMcin1att=getMcin1att;


	function setMcin1att(v){
		this.Mcin1att=v;
	}
	this.setMcin1att=setMcin1att;

	this.Mcin1ex=null;


	function getMcin1ex() {
		return this.Mcin1ex;
	}
	this.getMcin1ex=getMcin1ex;


	function setMcin1ex(v){
		this.Mcin1ex=v;
	}
	this.setMcin1ex=setMcin1ex;

	this.Mcin1vis=null;


	function getMcin1vis() {
		return this.Mcin1vis;
	}
	this.getMcin1vis=getMcin1vis;


	function setMcin1vis(v){
		this.Mcin1vis=v;
	}
	this.setMcin1vis=setMcin1vis;

	this.Mcinon2=null;


	function getMcinon2() {
		return this.Mcinon2;
	}
	this.getMcinon2=getMcinon2;


	function setMcinon2(v){
		this.Mcinon2=v;
	}
	this.setMcinon2=setMcinon2;

	this.Mcin2lan=null;


	function getMcin2lan() {
		return this.Mcin2lan;
	}
	this.getMcin2lan=getMcin2lan;


	function setMcin2lan(v){
		this.Mcin2lan=v;
	}
	this.setMcin2lan=setMcin2lan;

	this.Mcin2att=null;


	function getMcin2att() {
		return this.Mcin2att;
	}
	this.getMcin2att=getMcin2att;


	function setMcin2att(v){
		this.Mcin2att=v;
	}
	this.setMcin2att=setMcin2att;

	this.Mcin2ex=null;


	function getMcin2ex() {
		return this.Mcin2ex;
	}
	this.getMcin2ex=getMcin2ex;


	function setMcin2ex(v){
		this.Mcin2ex=v;
	}
	this.setMcin2ex=setMcin2ex;

	this.Mcin2vis=null;


	function getMcin2vis() {
		return this.Mcin2vis;
	}
	this.getMcin2vis=getMcin2vis;


	function setMcin2vis(v){
		this.Mcin2vis=v;
	}
	this.setMcin2vis=setMcin2vis;

	this.Impnomci=null;


	function getImpnomci() {
		return this.Impnomci;
	}
	this.getImpnomci=getImpnomci;


	function setImpnomci(v){
		this.Impnomci=v;
	}
	this.setImpnomci=setImpnomci;

	this.Probad=null;


	function getProbad() {
		return this.Probad;
	}
	this.getProbad=getProbad;


	function setProbad(v){
		this.Probad=v;
	}
	this.setProbad=setProbad;

	this.Probadif=null;


	function getProbadif() {
		return this.Probadif;
	}
	this.getProbadif=getProbadif;


	function setProbadif(v){
		this.Probadif=v;
	}
	this.setProbadif=setProbadif;

	this.Possad=null;


	function getPossad() {
		return this.Possad;
	}
	this.getPossad=getPossad;


	function setPossad(v){
		this.Possad=v;
	}
	this.setPossad=setPossad;

	this.Possadif=null;


	function getPossadif() {
		return this.Possadif;
	}
	this.getPossadif=getPossadif;


	function setPossadif(v){
		this.Possadif=v;
	}
	this.setPossadif=setPossadif;

	this.Dlb=null;


	function getDlb() {
		return this.Dlb;
	}
	this.getDlb=getDlb;


	function setDlb(v){
		this.Dlb=v;
	}
	this.setDlb=setDlb;

	this.Dlbif=null;


	function getDlbif() {
		return this.Dlbif;
	}
	this.getDlbif=getDlbif;


	function setDlbif(v){
		this.Dlbif=v;
	}
	this.setDlbif=setDlbif;

	this.Vasc=null;


	function getVasc() {
		return this.Vasc;
	}
	this.getVasc=getVasc;


	function setVasc(v){
		this.Vasc=v;
	}
	this.setVasc=setVasc;

	this.Vascif=null;


	function getVascif() {
		return this.Vascif;
	}
	this.getVascif=getVascif;


	function setVascif(v){
		this.Vascif=v;
	}
	this.setVascif=setVascif;

	this.Vascps=null;


	function getVascps() {
		return this.Vascps;
	}
	this.getVascps=getVascps;


	function setVascps(v){
		this.Vascps=v;
	}
	this.setVascps=setVascps;

	this.Vascpsif=null;


	function getVascpsif() {
		return this.Vascpsif;
	}
	this.getVascpsif=getVascpsif;


	function setVascpsif(v){
		this.Vascpsif=v;
	}
	this.setVascpsif=setVascpsif;

	this.Alcdem=null;


	function getAlcdem() {
		return this.Alcdem;
	}
	this.getAlcdem=getAlcdem;


	function setAlcdem(v){
		this.Alcdem=v;
	}
	this.setAlcdem=setAlcdem;

	this.Alcdemif=null;


	function getAlcdemif() {
		return this.Alcdemif;
	}
	this.getAlcdemif=getAlcdemif;


	function setAlcdemif(v){
		this.Alcdemif=v;
	}
	this.setAlcdemif=setAlcdemif;

	this.Demun=null;


	function getDemun() {
		return this.Demun;
	}
	this.getDemun=getDemun;


	function setDemun(v){
		this.Demun=v;
	}
	this.setDemun=setDemun;

	this.Demunif=null;


	function getDemunif() {
		return this.Demunif;
	}
	this.getDemunif=getDemunif;


	function setDemunif(v){
		this.Demunif=v;
	}
	this.setDemunif=setDemunif;

	this.Ftd=null;


	function getFtd() {
		return this.Ftd;
	}
	this.getFtd=getFtd;


	function setFtd(v){
		this.Ftd=v;
	}
	this.setFtd=setFtd;

	this.Ftdif=null;


	function getFtdif() {
		return this.Ftdif;
	}
	this.getFtdif=getFtdif;


	function setFtdif(v){
		this.Ftdif=v;
	}
	this.setFtdif=setFtdif;

	this.Ppaph=null;


	function getPpaph() {
		return this.Ppaph;
	}
	this.getPpaph=getPpaph;


	function setPpaph(v){
		this.Ppaph=v;
	}
	this.setPpaph=setPpaph;

	this.Ppaphif=null;


	function getPpaphif() {
		return this.Ppaphif;
	}
	this.getPpaphif=getPpaphif;


	function setPpaphif(v){
		this.Ppaphif=v;
	}
	this.setPpaphif=setPpaphif;

	this.Pnaph=null;


	function getPnaph() {
		return this.Pnaph;
	}
	this.getPnaph=getPnaph;


	function setPnaph(v){
		this.Pnaph=v;
	}
	this.setPnaph=setPnaph;

	this.Semdeman=null;


	function getSemdeman() {
		return this.Semdeman;
	}
	this.getSemdeman=getSemdeman;


	function setSemdeman(v){
		this.Semdeman=v;
	}
	this.setSemdeman=setSemdeman;

	this.Semdemag=null;


	function getSemdemag() {
		return this.Semdemag;
	}
	this.getSemdemag=getSemdemag;


	function setSemdemag(v){
		this.Semdemag=v;
	}
	this.setSemdemag=setSemdemag;

	this.Ppaothr=null;


	function getPpaothr() {
		return this.Ppaothr;
	}
	this.getPpaothr=getPpaothr;


	function setPpaothr(v){
		this.Ppaothr=v;
	}
	this.setPpaothr=setPpaothr;

	this.Psp=null;


	function getPsp() {
		return this.Psp;
	}
	this.getPsp=getPsp;


	function setPsp(v){
		this.Psp=v;
	}
	this.setPsp=setPsp;

	this.Pspif=null;


	function getPspif() {
		return this.Pspif;
	}
	this.getPspif=getPspif;


	function setPspif(v){
		this.Pspif=v;
	}
	this.setPspif=setPspif;

	this.Cort=null;


	function getCort() {
		return this.Cort;
	}
	this.getCort=getCort;


	function setCort(v){
		this.Cort=v;
	}
	this.setCort=setCort;

	this.Cortif=null;


	function getCortif() {
		return this.Cortif;
	}
	this.getCortif=getCortif;


	function setCortif(v){
		this.Cortif=v;
	}
	this.setCortif=setCortif;

	this.Hunt=null;


	function getHunt() {
		return this.Hunt;
	}
	this.getHunt=getHunt;


	function setHunt(v){
		this.Hunt=v;
	}
	this.setHunt=setHunt;

	this.Huntif=null;


	function getHuntif() {
		return this.Huntif;
	}
	this.getHuntif=getHuntif;


	function setHuntif(v){
		this.Huntif=v;
	}
	this.setHuntif=setHuntif;

	this.Prion=null;


	function getPrion() {
		return this.Prion;
	}
	this.getPrion=getPrion;


	function setPrion(v){
		this.Prion=v;
	}
	this.setPrion=setPrion;

	this.Prionif=null;


	function getPrionif() {
		return this.Prionif;
	}
	this.getPrionif=getPrionif;


	function setPrionif(v){
		this.Prionif=v;
	}
	this.setPrionif=setPrionif;

	this.Meds=null;


	function getMeds() {
		return this.Meds;
	}
	this.getMeds=getMeds;


	function setMeds(v){
		this.Meds=v;
	}
	this.setMeds=setMeds;

	this.Medsif=null;


	function getMedsif() {
		return this.Medsif;
	}
	this.getMedsif=getMedsif;


	function setMedsif(v){
		this.Medsif=v;
	}
	this.setMedsif=setMedsif;

	this.Dysill=null;


	function getDysill() {
		return this.Dysill;
	}
	this.getDysill=getDysill;


	function setDysill(v){
		this.Dysill=v;
	}
	this.setDysill=setDysill;

	this.Dysillif=null;


	function getDysillif() {
		return this.Dysillif;
	}
	this.getDysillif=getDysillif;


	function setDysillif(v){
		this.Dysillif=v;
	}
	this.setDysillif=setDysillif;

	this.Dep=null;


	function getDep() {
		return this.Dep;
	}
	this.getDep=getDep;


	function setDep(v){
		this.Dep=v;
	}
	this.setDep=setDep;

	this.Depif=null;


	function getDepif() {
		return this.Depif;
	}
	this.getDepif=getDepif;


	function setDepif(v){
		this.Depif=v;
	}
	this.setDepif=setDepif;

	this.Othpsy=null;


	function getOthpsy() {
		return this.Othpsy;
	}
	this.getOthpsy=getOthpsy;


	function setOthpsy(v){
		this.Othpsy=v;
	}
	this.setOthpsy=setOthpsy;

	this.Othpsyif=null;


	function getOthpsyif() {
		return this.Othpsyif;
	}
	this.getOthpsyif=getOthpsyif;


	function setOthpsyif(v){
		this.Othpsyif=v;
	}
	this.setOthpsyif=setOthpsyif;

	this.Downs=null;


	function getDowns() {
		return this.Downs;
	}
	this.getDowns=getDowns;


	function setDowns(v){
		this.Downs=v;
	}
	this.setDowns=setDowns;

	this.Downsif=null;


	function getDownsif() {
		return this.Downsif;
	}
	this.getDownsif=getDownsif;


	function setDownsif(v){
		this.Downsif=v;
	}
	this.setDownsif=setDownsif;

	this.Park=null;


	function getPark() {
		return this.Park;
	}
	this.getPark=getPark;


	function setPark(v){
		this.Park=v;
	}
	this.setPark=setPark;

	this.Parkif=null;


	function getParkif() {
		return this.Parkif;
	}
	this.getParkif=getParkif;


	function setParkif(v){
		this.Parkif=v;
	}
	this.setParkif=setParkif;

	this.Stroke=null;


	function getStroke() {
		return this.Stroke;
	}
	this.getStroke=getStroke;


	function setStroke(v){
		this.Stroke=v;
	}
	this.setStroke=setStroke;

	this.Strokif=null;


	function getStrokif() {
		return this.Strokif;
	}
	this.getStrokif=getStrokif;


	function setStrokif(v){
		this.Strokif=v;
	}
	this.setStrokif=setStrokif;

	this.Hyceph=null;


	function getHyceph() {
		return this.Hyceph;
	}
	this.getHyceph=getHyceph;


	function setHyceph(v){
		this.Hyceph=v;
	}
	this.setHyceph=setHyceph;

	this.Hycephif=null;


	function getHycephif() {
		return this.Hycephif;
	}
	this.getHycephif=getHycephif;


	function setHycephif(v){
		this.Hycephif=v;
	}
	this.setHycephif=setHycephif;

	this.Brninj=null;


	function getBrninj() {
		return this.Brninj;
	}
	this.getBrninj=getBrninj;


	function setBrninj(v){
		this.Brninj=v;
	}
	this.setBrninj=setBrninj;

	this.Brninjif=null;


	function getBrninjif() {
		return this.Brninjif;
	}
	this.getBrninjif=getBrninjif;


	function setBrninjif(v){
		this.Brninjif=v;
	}
	this.setBrninjif=setBrninjif;

	this.Neop=null;


	function getNeop() {
		return this.Neop;
	}
	this.getNeop=getNeop;


	function setNeop(v){
		this.Neop=v;
	}
	this.setNeop=setNeop;

	this.Neopif=null;


	function getNeopif() {
		return this.Neopif;
	}
	this.getNeopif=getNeopif;


	function setNeopif(v){
		this.Neopif=v;
	}
	this.setNeopif=setNeopif;

	this.Cogoth=null;


	function getCogoth() {
		return this.Cogoth;
	}
	this.getCogoth=getCogoth;


	function setCogoth(v){
		this.Cogoth=v;
	}
	this.setCogoth=setCogoth;

	this.Cogothx=null;


	function getCogothx() {
		return this.Cogothx;
	}
	this.getCogothx=getCogothx;


	function setCogothx(v){
		this.Cogothx=v;
	}
	this.setCogothx=setCogothx;

	this.Cogothif=null;


	function getCogothif() {
		return this.Cogothif;
	}
	this.getCogothif=getCogothif;


	function setCogothif(v){
		this.Cogothif=v;
	}
	this.setCogothif=setCogothif;

	this.Cogoth2=null;


	function getCogoth2() {
		return this.Cogoth2;
	}
	this.getCogoth2=getCogoth2;


	function setCogoth2(v){
		this.Cogoth2=v;
	}
	this.setCogoth2=setCogoth2;

	this.Cogoth2x=null;


	function getCogoth2x() {
		return this.Cogoth2x;
	}
	this.getCogoth2x=getCogoth2x;


	function setCogoth2x(v){
		this.Cogoth2x=v;
	}
	this.setCogoth2x=setCogoth2x;

	this.Cogoth2f=null;


	function getCogoth2f() {
		return this.Cogoth2f;
	}
	this.getCogoth2f=getCogoth2f;


	function setCogoth2f(v){
		this.Cogoth2f=v;
	}
	this.setCogoth2f=setCogoth2f;

	this.Cogoth3=null;


	function getCogoth3() {
		return this.Cogoth3;
	}
	this.getCogoth3=getCogoth3;


	function setCogoth3(v){
		this.Cogoth3=v;
	}
	this.setCogoth3=setCogoth3;

	this.Cogoth3x=null;


	function getCogoth3x() {
		return this.Cogoth3x;
	}
	this.getCogoth3x=getCogoth3x;


	function setCogoth3x(v){
		this.Cogoth3x=v;
	}
	this.setCogoth3x=setCogoth3x;

	this.Cogoth3f=null;


	function getCogoth3f() {
		return this.Cogoth3f;
	}
	this.getCogoth3f=getCogoth3f;


	function setCogoth3f(v){
		this.Cogoth3f=v;
	}
	this.setCogoth3f=setCogoth3f;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="WHODIDDX"){
				return this.Whodiddx ;
			} else 
			if(xmlPath=="NORMCOG"){
				return this.Normcog ;
			} else 
			if(xmlPath=="DEMENTED"){
				return this.Demented ;
			} else 
			if(xmlPath=="MCIAMEM"){
				return this.Mciamem ;
			} else 
			if(xmlPath=="MCIAPLUS"){
				return this.Mciaplus ;
			} else 
			if(xmlPath=="MCIAPLAN"){
				return this.Mciaplan ;
			} else 
			if(xmlPath=="MCIAPATT"){
				return this.Mciapatt ;
			} else 
			if(xmlPath=="MCIAPEX"){
				return this.Mciapex ;
			} else 
			if(xmlPath=="MCIAPVIS"){
				return this.Mciapvis ;
			} else 
			if(xmlPath=="MCINON1"){
				return this.Mcinon1 ;
			} else 
			if(xmlPath=="MCIN1LAN"){
				return this.Mcin1lan ;
			} else 
			if(xmlPath=="MCIN1ATT"){
				return this.Mcin1att ;
			} else 
			if(xmlPath=="MCIN1EX"){
				return this.Mcin1ex ;
			} else 
			if(xmlPath=="MCIN1VIS"){
				return this.Mcin1vis ;
			} else 
			if(xmlPath=="MCINON2"){
				return this.Mcinon2 ;
			} else 
			if(xmlPath=="MCIN2LAN"){
				return this.Mcin2lan ;
			} else 
			if(xmlPath=="MCIN2ATT"){
				return this.Mcin2att ;
			} else 
			if(xmlPath=="MCIN2EX"){
				return this.Mcin2ex ;
			} else 
			if(xmlPath=="MCIN2VIS"){
				return this.Mcin2vis ;
			} else 
			if(xmlPath=="IMPNOMCI"){
				return this.Impnomci ;
			} else 
			if(xmlPath=="PROBAD"){
				return this.Probad ;
			} else 
			if(xmlPath=="PROBADIF"){
				return this.Probadif ;
			} else 
			if(xmlPath=="POSSAD"){
				return this.Possad ;
			} else 
			if(xmlPath=="POSSADIF"){
				return this.Possadif ;
			} else 
			if(xmlPath=="DLB"){
				return this.Dlb ;
			} else 
			if(xmlPath=="DLBIF"){
				return this.Dlbif ;
			} else 
			if(xmlPath=="VASC"){
				return this.Vasc ;
			} else 
			if(xmlPath=="VASCIF"){
				return this.Vascif ;
			} else 
			if(xmlPath=="VASCPS"){
				return this.Vascps ;
			} else 
			if(xmlPath=="VASCPSIF"){
				return this.Vascpsif ;
			} else 
			if(xmlPath=="ALCDEM"){
				return this.Alcdem ;
			} else 
			if(xmlPath=="ALCDEMIF"){
				return this.Alcdemif ;
			} else 
			if(xmlPath=="DEMUN"){
				return this.Demun ;
			} else 
			if(xmlPath=="DEMUNIF"){
				return this.Demunif ;
			} else 
			if(xmlPath=="FTD"){
				return this.Ftd ;
			} else 
			if(xmlPath=="FTDIF"){
				return this.Ftdif ;
			} else 
			if(xmlPath=="PPAPH"){
				return this.Ppaph ;
			} else 
			if(xmlPath=="PPAPHIF"){
				return this.Ppaphif ;
			} else 
			if(xmlPath=="PNAPH"){
				return this.Pnaph ;
			} else 
			if(xmlPath=="SEMDEMAN"){
				return this.Semdeman ;
			} else 
			if(xmlPath=="SEMDEMAG"){
				return this.Semdemag ;
			} else 
			if(xmlPath=="PPAOTHR"){
				return this.Ppaothr ;
			} else 
			if(xmlPath=="PSP"){
				return this.Psp ;
			} else 
			if(xmlPath=="PSPIF"){
				return this.Pspif ;
			} else 
			if(xmlPath=="CORT"){
				return this.Cort ;
			} else 
			if(xmlPath=="CORTIF"){
				return this.Cortif ;
			} else 
			if(xmlPath=="HUNT"){
				return this.Hunt ;
			} else 
			if(xmlPath=="HUNTIF"){
				return this.Huntif ;
			} else 
			if(xmlPath=="PRION"){
				return this.Prion ;
			} else 
			if(xmlPath=="PRIONIF"){
				return this.Prionif ;
			} else 
			if(xmlPath=="MEDS"){
				return this.Meds ;
			} else 
			if(xmlPath=="MEDSIF"){
				return this.Medsif ;
			} else 
			if(xmlPath=="DYSILL"){
				return this.Dysill ;
			} else 
			if(xmlPath=="DYSILLIF"){
				return this.Dysillif ;
			} else 
			if(xmlPath=="DEP"){
				return this.Dep ;
			} else 
			if(xmlPath=="DEPIF"){
				return this.Depif ;
			} else 
			if(xmlPath=="OTHPSY"){
				return this.Othpsy ;
			} else 
			if(xmlPath=="OTHPSYIF"){
				return this.Othpsyif ;
			} else 
			if(xmlPath=="DOWNS"){
				return this.Downs ;
			} else 
			if(xmlPath=="DOWNSIF"){
				return this.Downsif ;
			} else 
			if(xmlPath=="PARK"){
				return this.Park ;
			} else 
			if(xmlPath=="PARKIF"){
				return this.Parkif ;
			} else 
			if(xmlPath=="STROKE"){
				return this.Stroke ;
			} else 
			if(xmlPath=="STROKIF"){
				return this.Strokif ;
			} else 
			if(xmlPath=="HYCEPH"){
				return this.Hyceph ;
			} else 
			if(xmlPath=="HYCEPHIF"){
				return this.Hycephif ;
			} else 
			if(xmlPath=="BRNINJ"){
				return this.Brninj ;
			} else 
			if(xmlPath=="BRNINJIF"){
				return this.Brninjif ;
			} else 
			if(xmlPath=="NEOP"){
				return this.Neop ;
			} else 
			if(xmlPath=="NEOPIF"){
				return this.Neopif ;
			} else 
			if(xmlPath=="COGOTH"){
				return this.Cogoth ;
			} else 
			if(xmlPath=="COGOTHX"){
				return this.Cogothx ;
			} else 
			if(xmlPath=="COGOTHIF"){
				return this.Cogothif ;
			} else 
			if(xmlPath=="COGOTH2"){
				return this.Cogoth2 ;
			} else 
			if(xmlPath=="COGOTH2X"){
				return this.Cogoth2x ;
			} else 
			if(xmlPath=="COGOTH2F"){
				return this.Cogoth2f ;
			} else 
			if(xmlPath=="COGOTH3"){
				return this.Cogoth3 ;
			} else 
			if(xmlPath=="COGOTH3X"){
				return this.Cogoth3x ;
			} else 
			if(xmlPath=="COGOTH3F"){
				return this.Cogoth3f ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="WHODIDDX"){
				this.Whodiddx=value;
			} else 
			if(xmlPath=="NORMCOG"){
				this.Normcog=value;
			} else 
			if(xmlPath=="DEMENTED"){
				this.Demented=value;
			} else 
			if(xmlPath=="MCIAMEM"){
				this.Mciamem=value;
			} else 
			if(xmlPath=="MCIAPLUS"){
				this.Mciaplus=value;
			} else 
			if(xmlPath=="MCIAPLAN"){
				this.Mciaplan=value;
			} else 
			if(xmlPath=="MCIAPATT"){
				this.Mciapatt=value;
			} else 
			if(xmlPath=="MCIAPEX"){
				this.Mciapex=value;
			} else 
			if(xmlPath=="MCIAPVIS"){
				this.Mciapvis=value;
			} else 
			if(xmlPath=="MCINON1"){
				this.Mcinon1=value;
			} else 
			if(xmlPath=="MCIN1LAN"){
				this.Mcin1lan=value;
			} else 
			if(xmlPath=="MCIN1ATT"){
				this.Mcin1att=value;
			} else 
			if(xmlPath=="MCIN1EX"){
				this.Mcin1ex=value;
			} else 
			if(xmlPath=="MCIN1VIS"){
				this.Mcin1vis=value;
			} else 
			if(xmlPath=="MCINON2"){
				this.Mcinon2=value;
			} else 
			if(xmlPath=="MCIN2LAN"){
				this.Mcin2lan=value;
			} else 
			if(xmlPath=="MCIN2ATT"){
				this.Mcin2att=value;
			} else 
			if(xmlPath=="MCIN2EX"){
				this.Mcin2ex=value;
			} else 
			if(xmlPath=="MCIN2VIS"){
				this.Mcin2vis=value;
			} else 
			if(xmlPath=="IMPNOMCI"){
				this.Impnomci=value;
			} else 
			if(xmlPath=="PROBAD"){
				this.Probad=value;
			} else 
			if(xmlPath=="PROBADIF"){
				this.Probadif=value;
			} else 
			if(xmlPath=="POSSAD"){
				this.Possad=value;
			} else 
			if(xmlPath=="POSSADIF"){
				this.Possadif=value;
			} else 
			if(xmlPath=="DLB"){
				this.Dlb=value;
			} else 
			if(xmlPath=="DLBIF"){
				this.Dlbif=value;
			} else 
			if(xmlPath=="VASC"){
				this.Vasc=value;
			} else 
			if(xmlPath=="VASCIF"){
				this.Vascif=value;
			} else 
			if(xmlPath=="VASCPS"){
				this.Vascps=value;
			} else 
			if(xmlPath=="VASCPSIF"){
				this.Vascpsif=value;
			} else 
			if(xmlPath=="ALCDEM"){
				this.Alcdem=value;
			} else 
			if(xmlPath=="ALCDEMIF"){
				this.Alcdemif=value;
			} else 
			if(xmlPath=="DEMUN"){
				this.Demun=value;
			} else 
			if(xmlPath=="DEMUNIF"){
				this.Demunif=value;
			} else 
			if(xmlPath=="FTD"){
				this.Ftd=value;
			} else 
			if(xmlPath=="FTDIF"){
				this.Ftdif=value;
			} else 
			if(xmlPath=="PPAPH"){
				this.Ppaph=value;
			} else 
			if(xmlPath=="PPAPHIF"){
				this.Ppaphif=value;
			} else 
			if(xmlPath=="PNAPH"){
				this.Pnaph=value;
			} else 
			if(xmlPath=="SEMDEMAN"){
				this.Semdeman=value;
			} else 
			if(xmlPath=="SEMDEMAG"){
				this.Semdemag=value;
			} else 
			if(xmlPath=="PPAOTHR"){
				this.Ppaothr=value;
			} else 
			if(xmlPath=="PSP"){
				this.Psp=value;
			} else 
			if(xmlPath=="PSPIF"){
				this.Pspif=value;
			} else 
			if(xmlPath=="CORT"){
				this.Cort=value;
			} else 
			if(xmlPath=="CORTIF"){
				this.Cortif=value;
			} else 
			if(xmlPath=="HUNT"){
				this.Hunt=value;
			} else 
			if(xmlPath=="HUNTIF"){
				this.Huntif=value;
			} else 
			if(xmlPath=="PRION"){
				this.Prion=value;
			} else 
			if(xmlPath=="PRIONIF"){
				this.Prionif=value;
			} else 
			if(xmlPath=="MEDS"){
				this.Meds=value;
			} else 
			if(xmlPath=="MEDSIF"){
				this.Medsif=value;
			} else 
			if(xmlPath=="DYSILL"){
				this.Dysill=value;
			} else 
			if(xmlPath=="DYSILLIF"){
				this.Dysillif=value;
			} else 
			if(xmlPath=="DEP"){
				this.Dep=value;
			} else 
			if(xmlPath=="DEPIF"){
				this.Depif=value;
			} else 
			if(xmlPath=="OTHPSY"){
				this.Othpsy=value;
			} else 
			if(xmlPath=="OTHPSYIF"){
				this.Othpsyif=value;
			} else 
			if(xmlPath=="DOWNS"){
				this.Downs=value;
			} else 
			if(xmlPath=="DOWNSIF"){
				this.Downsif=value;
			} else 
			if(xmlPath=="PARK"){
				this.Park=value;
			} else 
			if(xmlPath=="PARKIF"){
				this.Parkif=value;
			} else 
			if(xmlPath=="STROKE"){
				this.Stroke=value;
			} else 
			if(xmlPath=="STROKIF"){
				this.Strokif=value;
			} else 
			if(xmlPath=="HYCEPH"){
				this.Hyceph=value;
			} else 
			if(xmlPath=="HYCEPHIF"){
				this.Hycephif=value;
			} else 
			if(xmlPath=="BRNINJ"){
				this.Brninj=value;
			} else 
			if(xmlPath=="BRNINJIF"){
				this.Brninjif=value;
			} else 
			if(xmlPath=="NEOP"){
				this.Neop=value;
			} else 
			if(xmlPath=="NEOPIF"){
				this.Neopif=value;
			} else 
			if(xmlPath=="COGOTH"){
				this.Cogoth=value;
			} else 
			if(xmlPath=="COGOTHX"){
				this.Cogothx=value;
			} else 
			if(xmlPath=="COGOTHIF"){
				this.Cogothif=value;
			} else 
			if(xmlPath=="COGOTH2"){
				this.Cogoth2=value;
			} else 
			if(xmlPath=="COGOTH2X"){
				this.Cogoth2x=value;
			} else 
			if(xmlPath=="COGOTH2F"){
				this.Cogoth2f=value;
			} else 
			if(xmlPath=="COGOTH3"){
				this.Cogoth3=value;
			} else 
			if(xmlPath=="COGOTH3X"){
				this.Cogoth3x=value;
			} else 
			if(xmlPath=="COGOTH3F"){
				this.Cogoth3f=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="WHODIDDX"){
			return "field_data";
		}else if (xmlPath=="NORMCOG"){
			return "field_data";
		}else if (xmlPath=="DEMENTED"){
			return "field_data";
		}else if (xmlPath=="MCIAMEM"){
			return "field_data";
		}else if (xmlPath=="MCIAPLUS"){
			return "field_data";
		}else if (xmlPath=="MCIAPLAN"){
			return "field_data";
		}else if (xmlPath=="MCIAPATT"){
			return "field_data";
		}else if (xmlPath=="MCIAPEX"){
			return "field_data";
		}else if (xmlPath=="MCIAPVIS"){
			return "field_data";
		}else if (xmlPath=="MCINON1"){
			return "field_data";
		}else if (xmlPath=="MCIN1LAN"){
			return "field_data";
		}else if (xmlPath=="MCIN1ATT"){
			return "field_data";
		}else if (xmlPath=="MCIN1EX"){
			return "field_data";
		}else if (xmlPath=="MCIN1VIS"){
			return "field_data";
		}else if (xmlPath=="MCINON2"){
			return "field_data";
		}else if (xmlPath=="MCIN2LAN"){
			return "field_data";
		}else if (xmlPath=="MCIN2ATT"){
			return "field_data";
		}else if (xmlPath=="MCIN2EX"){
			return "field_data";
		}else if (xmlPath=="MCIN2VIS"){
			return "field_data";
		}else if (xmlPath=="IMPNOMCI"){
			return "field_data";
		}else if (xmlPath=="PROBAD"){
			return "field_data";
		}else if (xmlPath=="PROBADIF"){
			return "field_data";
		}else if (xmlPath=="POSSAD"){
			return "field_data";
		}else if (xmlPath=="POSSADIF"){
			return "field_data";
		}else if (xmlPath=="DLB"){
			return "field_data";
		}else if (xmlPath=="DLBIF"){
			return "field_data";
		}else if (xmlPath=="VASC"){
			return "field_data";
		}else if (xmlPath=="VASCIF"){
			return "field_data";
		}else if (xmlPath=="VASCPS"){
			return "field_data";
		}else if (xmlPath=="VASCPSIF"){
			return "field_data";
		}else if (xmlPath=="ALCDEM"){
			return "field_data";
		}else if (xmlPath=="ALCDEMIF"){
			return "field_data";
		}else if (xmlPath=="DEMUN"){
			return "field_data";
		}else if (xmlPath=="DEMUNIF"){
			return "field_data";
		}else if (xmlPath=="FTD"){
			return "field_data";
		}else if (xmlPath=="FTDIF"){
			return "field_data";
		}else if (xmlPath=="PPAPH"){
			return "field_data";
		}else if (xmlPath=="PPAPHIF"){
			return "field_data";
		}else if (xmlPath=="PNAPH"){
			return "field_data";
		}else if (xmlPath=="SEMDEMAN"){
			return "field_data";
		}else if (xmlPath=="SEMDEMAG"){
			return "field_data";
		}else if (xmlPath=="PPAOTHR"){
			return "field_data";
		}else if (xmlPath=="PSP"){
			return "field_data";
		}else if (xmlPath=="PSPIF"){
			return "field_data";
		}else if (xmlPath=="CORT"){
			return "field_data";
		}else if (xmlPath=="CORTIF"){
			return "field_data";
		}else if (xmlPath=="HUNT"){
			return "field_data";
		}else if (xmlPath=="HUNTIF"){
			return "field_data";
		}else if (xmlPath=="PRION"){
			return "field_data";
		}else if (xmlPath=="PRIONIF"){
			return "field_data";
		}else if (xmlPath=="MEDS"){
			return "field_data";
		}else if (xmlPath=="MEDSIF"){
			return "field_data";
		}else if (xmlPath=="DYSILL"){
			return "field_data";
		}else if (xmlPath=="DYSILLIF"){
			return "field_data";
		}else if (xmlPath=="DEP"){
			return "field_data";
		}else if (xmlPath=="DEPIF"){
			return "field_data";
		}else if (xmlPath=="OTHPSY"){
			return "field_data";
		}else if (xmlPath=="OTHPSYIF"){
			return "field_data";
		}else if (xmlPath=="DOWNS"){
			return "field_data";
		}else if (xmlPath=="DOWNSIF"){
			return "field_data";
		}else if (xmlPath=="PARK"){
			return "field_data";
		}else if (xmlPath=="PARKIF"){
			return "field_data";
		}else if (xmlPath=="STROKE"){
			return "field_data";
		}else if (xmlPath=="STROKIF"){
			return "field_data";
		}else if (xmlPath=="HYCEPH"){
			return "field_data";
		}else if (xmlPath=="HYCEPHIF"){
			return "field_data";
		}else if (xmlPath=="BRNINJ"){
			return "field_data";
		}else if (xmlPath=="BRNINJIF"){
			return "field_data";
		}else if (xmlPath=="NEOP"){
			return "field_data";
		}else if (xmlPath=="NEOPIF"){
			return "field_data";
		}else if (xmlPath=="COGOTH"){
			return "field_data";
		}else if (xmlPath=="COGOTHX"){
			return "field_data";
		}else if (xmlPath=="COGOTHIF"){
			return "field_data";
		}else if (xmlPath=="COGOTH2"){
			return "field_data";
		}else if (xmlPath=="COGOTH2X"){
			return "field_data";
		}else if (xmlPath=="COGOTH2F"){
			return "field_data";
		}else if (xmlPath=="COGOTH3"){
			return "field_data";
		}else if (xmlPath=="COGOTH3X"){
			return "field_data";
		}else if (xmlPath=="COGOTH3F"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<uds:D1DX";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</uds:D1DX>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Whodiddx!=null){
			xmlTxt+="\n<uds:WHODIDDX";
			xmlTxt+=">";
			xmlTxt+=this.Whodiddx;
			xmlTxt+="</uds:WHODIDDX>";
		}
		if (this.Normcog!=null){
			xmlTxt+="\n<uds:NORMCOG";
			xmlTxt+=">";
			xmlTxt+=this.Normcog;
			xmlTxt+="</uds:NORMCOG>";
		}
		if (this.Demented!=null){
			xmlTxt+="\n<uds:DEMENTED";
			xmlTxt+=">";
			xmlTxt+=this.Demented;
			xmlTxt+="</uds:DEMENTED>";
		}
		if (this.Mciamem!=null){
			xmlTxt+="\n<uds:MCIAMEM";
			xmlTxt+=">";
			xmlTxt+=this.Mciamem;
			xmlTxt+="</uds:MCIAMEM>";
		}
		if (this.Mciaplus!=null){
			xmlTxt+="\n<uds:MCIAPLUS";
			xmlTxt+=">";
			xmlTxt+=this.Mciaplus;
			xmlTxt+="</uds:MCIAPLUS>";
		}
		if (this.Mciaplan!=null){
			xmlTxt+="\n<uds:MCIAPLAN";
			xmlTxt+=">";
			xmlTxt+=this.Mciaplan;
			xmlTxt+="</uds:MCIAPLAN>";
		}
		if (this.Mciapatt!=null){
			xmlTxt+="\n<uds:MCIAPATT";
			xmlTxt+=">";
			xmlTxt+=this.Mciapatt;
			xmlTxt+="</uds:MCIAPATT>";
		}
		if (this.Mciapex!=null){
			xmlTxt+="\n<uds:MCIAPEX";
			xmlTxt+=">";
			xmlTxt+=this.Mciapex;
			xmlTxt+="</uds:MCIAPEX>";
		}
		if (this.Mciapvis!=null){
			xmlTxt+="\n<uds:MCIAPVIS";
			xmlTxt+=">";
			xmlTxt+=this.Mciapvis;
			xmlTxt+="</uds:MCIAPVIS>";
		}
		if (this.Mcinon1!=null){
			xmlTxt+="\n<uds:MCINON1";
			xmlTxt+=">";
			xmlTxt+=this.Mcinon1;
			xmlTxt+="</uds:MCINON1>";
		}
		if (this.Mcin1lan!=null){
			xmlTxt+="\n<uds:MCIN1LAN";
			xmlTxt+=">";
			xmlTxt+=this.Mcin1lan;
			xmlTxt+="</uds:MCIN1LAN>";
		}
		if (this.Mcin1att!=null){
			xmlTxt+="\n<uds:MCIN1ATT";
			xmlTxt+=">";
			xmlTxt+=this.Mcin1att;
			xmlTxt+="</uds:MCIN1ATT>";
		}
		if (this.Mcin1ex!=null){
			xmlTxt+="\n<uds:MCIN1EX";
			xmlTxt+=">";
			xmlTxt+=this.Mcin1ex;
			xmlTxt+="</uds:MCIN1EX>";
		}
		if (this.Mcin1vis!=null){
			xmlTxt+="\n<uds:MCIN1VIS";
			xmlTxt+=">";
			xmlTxt+=this.Mcin1vis;
			xmlTxt+="</uds:MCIN1VIS>";
		}
		if (this.Mcinon2!=null){
			xmlTxt+="\n<uds:MCINON2";
			xmlTxt+=">";
			xmlTxt+=this.Mcinon2;
			xmlTxt+="</uds:MCINON2>";
		}
		if (this.Mcin2lan!=null){
			xmlTxt+="\n<uds:MCIN2LAN";
			xmlTxt+=">";
			xmlTxt+=this.Mcin2lan;
			xmlTxt+="</uds:MCIN2LAN>";
		}
		if (this.Mcin2att!=null){
			xmlTxt+="\n<uds:MCIN2ATT";
			xmlTxt+=">";
			xmlTxt+=this.Mcin2att;
			xmlTxt+="</uds:MCIN2ATT>";
		}
		if (this.Mcin2ex!=null){
			xmlTxt+="\n<uds:MCIN2EX";
			xmlTxt+=">";
			xmlTxt+=this.Mcin2ex;
			xmlTxt+="</uds:MCIN2EX>";
		}
		if (this.Mcin2vis!=null){
			xmlTxt+="\n<uds:MCIN2VIS";
			xmlTxt+=">";
			xmlTxt+=this.Mcin2vis;
			xmlTxt+="</uds:MCIN2VIS>";
		}
		if (this.Impnomci!=null){
			xmlTxt+="\n<uds:IMPNOMCI";
			xmlTxt+=">";
			xmlTxt+=this.Impnomci;
			xmlTxt+="</uds:IMPNOMCI>";
		}
		if (this.Probad!=null){
			xmlTxt+="\n<uds:PROBAD";
			xmlTxt+=">";
			xmlTxt+=this.Probad;
			xmlTxt+="</uds:PROBAD>";
		}
		if (this.Probadif!=null){
			xmlTxt+="\n<uds:PROBADIF";
			xmlTxt+=">";
			xmlTxt+=this.Probadif;
			xmlTxt+="</uds:PROBADIF>";
		}
		if (this.Possad!=null){
			xmlTxt+="\n<uds:POSSAD";
			xmlTxt+=">";
			xmlTxt+=this.Possad;
			xmlTxt+="</uds:POSSAD>";
		}
		if (this.Possadif!=null){
			xmlTxt+="\n<uds:POSSADIF";
			xmlTxt+=">";
			xmlTxt+=this.Possadif;
			xmlTxt+="</uds:POSSADIF>";
		}
		if (this.Dlb!=null){
			xmlTxt+="\n<uds:DLB";
			xmlTxt+=">";
			xmlTxt+=this.Dlb;
			xmlTxt+="</uds:DLB>";
		}
		if (this.Dlbif!=null){
			xmlTxt+="\n<uds:DLBIF";
			xmlTxt+=">";
			xmlTxt+=this.Dlbif;
			xmlTxt+="</uds:DLBIF>";
		}
		if (this.Vasc!=null){
			xmlTxt+="\n<uds:VASC";
			xmlTxt+=">";
			xmlTxt+=this.Vasc;
			xmlTxt+="</uds:VASC>";
		}
		if (this.Vascif!=null){
			xmlTxt+="\n<uds:VASCIF";
			xmlTxt+=">";
			xmlTxt+=this.Vascif;
			xmlTxt+="</uds:VASCIF>";
		}
		if (this.Vascps!=null){
			xmlTxt+="\n<uds:VASCPS";
			xmlTxt+=">";
			xmlTxt+=this.Vascps;
			xmlTxt+="</uds:VASCPS>";
		}
		if (this.Vascpsif!=null){
			xmlTxt+="\n<uds:VASCPSIF";
			xmlTxt+=">";
			xmlTxt+=this.Vascpsif;
			xmlTxt+="</uds:VASCPSIF>";
		}
		if (this.Alcdem!=null){
			xmlTxt+="\n<uds:ALCDEM";
			xmlTxt+=">";
			xmlTxt+=this.Alcdem;
			xmlTxt+="</uds:ALCDEM>";
		}
		if (this.Alcdemif!=null){
			xmlTxt+="\n<uds:ALCDEMIF";
			xmlTxt+=">";
			xmlTxt+=this.Alcdemif;
			xmlTxt+="</uds:ALCDEMIF>";
		}
		if (this.Demun!=null){
			xmlTxt+="\n<uds:DEMUN";
			xmlTxt+=">";
			xmlTxt+=this.Demun;
			xmlTxt+="</uds:DEMUN>";
		}
		if (this.Demunif!=null){
			xmlTxt+="\n<uds:DEMUNIF";
			xmlTxt+=">";
			xmlTxt+=this.Demunif;
			xmlTxt+="</uds:DEMUNIF>";
		}
		if (this.Ftd!=null){
			xmlTxt+="\n<uds:FTD";
			xmlTxt+=">";
			xmlTxt+=this.Ftd;
			xmlTxt+="</uds:FTD>";
		}
		if (this.Ftdif!=null){
			xmlTxt+="\n<uds:FTDIF";
			xmlTxt+=">";
			xmlTxt+=this.Ftdif;
			xmlTxt+="</uds:FTDIF>";
		}
		if (this.Ppaph!=null){
			xmlTxt+="\n<uds:PPAPH";
			xmlTxt+=">";
			xmlTxt+=this.Ppaph;
			xmlTxt+="</uds:PPAPH>";
		}
		if (this.Ppaphif!=null){
			xmlTxt+="\n<uds:PPAPHIF";
			xmlTxt+=">";
			xmlTxt+=this.Ppaphif;
			xmlTxt+="</uds:PPAPHIF>";
		}
		if (this.Pnaph!=null){
			xmlTxt+="\n<uds:PNAPH";
			xmlTxt+=">";
			xmlTxt+=this.Pnaph;
			xmlTxt+="</uds:PNAPH>";
		}
		if (this.Semdeman!=null){
			xmlTxt+="\n<uds:SEMDEMAN";
			xmlTxt+=">";
			xmlTxt+=this.Semdeman;
			xmlTxt+="</uds:SEMDEMAN>";
		}
		if (this.Semdemag!=null){
			xmlTxt+="\n<uds:SEMDEMAG";
			xmlTxt+=">";
			xmlTxt+=this.Semdemag;
			xmlTxt+="</uds:SEMDEMAG>";
		}
		if (this.Ppaothr!=null){
			xmlTxt+="\n<uds:PPAOTHR";
			xmlTxt+=">";
			xmlTxt+=this.Ppaothr;
			xmlTxt+="</uds:PPAOTHR>";
		}
		if (this.Psp!=null){
			xmlTxt+="\n<uds:PSP";
			xmlTxt+=">";
			xmlTxt+=this.Psp;
			xmlTxt+="</uds:PSP>";
		}
		if (this.Pspif!=null){
			xmlTxt+="\n<uds:PSPIF";
			xmlTxt+=">";
			xmlTxt+=this.Pspif;
			xmlTxt+="</uds:PSPIF>";
		}
		if (this.Cort!=null){
			xmlTxt+="\n<uds:CORT";
			xmlTxt+=">";
			xmlTxt+=this.Cort;
			xmlTxt+="</uds:CORT>";
		}
		if (this.Cortif!=null){
			xmlTxt+="\n<uds:CORTIF";
			xmlTxt+=">";
			xmlTxt+=this.Cortif;
			xmlTxt+="</uds:CORTIF>";
		}
		if (this.Hunt!=null){
			xmlTxt+="\n<uds:HUNT";
			xmlTxt+=">";
			xmlTxt+=this.Hunt;
			xmlTxt+="</uds:HUNT>";
		}
		if (this.Huntif!=null){
			xmlTxt+="\n<uds:HUNTIF";
			xmlTxt+=">";
			xmlTxt+=this.Huntif;
			xmlTxt+="</uds:HUNTIF>";
		}
		if (this.Prion!=null){
			xmlTxt+="\n<uds:PRION";
			xmlTxt+=">";
			xmlTxt+=this.Prion;
			xmlTxt+="</uds:PRION>";
		}
		if (this.Prionif!=null){
			xmlTxt+="\n<uds:PRIONIF";
			xmlTxt+=">";
			xmlTxt+=this.Prionif;
			xmlTxt+="</uds:PRIONIF>";
		}
		if (this.Meds!=null){
			xmlTxt+="\n<uds:MEDS";
			xmlTxt+=">";
			xmlTxt+=this.Meds;
			xmlTxt+="</uds:MEDS>";
		}
		if (this.Medsif!=null){
			xmlTxt+="\n<uds:MEDSIF";
			xmlTxt+=">";
			xmlTxt+=this.Medsif;
			xmlTxt+="</uds:MEDSIF>";
		}
		if (this.Dysill!=null){
			xmlTxt+="\n<uds:DYSILL";
			xmlTxt+=">";
			xmlTxt+=this.Dysill;
			xmlTxt+="</uds:DYSILL>";
		}
		if (this.Dysillif!=null){
			xmlTxt+="\n<uds:DYSILLIF";
			xmlTxt+=">";
			xmlTxt+=this.Dysillif;
			xmlTxt+="</uds:DYSILLIF>";
		}
		if (this.Dep!=null){
			xmlTxt+="\n<uds:DEP";
			xmlTxt+=">";
			xmlTxt+=this.Dep;
			xmlTxt+="</uds:DEP>";
		}
		if (this.Depif!=null){
			xmlTxt+="\n<uds:DEPIF";
			xmlTxt+=">";
			xmlTxt+=this.Depif;
			xmlTxt+="</uds:DEPIF>";
		}
		if (this.Othpsy!=null){
			xmlTxt+="\n<uds:OTHPSY";
			xmlTxt+=">";
			xmlTxt+=this.Othpsy;
			xmlTxt+="</uds:OTHPSY>";
		}
		if (this.Othpsyif!=null){
			xmlTxt+="\n<uds:OTHPSYIF";
			xmlTxt+=">";
			xmlTxt+=this.Othpsyif;
			xmlTxt+="</uds:OTHPSYIF>";
		}
		if (this.Downs!=null){
			xmlTxt+="\n<uds:DOWNS";
			xmlTxt+=">";
			xmlTxt+=this.Downs;
			xmlTxt+="</uds:DOWNS>";
		}
		if (this.Downsif!=null){
			xmlTxt+="\n<uds:DOWNSIF";
			xmlTxt+=">";
			xmlTxt+=this.Downsif;
			xmlTxt+="</uds:DOWNSIF>";
		}
		if (this.Park!=null){
			xmlTxt+="\n<uds:PARK";
			xmlTxt+=">";
			xmlTxt+=this.Park;
			xmlTxt+="</uds:PARK>";
		}
		if (this.Parkif!=null){
			xmlTxt+="\n<uds:PARKIF";
			xmlTxt+=">";
			xmlTxt+=this.Parkif;
			xmlTxt+="</uds:PARKIF>";
		}
		if (this.Stroke!=null){
			xmlTxt+="\n<uds:STROKE";
			xmlTxt+=">";
			xmlTxt+=this.Stroke;
			xmlTxt+="</uds:STROKE>";
		}
		if (this.Strokif!=null){
			xmlTxt+="\n<uds:STROKIF";
			xmlTxt+=">";
			xmlTxt+=this.Strokif;
			xmlTxt+="</uds:STROKIF>";
		}
		if (this.Hyceph!=null){
			xmlTxt+="\n<uds:HYCEPH";
			xmlTxt+=">";
			xmlTxt+=this.Hyceph;
			xmlTxt+="</uds:HYCEPH>";
		}
		if (this.Hycephif!=null){
			xmlTxt+="\n<uds:HYCEPHIF";
			xmlTxt+=">";
			xmlTxt+=this.Hycephif;
			xmlTxt+="</uds:HYCEPHIF>";
		}
		if (this.Brninj!=null){
			xmlTxt+="\n<uds:BRNINJ";
			xmlTxt+=">";
			xmlTxt+=this.Brninj;
			xmlTxt+="</uds:BRNINJ>";
		}
		if (this.Brninjif!=null){
			xmlTxt+="\n<uds:BRNINJIF";
			xmlTxt+=">";
			xmlTxt+=this.Brninjif;
			xmlTxt+="</uds:BRNINJIF>";
		}
		if (this.Neop!=null){
			xmlTxt+="\n<uds:NEOP";
			xmlTxt+=">";
			xmlTxt+=this.Neop;
			xmlTxt+="</uds:NEOP>";
		}
		if (this.Neopif!=null){
			xmlTxt+="\n<uds:NEOPIF";
			xmlTxt+=">";
			xmlTxt+=this.Neopif;
			xmlTxt+="</uds:NEOPIF>";
		}
		if (this.Cogoth!=null){
			xmlTxt+="\n<uds:COGOTH";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth;
			xmlTxt+="</uds:COGOTH>";
		}
		if (this.Cogothx!=null){
			xmlTxt+="\n<uds:COGOTHX";
			xmlTxt+=">";
			xmlTxt+=this.Cogothx.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:COGOTHX>";
		}
		if (this.Cogothif!=null){
			xmlTxt+="\n<uds:COGOTHIF";
			xmlTxt+=">";
			xmlTxt+=this.Cogothif;
			xmlTxt+="</uds:COGOTHIF>";
		}
		if (this.Cogoth2!=null){
			xmlTxt+="\n<uds:COGOTH2";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth2;
			xmlTxt+="</uds:COGOTH2>";
		}
		if (this.Cogoth2x!=null){
			xmlTxt+="\n<uds:COGOTH2X";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth2x.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:COGOTH2X>";
		}
		if (this.Cogoth2f!=null){
			xmlTxt+="\n<uds:COGOTH2F";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth2f;
			xmlTxt+="</uds:COGOTH2F>";
		}
		if (this.Cogoth3!=null){
			xmlTxt+="\n<uds:COGOTH3";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth3;
			xmlTxt+="</uds:COGOTH3>";
		}
		if (this.Cogoth3x!=null){
			xmlTxt+="\n<uds:COGOTH3X";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth3x.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</uds:COGOTH3X>";
		}
		if (this.Cogoth3f!=null){
			xmlTxt+="\n<uds:COGOTH3F";
			xmlTxt+=">";
			xmlTxt+=this.Cogoth3f;
			xmlTxt+="</uds:COGOTH3F>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Whodiddx!=null) return true;
		if (this.Normcog!=null) return true;
		if (this.Demented!=null) return true;
		if (this.Mciamem!=null) return true;
		if (this.Mciaplus!=null) return true;
		if (this.Mciaplan!=null) return true;
		if (this.Mciapatt!=null) return true;
		if (this.Mciapex!=null) return true;
		if (this.Mciapvis!=null) return true;
		if (this.Mcinon1!=null) return true;
		if (this.Mcin1lan!=null) return true;
		if (this.Mcin1att!=null) return true;
		if (this.Mcin1ex!=null) return true;
		if (this.Mcin1vis!=null) return true;
		if (this.Mcinon2!=null) return true;
		if (this.Mcin2lan!=null) return true;
		if (this.Mcin2att!=null) return true;
		if (this.Mcin2ex!=null) return true;
		if (this.Mcin2vis!=null) return true;
		if (this.Impnomci!=null) return true;
		if (this.Probad!=null) return true;
		if (this.Probadif!=null) return true;
		if (this.Possad!=null) return true;
		if (this.Possadif!=null) return true;
		if (this.Dlb!=null) return true;
		if (this.Dlbif!=null) return true;
		if (this.Vasc!=null) return true;
		if (this.Vascif!=null) return true;
		if (this.Vascps!=null) return true;
		if (this.Vascpsif!=null) return true;
		if (this.Alcdem!=null) return true;
		if (this.Alcdemif!=null) return true;
		if (this.Demun!=null) return true;
		if (this.Demunif!=null) return true;
		if (this.Ftd!=null) return true;
		if (this.Ftdif!=null) return true;
		if (this.Ppaph!=null) return true;
		if (this.Ppaphif!=null) return true;
		if (this.Pnaph!=null) return true;
		if (this.Semdeman!=null) return true;
		if (this.Semdemag!=null) return true;
		if (this.Ppaothr!=null) return true;
		if (this.Psp!=null) return true;
		if (this.Pspif!=null) return true;
		if (this.Cort!=null) return true;
		if (this.Cortif!=null) return true;
		if (this.Hunt!=null) return true;
		if (this.Huntif!=null) return true;
		if (this.Prion!=null) return true;
		if (this.Prionif!=null) return true;
		if (this.Meds!=null) return true;
		if (this.Medsif!=null) return true;
		if (this.Dysill!=null) return true;
		if (this.Dysillif!=null) return true;
		if (this.Dep!=null) return true;
		if (this.Depif!=null) return true;
		if (this.Othpsy!=null) return true;
		if (this.Othpsyif!=null) return true;
		if (this.Downs!=null) return true;
		if (this.Downsif!=null) return true;
		if (this.Park!=null) return true;
		if (this.Parkif!=null) return true;
		if (this.Stroke!=null) return true;
		if (this.Strokif!=null) return true;
		if (this.Hyceph!=null) return true;
		if (this.Hycephif!=null) return true;
		if (this.Brninj!=null) return true;
		if (this.Brninjif!=null) return true;
		if (this.Neop!=null) return true;
		if (this.Neopif!=null) return true;
		if (this.Cogoth!=null) return true;
		if (this.Cogothx!=null) return true;
		if (this.Cogothif!=null) return true;
		if (this.Cogoth2!=null) return true;
		if (this.Cogoth2x!=null) return true;
		if (this.Cogoth2f!=null) return true;
		if (this.Cogoth3!=null) return true;
		if (this.Cogoth3x!=null) return true;
		if (this.Cogoth3f!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
