/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function ls2_lsNeuropsychData(){
this.xsiType="ls2:lsNeuropsychData";

	this.getSchemaElementName=function(){
		return "lsNeuropsychData";
	}

	this.getFullSchemaElementName=function(){
		return "ls2:lsNeuropsychData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Teststatus_tasksmissing=null;


	function getTeststatus_tasksmissing() {
		return this.Teststatus_tasksmissing;
	}
	this.getTeststatus_tasksmissing=getTeststatus_tasksmissing;


	function setTeststatus_tasksmissing(v){
		this.Teststatus_tasksmissing=v;
	}
	this.setTeststatus_tasksmissing=setTeststatus_tasksmissing;

	this.Teststatus_complete=null;


	function getTeststatus_complete() {
		return this.Teststatus_complete;
	}
	this.getTeststatus_complete=getTeststatus_complete;


	function setTeststatus_complete(v){
		this.Teststatus_complete=v;
	}
	this.setTeststatus_complete=setTeststatus_complete;

	this.Source=null;


	function getSource() {
		return this.Source;
	}
	this.getSource=getSource;


	function setSource(v){
		this.Source=v;
	}
	this.setSource=setSource;

	this.Oasisdate=null;


	function getOasisdate() {
		return this.Oasisdate;
	}
	this.getOasisdate=getOasisdate;


	function setOasisdate(v){
		this.Oasisdate=v;
	}
	this.setOasisdate=setOasisdate;

	this.Mmse=null;


	function getMmse() {
		return this.Mmse;
	}
	this.getMmse=getMmse;


	function setMmse(v){
		this.Mmse=v;
	}
	this.setMmse=setMmse;

	this.Blessed=null;


	function getBlessed() {
		return this.Blessed;
	}
	this.getBlessed=getBlessed;


	function setBlessed(v){
		this.Blessed=v;
	}
	this.setBlessed=setBlessed;

	this.CvltIi_cTa1=null;


	function getCvltIi_cTa1() {
		return this.CvltIi_cTa1;
	}
	this.getCvltIi_cTa1=getCvltIi_cTa1;


	function setCvltIi_cTa1(v){
		this.CvltIi_cTa1=v;
	}
	this.setCvltIi_cTa1=setCvltIi_cTa1;

	this.CvltIi_cTa5=null;


	function getCvltIi_cTa5() {
		return this.CvltIi_cTa5;
	}
	this.getCvltIi_cTa5=getCvltIi_cTa5;


	function setCvltIi_cTa5(v){
		this.CvltIi_cTa5=v;
	}
	this.setCvltIi_cTa5=setCvltIi_cTa5;

	this.CvltIi_cTotal=null;


	function getCvltIi_cTotal() {
		return this.CvltIi_cTotal;
	}
	this.getCvltIi_cTotal=getCvltIi_cTotal;


	function setCvltIi_cTotal(v){
		this.CvltIi_cTotal=v;
	}
	this.setCvltIi_cTotal=setCvltIi_cTotal;

	this.CvltIi_cListb=null;


	function getCvltIi_cListb() {
		return this.CvltIi_cListb;
	}
	this.getCvltIi_cListb=getCvltIi_cListb;


	function setCvltIi_cListb(v){
		this.CvltIi_cListb=v;
	}
	this.setCvltIi_cListb=setCvltIi_cListb;

	this.CvltIi_cSdfree=null;


	function getCvltIi_cSdfree() {
		return this.CvltIi_cSdfree;
	}
	this.getCvltIi_cSdfree=getCvltIi_cSdfree;


	function setCvltIi_cSdfree(v){
		this.CvltIi_cSdfree=v;
	}
	this.setCvltIi_cSdfree=setCvltIi_cSdfree;

	this.CvltIi_cSdcue=null;


	function getCvltIi_cSdcue() {
		return this.CvltIi_cSdcue;
	}
	this.getCvltIi_cSdcue=getCvltIi_cSdcue;


	function setCvltIi_cSdcue(v){
		this.CvltIi_cSdcue=v;
	}
	this.setCvltIi_cSdcue=setCvltIi_cSdcue;

	this.CvltIi_cLdfree=null;


	function getCvltIi_cLdfree() {
		return this.CvltIi_cLdfree;
	}
	this.getCvltIi_cLdfree=getCvltIi_cLdfree;


	function setCvltIi_cLdfree(v){
		this.CvltIi_cLdfree=v;
	}
	this.setCvltIi_cLdfree=setCvltIi_cLdfree;

	this.CvltIi_cLdcue=null;


	function getCvltIi_cLdcue() {
		return this.CvltIi_cLdcue;
	}
	this.getCvltIi_cLdcue=getCvltIi_cLdcue;


	function setCvltIi_cLdcue(v){
		this.CvltIi_cLdcue=v;
	}
	this.setCvltIi_cLdcue=setCvltIi_cLdcue;

	this.CvltIi_cSemclu=null;


	function getCvltIi_cSemclu() {
		return this.CvltIi_cSemclu;
	}
	this.getCvltIi_cSemclu=getCvltIi_cSemclu;


	function setCvltIi_cSemclu(v){
		this.CvltIi_cSemclu=v;
	}
	this.setCvltIi_cSemclu=setCvltIi_cSemclu;

	this.CvltIi_cSerclu=null;


	function getCvltIi_cSerclu() {
		return this.CvltIi_cSerclu;
	}
	this.getCvltIi_cSerclu=getCvltIi_cSerclu;


	function setCvltIi_cSerclu(v){
		this.CvltIi_cSerclu=v;
	}
	this.setCvltIi_cSerclu=setCvltIi_cSerclu;

	this.CvltIi_cPrimac=null;


	function getCvltIi_cPrimac() {
		return this.CvltIi_cPrimac;
	}
	this.getCvltIi_cPrimac=getCvltIi_cPrimac;


	function setCvltIi_cPrimac(v){
		this.CvltIi_cPrimac=v;
	}
	this.setCvltIi_cPrimac=setCvltIi_cPrimac;

	this.CvltIi_cMiddle=null;


	function getCvltIi_cMiddle() {
		return this.CvltIi_cMiddle;
	}
	this.getCvltIi_cMiddle=getCvltIi_cMiddle;


	function setCvltIi_cMiddle(v){
		this.CvltIi_cMiddle=v;
	}
	this.setCvltIi_cMiddle=setCvltIi_cMiddle;

	this.CvltIi_cRecenc=null;


	function getCvltIi_cRecenc() {
		return this.CvltIi_cRecenc;
	}
	this.getCvltIi_cRecenc=getCvltIi_cRecenc;


	function setCvltIi_cRecenc(v){
		this.CvltIi_cRecenc=v;
	}
	this.setCvltIi_cRecenc=setCvltIi_cRecenc;

	this.CvltIi_cSlope=null;


	function getCvltIi_cSlope() {
		return this.CvltIi_cSlope;
	}
	this.getCvltIi_cSlope=getCvltIi_cSlope;


	function setCvltIi_cSlope(v){
		this.CvltIi_cSlope=v;
	}
	this.setCvltIi_cSlope=setCvltIi_cSlope;

	this.CvltIi_cConsis=null;


	function getCvltIi_cConsis() {
		return this.CvltIi_cConsis;
	}
	this.getCvltIi_cConsis=getCvltIi_cConsis;


	function setCvltIi_cConsis(v){
		this.CvltIi_cConsis=v;
	}
	this.setCvltIi_cConsis=setCvltIi_cConsis;

	this.CvltIi_cProint=null;


	function getCvltIi_cProint() {
		return this.CvltIi_cProint;
	}
	this.getCvltIi_cProint=getCvltIi_cProint;


	function setCvltIi_cProint(v){
		this.CvltIi_cProint=v;
	}
	this.setCvltIi_cProint=setCvltIi_cProint;

	this.CvltIi_cRetroint=null;


	function getCvltIi_cRetroint() {
		return this.CvltIi_cRetroint;
	}
	this.getCvltIi_cRetroint=getCvltIi_cRetroint;


	function setCvltIi_cRetroint(v){
		this.CvltIi_cRetroint=v;
	}
	this.setCvltIi_cRetroint=setCvltIi_cRetroint;

	this.CvltIi_cPersev=null;


	function getCvltIi_cPersev() {
		return this.CvltIi_cPersev;
	}
	this.getCvltIi_cPersev=getCvltIi_cPersev;


	function setCvltIi_cPersev(v){
		this.CvltIi_cPersev=v;
	}
	this.setCvltIi_cPersev=setCvltIi_cPersev;

	this.CvltIi_cIntrus=null;


	function getCvltIi_cIntrus() {
		return this.CvltIi_cIntrus;
	}
	this.getCvltIi_cIntrus=getCvltIi_cIntrus;


	function setCvltIi_cIntrus(v){
		this.CvltIi_cIntrus=v;
	}
	this.setCvltIi_cIntrus=setCvltIi_cIntrus;

	this.CvltIi_cTiri=null;


	function getCvltIi_cTiri() {
		return this.CvltIi_cTiri;
	}
	this.getCvltIi_cTiri=getCvltIi_cTiri;


	function setCvltIi_cTiri(v){
		this.CvltIi_cTiri=v;
	}
	this.setCvltIi_cTiri=setCvltIi_cTiri;

	this.CvltIi_cTdri=null;


	function getCvltIi_cTdri() {
		return this.CvltIi_cTdri;
	}
	this.getCvltIi_cTdri=getCvltIi_cTdri;


	function setCvltIi_cTdri(v){
		this.CvltIi_cTdri=v;
	}
	this.setCvltIi_cTdri=setCvltIi_cTdri;

	this.CvltIi_cTfri=null;


	function getCvltIi_cTfri() {
		return this.CvltIi_cTfri;
	}
	this.getCvltIi_cTfri=getCvltIi_cTfri;


	function setCvltIi_cTfri(v){
		this.CvltIi_cTfri=v;
	}
	this.setCvltIi_cTfri=setCvltIi_cTfri;

	this.CvltIi_cTcri=null;


	function getCvltIi_cTcri() {
		return this.CvltIi_cTcri;
	}
	this.getCvltIi_cTcri=getCvltIi_cTcri;


	function setCvltIi_cTcri(v){
		this.CvltIi_cTcri=v;
	}
	this.setCvltIi_cTcri=setCvltIi_cTcri;

	this.CvltIi_cTcnci=null;


	function getCvltIi_cTcnci() {
		return this.CvltIi_cTcnci;
	}
	this.getCvltIi_cTcnci=getCvltIi_cTcnci;


	function setCvltIi_cTcnci(v){
		this.CvltIi_cTcnci=v;
	}
	this.setCvltIi_cTcnci=setCvltIi_cTcnci;

	this.CvltIi_cTssi=null;


	function getCvltIi_cTssi() {
		return this.CvltIi_cTssi;
	}
	this.getCvltIi_cTssi=getCvltIi_cTssi;


	function setCvltIi_cTssi(v){
		this.CvltIi_cTssi=v;
	}
	this.setCvltIi_cTssi=setCvltIi_cTssi;

	this.CvltIi_cTali=null;


	function getCvltIi_cTali() {
		return this.CvltIi_cTali;
	}
	this.getCvltIi_cTali=getCvltIi_cTali;


	function setCvltIi_cTali(v){
		this.CvltIi_cTali=v;
	}
	this.setCvltIi_cTali=setCvltIi_cTali;

	this.CvltIi_cTrd=null;


	function getCvltIi_cTrd() {
		return this.CvltIi_cTrd;
	}
	this.getCvltIi_cTrd=getCvltIi_cTrd;


	function setCvltIi_cTrd(v){
		this.CvltIi_cTrd=v;
	}
	this.setCvltIi_cTrd=setCvltIi_cTrd;

	this.CvltIi_cIrd=null;


	function getCvltIi_cIrd() {
		return this.CvltIi_cIrd;
	}
	this.getCvltIi_cIrd=getCvltIi_cIrd;


	function setCvltIi_cIrd(v){
		this.CvltIi_cIrd=v;
	}
	this.setCvltIi_cIrd=setCvltIi_cIrd;

	this.CvltIi_cDrd=null;


	function getCvltIi_cDrd() {
		return this.CvltIi_cDrd;
	}
	this.getCvltIi_cDrd=getCvltIi_cDrd;


	function setCvltIi_cDrd(v){
		this.CvltIi_cDrd=v;
	}
	this.setCvltIi_cDrd=setCvltIi_cDrd;

	this.CvltIi_cFrd=null;


	function getCvltIi_cFrd() {
		return this.CvltIi_cFrd;
	}
	this.getCvltIi_cFrd=getCvltIi_cFrd;


	function setCvltIi_cFrd(v){
		this.CvltIi_cFrd=v;
	}
	this.setCvltIi_cFrd=setCvltIi_cFrd;

	this.CvltIi_cCrd=null;


	function getCvltIi_cCrd() {
		return this.CvltIi_cCrd;
	}
	this.getCvltIi_cCrd=getCvltIi_cCrd;


	function setCvltIi_cCrd(v){
		this.CvltIi_cCrd=v;
	}
	this.setCvltIi_cCrd=setCvltIi_cCrd;

	this.CvltIi_cRechits=null;


	function getCvltIi_cRechits() {
		return this.CvltIi_cRechits;
	}
	this.getCvltIi_cRechits=getCvltIi_cRechits;


	function setCvltIi_cRechits(v){
		this.CvltIi_cRechits=v;
	}
	this.setCvltIi_cRechits=setCvltIi_cRechits;

	this.CvltIi_cRecfp=null;


	function getCvltIi_cRecfp() {
		return this.CvltIi_cRecfp;
	}
	this.getCvltIi_cRecfp=getCvltIi_cRecfp;


	function setCvltIi_cRecfp(v){
		this.CvltIi_cRecfp=v;
	}
	this.setCvltIi_cRecfp=setCvltIi_cRecfp;

	this.CvltIi_cRecdisc=null;


	function getCvltIi_cRecdisc() {
		return this.CvltIi_cRecdisc;
	}
	this.getCvltIi_cRecdisc=getCvltIi_cRecdisc;


	function setCvltIi_cRecdisc(v){
		this.CvltIi_cRecdisc=v;
	}
	this.setCvltIi_cRecdisc=setCvltIi_cRecdisc;

	this.CvltIi_cBias=null;


	function getCvltIi_cBias() {
		return this.CvltIi_cBias;
	}
	this.getCvltIi_cBias=getCvltIi_cBias;


	function setCvltIi_cBias(v){
		this.CvltIi_cBias=v;
	}
	this.setCvltIi_cBias=setCvltIi_cBias;

	this.Temporalordermemory_tomRecall=null;


	function getTemporalordermemory_tomRecall() {
		return this.Temporalordermemory_tomRecall;
	}
	this.getTemporalordermemory_tomRecall=getTemporalordermemory_tomRecall;


	function setTemporalordermemory_tomRecall(v){
		this.Temporalordermemory_tomRecall=v;
	}
	this.setTemporalordermemory_tomRecall=setTemporalordermemory_tomRecall;

	this.Temporalordermemory_tomHits=null;


	function getTemporalordermemory_tomHits() {
		return this.Temporalordermemory_tomHits;
	}
	this.getTemporalordermemory_tomHits=getTemporalordermemory_tomHits;


	function setTemporalordermemory_tomHits(v){
		this.Temporalordermemory_tomHits=v;
	}
	this.setTemporalordermemory_tomHits=setTemporalordermemory_tomHits;

	this.Temporalordermemory_tomFa=null;


	function getTemporalordermemory_tomFa() {
		return this.Temporalordermemory_tomFa;
	}
	this.getTemporalordermemory_tomFa=getTemporalordermemory_tomFa;


	function setTemporalordermemory_tomFa(v){
		this.Temporalordermemory_tomFa=v;
	}
	this.setTemporalordermemory_tomFa=setTemporalordermemory_tomFa;

	this.Temporalordermemory_tomDiscrim=null;


	function getTemporalordermemory_tomDiscrim() {
		return this.Temporalordermemory_tomDiscrim;
	}
	this.getTemporalordermemory_tomDiscrim=getTemporalordermemory_tomDiscrim;


	function setTemporalordermemory_tomDiscrim(v){
		this.Temporalordermemory_tomDiscrim=v;
	}
	this.setTemporalordermemory_tomDiscrim=setTemporalordermemory_tomDiscrim;

	this.WmsIii_verbalpairedassociates_vpaIm=null;


	function getWmsIii_verbalpairedassociates_vpaIm() {
		return this.WmsIii_verbalpairedassociates_vpaIm;
	}
	this.getWmsIii_verbalpairedassociates_vpaIm=getWmsIii_verbalpairedassociates_vpaIm;


	function setWmsIii_verbalpairedassociates_vpaIm(v){
		this.WmsIii_verbalpairedassociates_vpaIm=v;
	}
	this.setWmsIii_verbalpairedassociates_vpaIm=setWmsIii_verbalpairedassociates_vpaIm;

	this.WmsIii_verbalpairedassociates_vpaDel=null;


	function getWmsIii_verbalpairedassociates_vpaDel() {
		return this.WmsIii_verbalpairedassociates_vpaDel;
	}
	this.getWmsIii_verbalpairedassociates_vpaDel=getWmsIii_verbalpairedassociates_vpaDel;


	function setWmsIii_verbalpairedassociates_vpaDel(v){
		this.WmsIii_verbalpairedassociates_vpaDel=v;
	}
	this.setWmsIii_verbalpairedassociates_vpaDel=setWmsIii_verbalpairedassociates_vpaDel;

	this.WmsIii_verbalpairedassociates_vpaRecog=null;


	function getWmsIii_verbalpairedassociates_vpaRecog() {
		return this.WmsIii_verbalpairedassociates_vpaRecog;
	}
	this.getWmsIii_verbalpairedassociates_vpaRecog=getWmsIii_verbalpairedassociates_vpaRecog;


	function setWmsIii_verbalpairedassociates_vpaRecog(v){
		this.WmsIii_verbalpairedassociates_vpaRecog=v;
	}
	this.setWmsIii_verbalpairedassociates_vpaRecog=setWmsIii_verbalpairedassociates_vpaRecog;

	this.WmsIii_logicalmemory_lmIm=null;


	function getWmsIii_logicalmemory_lmIm() {
		return this.WmsIii_logicalmemory_lmIm;
	}
	this.getWmsIii_logicalmemory_lmIm=getWmsIii_logicalmemory_lmIm;


	function setWmsIii_logicalmemory_lmIm(v){
		this.WmsIii_logicalmemory_lmIm=v;
	}
	this.setWmsIii_logicalmemory_lmIm=setWmsIii_logicalmemory_lmIm;

	this.WmsIii_logicalmemory_lmDel=null;


	function getWmsIii_logicalmemory_lmDel() {
		return this.WmsIii_logicalmemory_lmDel;
	}
	this.getWmsIii_logicalmemory_lmDel=getWmsIii_logicalmemory_lmDel;


	function setWmsIii_logicalmemory_lmDel(v){
		this.WmsIii_logicalmemory_lmDel=v;
	}
	this.setWmsIii_logicalmemory_lmDel=setWmsIii_logicalmemory_lmDel;

	this.WmsIii_logicalmemory_lmRecog=null;


	function getWmsIii_logicalmemory_lmRecog() {
		return this.WmsIii_logicalmemory_lmRecog;
	}
	this.getWmsIii_logicalmemory_lmRecog=getWmsIii_logicalmemory_lmRecog;


	function setWmsIii_logicalmemory_lmRecog(v){
		this.WmsIii_logicalmemory_lmRecog=v;
	}
	this.setWmsIii_logicalmemory_lmRecog=setWmsIii_logicalmemory_lmRecog;

	this.WmsIii_audRecDelay=null;


	function getWmsIii_audRecDelay() {
		return this.WmsIii_audRecDelay;
	}
	this.getWmsIii_audRecDelay=getWmsIii_audRecDelay;


	function setWmsIii_audRecDelay(v){
		this.WmsIii_audRecDelay=v;
	}
	this.setWmsIii_audRecDelay=setWmsIii_audRecDelay;

	this.WmsIii_lnseq=null;


	function getWmsIii_lnseq() {
		return this.WmsIii_lnseq;
	}
	this.getWmsIii_lnseq=getWmsIii_lnseq;


	function setWmsIii_lnseq(v){
		this.WmsIii_lnseq=v;
	}
	this.setWmsIii_lnseq=setWmsIii_lnseq;

	this.Digsym=null;


	function getDigsym() {
		return this.Digsym;
	}
	this.getDigsym=getDigsym;


	function setDigsym(v){
		this.Digsym=v;
	}
	this.setDigsym=setDigsym;

	this.Dotcomparison_dcomp1=null;


	function getDotcomparison_dcomp1() {
		return this.Dotcomparison_dcomp1;
	}
	this.getDotcomparison_dcomp1=getDotcomparison_dcomp1;


	function setDotcomparison_dcomp1(v){
		this.Dotcomparison_dcomp1=v;
	}
	this.setDotcomparison_dcomp1=setDotcomparison_dcomp1;

	this.Dotcomparison_dcomp2=null;


	function getDotcomparison_dcomp2() {
		return this.Dotcomparison_dcomp2;
	}
	this.getDotcomparison_dcomp2=getDotcomparison_dcomp2;


	function setDotcomparison_dcomp2(v){
		this.Dotcomparison_dcomp2=v;
	}
	this.setDotcomparison_dcomp2=setDotcomparison_dcomp2;

	this.Dotcomparison_dcomp3=null;


	function getDotcomparison_dcomp3() {
		return this.Dotcomparison_dcomp3;
	}
	this.getDotcomparison_dcomp3=getDotcomparison_dcomp3;


	function setDotcomparison_dcomp3(v){
		this.Dotcomparison_dcomp3=v;
	}
	this.setDotcomparison_dcomp3=setDotcomparison_dcomp3;

	this.Dotcomparison_dcompa=null;


	function getDotcomparison_dcompa() {
		return this.Dotcomparison_dcompa;
	}
	this.getDotcomparison_dcompa=getDotcomparison_dcompa;


	function setDotcomparison_dcompa(v){
		this.Dotcomparison_dcompa=v;
	}
	this.setDotcomparison_dcompa=setDotcomparison_dcompa;

	this.Trailmaking_tma=null;


	function getTrailmaking_tma() {
		return this.Trailmaking_tma;
	}
	this.getTrailmaking_tma=getTrailmaking_tma;


	function setTrailmaking_tma(v){
		this.Trailmaking_tma=v;
	}
	this.setTrailmaking_tma=setTrailmaking_tma;

	this.Trailmaking_tmb=null;


	function getTrailmaking_tmb() {
		return this.Trailmaking_tmb;
	}
	this.getTrailmaking_tmb=getTrailmaking_tmb;


	function setTrailmaking_tmb(v){
		this.Trailmaking_tmb=v;
	}
	this.setTrailmaking_tmb=setTrailmaking_tmb;

	this.Buildmem=null;


	function getBuildmem() {
		return this.Buildmem;
	}
	this.getBuildmem=getBuildmem;


	function setBuildmem(v){
		this.Buildmem=v;
	}
	this.setBuildmem=setBuildmem;

	this.Stroop_strpc=null;


	function getStroop_strpc() {
		return this.Stroop_strpc;
	}
	this.getStroop_strpc=getStroop_strpc;


	function setStroop_strpc(v){
		this.Stroop_strpc=v;
	}
	this.setStroop_strpc=setStroop_strpc;

	this.Stroop_strpw=null;


	function getStroop_strpw() {
		return this.Stroop_strpw;
	}
	this.getStroop_strpw=getStroop_strpw;


	function setStroop_strpw(v){
		this.Stroop_strpw=v;
	}
	this.setStroop_strpw=setStroop_strpw;

	this.Stroop_strpcw=null;


	function getStroop_strpcw() {
		return this.Stroop_strpcw;
	}
	this.getStroop_strpcw=getStroop_strpcw;


	function setStroop_strpcw(v){
		this.Stroop_strpcw=v;
	}
	this.setStroop_strpcw=setStroop_strpcw;

	this.Stroop_strpint=null;


	function getStroop_strpint() {
		return this.Stroop_strpint;
	}
	this.getStroop_strpint=getStroop_strpint;


	function setStroop_strpint(v){
		this.Stroop_strpint=v;
	}
	this.setStroop_strpint=setStroop_strpint;

	this.PfA=null;


	function getPfA() {
		return this.PfA;
	}
	this.getPfA=getPfA;


	function setPfA(v){
		this.PfA=v;
	}
	this.setPfA=setPfA;

	this.SfRound=null;


	function getSfRound() {
		return this.SfRound;
	}
	this.getSfRound=getSfRound;


	function setSfRound(v){
		this.SfRound=v;
	}
	this.setSfRound=setSfRound;

	this.Shipley=null;


	function getShipley() {
		return this.Shipley;
	}
	this.getShipley=getShipley;


	function setShipley(v){
		this.Shipley=v;
	}
	this.setShipley=setShipley;

	this.WjiiiLwi=null;


	function getWjiiiLwi() {
		return this.WjiiiLwi;
	}
	this.getWjiiiLwi=getWjiiiLwi;


	function setWjiiiLwi(v){
		this.WjiiiLwi=v;
	}
	this.setWjiiiLwi=setWjiiiLwi;

	this.Wasi_wasiVocab=null;


	function getWasi_wasiVocab() {
		return this.Wasi_wasiVocab;
	}
	this.getWasi_wasiVocab=getWasi_wasiVocab;


	function setWasi_wasiVocab(v){
		this.Wasi_wasiVocab=v;
	}
	this.setWasi_wasiVocab=setWasi_wasiVocab;

	this.Wasi_wasiMr=null;


	function getWasi_wasiMr() {
		return this.Wasi_wasiMr;
	}
	this.getWasi_wasiMr=getWasi_wasiMr;


	function setWasi_wasiMr(v){
		this.Wasi_wasiMr=v;
	}
	this.setWasi_wasiMr=setWasi_wasiMr;

	this.Wasi_wasiSim=null;


	function getWasi_wasiSim() {
		return this.Wasi_wasiSim;
	}
	this.getWasi_wasiSim=getWasi_wasiSim;


	function setWasi_wasiSim(v){
		this.Wasi_wasiSim=v;
	}
	this.setWasi_wasiSim=setWasi_wasiSim;

	this.Wasi_wasiBd=null;


	function getWasi_wasiBd() {
		return this.Wasi_wasiBd;
	}
	this.getWasi_wasiBd=getWasi_wasiBd;


	function setWasi_wasiBd(v){
		this.Wasi_wasiBd=v;
	}
	this.setWasi_wasiBd=setWasi_wasiBd;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="testStatus/tasksMissing"){
				return this.Teststatus_tasksmissing ;
			} else 
			if(xmlPath=="testStatus/complete"){
				return this.Teststatus_complete ;
			} else 
			if(xmlPath=="source"){
				return this.Source ;
			} else 
			if(xmlPath=="oasisDate"){
				return this.Oasisdate ;
			} else 
			if(xmlPath=="MMSE"){
				return this.Mmse ;
			} else 
			if(xmlPath=="Blessed"){
				return this.Blessed ;
			} else 
			if(xmlPath=="CVLT_II/c_ta1"){
				return this.CvltIi_cTa1 ;
			} else 
			if(xmlPath=="CVLT_II/c_ta5"){
				return this.CvltIi_cTa5 ;
			} else 
			if(xmlPath=="CVLT_II/c_total"){
				return this.CvltIi_cTotal ;
			} else 
			if(xmlPath=="CVLT_II/c_listb"){
				return this.CvltIi_cListb ;
			} else 
			if(xmlPath=="CVLT_II/c_sdfree"){
				return this.CvltIi_cSdfree ;
			} else 
			if(xmlPath=="CVLT_II/c_sdcue"){
				return this.CvltIi_cSdcue ;
			} else 
			if(xmlPath=="CVLT_II/c_ldfree"){
				return this.CvltIi_cLdfree ;
			} else 
			if(xmlPath=="CVLT_II/c_ldcue"){
				return this.CvltIi_cLdcue ;
			} else 
			if(xmlPath=="CVLT_II/c_semclu"){
				return this.CvltIi_cSemclu ;
			} else 
			if(xmlPath=="CVLT_II/c_serclu"){
				return this.CvltIi_cSerclu ;
			} else 
			if(xmlPath=="CVLT_II/c_primac"){
				return this.CvltIi_cPrimac ;
			} else 
			if(xmlPath=="CVLT_II/c_middle"){
				return this.CvltIi_cMiddle ;
			} else 
			if(xmlPath=="CVLT_II/c_recenc"){
				return this.CvltIi_cRecenc ;
			} else 
			if(xmlPath=="CVLT_II/c_slope"){
				return this.CvltIi_cSlope ;
			} else 
			if(xmlPath=="CVLT_II/c_consis"){
				return this.CvltIi_cConsis ;
			} else 
			if(xmlPath=="CVLT_II/c_proint"){
				return this.CvltIi_cProint ;
			} else 
			if(xmlPath=="CVLT_II/c_retroint"){
				return this.CvltIi_cRetroint ;
			} else 
			if(xmlPath=="CVLT_II/c_persev"){
				return this.CvltIi_cPersev ;
			} else 
			if(xmlPath=="CVLT_II/c_intrus"){
				return this.CvltIi_cIntrus ;
			} else 
			if(xmlPath=="CVLT_II/c_tiri"){
				return this.CvltIi_cTiri ;
			} else 
			if(xmlPath=="CVLT_II/c_tdri"){
				return this.CvltIi_cTdri ;
			} else 
			if(xmlPath=="CVLT_II/c_tfri"){
				return this.CvltIi_cTfri ;
			} else 
			if(xmlPath=="CVLT_II/c_tcri"){
				return this.CvltIi_cTcri ;
			} else 
			if(xmlPath=="CVLT_II/c_tcnci"){
				return this.CvltIi_cTcnci ;
			} else 
			if(xmlPath=="CVLT_II/c_tssi"){
				return this.CvltIi_cTssi ;
			} else 
			if(xmlPath=="CVLT_II/c_tali"){
				return this.CvltIi_cTali ;
			} else 
			if(xmlPath=="CVLT_II/c_trd"){
				return this.CvltIi_cTrd ;
			} else 
			if(xmlPath=="CVLT_II/c_ird"){
				return this.CvltIi_cIrd ;
			} else 
			if(xmlPath=="CVLT_II/c_drd"){
				return this.CvltIi_cDrd ;
			} else 
			if(xmlPath=="CVLT_II/c_frd"){
				return this.CvltIi_cFrd ;
			} else 
			if(xmlPath=="CVLT_II/c_crd"){
				return this.CvltIi_cCrd ;
			} else 
			if(xmlPath=="CVLT_II/c_rechits"){
				return this.CvltIi_cRechits ;
			} else 
			if(xmlPath=="CVLT_II/c_recfp"){
				return this.CvltIi_cRecfp ;
			} else 
			if(xmlPath=="CVLT_II/c_recdisc"){
				return this.CvltIi_cRecdisc ;
			} else 
			if(xmlPath=="CVLT_II/c_bias"){
				return this.CvltIi_cBias ;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_recall"){
				return this.Temporalordermemory_tomRecall ;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_hits"){
				return this.Temporalordermemory_tomHits ;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_fa"){
				return this.Temporalordermemory_tomFa ;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_discrim"){
				return this.Temporalordermemory_tomDiscrim ;
			} else 
			if(xmlPath=="WMS_III/verbalPairedAssociates/vpa_im"){
				return this.WmsIii_verbalpairedassociates_vpaIm ;
			} else 
			if(xmlPath=="WMS_III/verbalPairedAssociates/vpa_del"){
				return this.WmsIii_verbalpairedassociates_vpaDel ;
			} else 
			if(xmlPath=="WMS_III/verbalPairedAssociates/vpa_recog"){
				return this.WmsIii_verbalpairedassociates_vpaRecog ;
			} else 
			if(xmlPath=="WMS_III/logicalMemory/lm_im"){
				return this.WmsIii_logicalmemory_lmIm ;
			} else 
			if(xmlPath=="WMS_III/logicalMemory/lm_del"){
				return this.WmsIii_logicalmemory_lmDel ;
			} else 
			if(xmlPath=="WMS_III/logicalMemory/lm_recog"){
				return this.WmsIii_logicalmemory_lmRecog ;
			} else 
			if(xmlPath=="WMS_III/aud_rec_delay"){
				return this.WmsIii_audRecDelay ;
			} else 
			if(xmlPath=="WMS_III/lnSeq"){
				return this.WmsIii_lnseq ;
			} else 
			if(xmlPath=="digSym"){
				return this.Digsym ;
			} else 
			if(xmlPath=="dotComparison/dcomp1"){
				return this.Dotcomparison_dcomp1 ;
			} else 
			if(xmlPath=="dotComparison/dcomp2"){
				return this.Dotcomparison_dcomp2 ;
			} else 
			if(xmlPath=="dotComparison/dcomp3"){
				return this.Dotcomparison_dcomp3 ;
			} else 
			if(xmlPath=="dotComparison/dcompa"){
				return this.Dotcomparison_dcompa ;
			} else 
			if(xmlPath=="trailMaking/tma"){
				return this.Trailmaking_tma ;
			} else 
			if(xmlPath=="trailMaking/tmb"){
				return this.Trailmaking_tmb ;
			} else 
			if(xmlPath=="buildMem"){
				return this.Buildmem ;
			} else 
			if(xmlPath=="Stroop/strpc"){
				return this.Stroop_strpc ;
			} else 
			if(xmlPath=="Stroop/strpw"){
				return this.Stroop_strpw ;
			} else 
			if(xmlPath=="Stroop/strpcw"){
				return this.Stroop_strpcw ;
			} else 
			if(xmlPath=="Stroop/strpint"){
				return this.Stroop_strpint ;
			} else 
			if(xmlPath=="pf_a"){
				return this.PfA ;
			} else 
			if(xmlPath=="sf_round"){
				return this.SfRound ;
			} else 
			if(xmlPath=="Shipley"){
				return this.Shipley ;
			} else 
			if(xmlPath=="wjIII_lwi"){
				return this.WjiiiLwi ;
			} else 
			if(xmlPath=="WASI/wasi_vocab"){
				return this.Wasi_wasiVocab ;
			} else 
			if(xmlPath=="WASI/wasi_mr"){
				return this.Wasi_wasiMr ;
			} else 
			if(xmlPath=="WASI/wasi_sim"){
				return this.Wasi_wasiSim ;
			} else 
			if(xmlPath=="WASI/wasi_bd"){
				return this.Wasi_wasiBd ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="testStatus/tasksMissing"){
				this.Teststatus_tasksmissing=value;
			} else 
			if(xmlPath=="testStatus/complete"){
				this.Teststatus_complete=value;
			} else 
			if(xmlPath=="source"){
				this.Source=value;
			} else 
			if(xmlPath=="oasisDate"){
				this.Oasisdate=value;
			} else 
			if(xmlPath=="MMSE"){
				this.Mmse=value;
			} else 
			if(xmlPath=="Blessed"){
				this.Blessed=value;
			} else 
			if(xmlPath=="CVLT_II/c_ta1"){
				this.CvltIi_cTa1=value;
			} else 
			if(xmlPath=="CVLT_II/c_ta5"){
				this.CvltIi_cTa5=value;
			} else 
			if(xmlPath=="CVLT_II/c_total"){
				this.CvltIi_cTotal=value;
			} else 
			if(xmlPath=="CVLT_II/c_listb"){
				this.CvltIi_cListb=value;
			} else 
			if(xmlPath=="CVLT_II/c_sdfree"){
				this.CvltIi_cSdfree=value;
			} else 
			if(xmlPath=="CVLT_II/c_sdcue"){
				this.CvltIi_cSdcue=value;
			} else 
			if(xmlPath=="CVLT_II/c_ldfree"){
				this.CvltIi_cLdfree=value;
			} else 
			if(xmlPath=="CVLT_II/c_ldcue"){
				this.CvltIi_cLdcue=value;
			} else 
			if(xmlPath=="CVLT_II/c_semclu"){
				this.CvltIi_cSemclu=value;
			} else 
			if(xmlPath=="CVLT_II/c_serclu"){
				this.CvltIi_cSerclu=value;
			} else 
			if(xmlPath=="CVLT_II/c_primac"){
				this.CvltIi_cPrimac=value;
			} else 
			if(xmlPath=="CVLT_II/c_middle"){
				this.CvltIi_cMiddle=value;
			} else 
			if(xmlPath=="CVLT_II/c_recenc"){
				this.CvltIi_cRecenc=value;
			} else 
			if(xmlPath=="CVLT_II/c_slope"){
				this.CvltIi_cSlope=value;
			} else 
			if(xmlPath=="CVLT_II/c_consis"){
				this.CvltIi_cConsis=value;
			} else 
			if(xmlPath=="CVLT_II/c_proint"){
				this.CvltIi_cProint=value;
			} else 
			if(xmlPath=="CVLT_II/c_retroint"){
				this.CvltIi_cRetroint=value;
			} else 
			if(xmlPath=="CVLT_II/c_persev"){
				this.CvltIi_cPersev=value;
			} else 
			if(xmlPath=="CVLT_II/c_intrus"){
				this.CvltIi_cIntrus=value;
			} else 
			if(xmlPath=="CVLT_II/c_tiri"){
				this.CvltIi_cTiri=value;
			} else 
			if(xmlPath=="CVLT_II/c_tdri"){
				this.CvltIi_cTdri=value;
			} else 
			if(xmlPath=="CVLT_II/c_tfri"){
				this.CvltIi_cTfri=value;
			} else 
			if(xmlPath=="CVLT_II/c_tcri"){
				this.CvltIi_cTcri=value;
			} else 
			if(xmlPath=="CVLT_II/c_tcnci"){
				this.CvltIi_cTcnci=value;
			} else 
			if(xmlPath=="CVLT_II/c_tssi"){
				this.CvltIi_cTssi=value;
			} else 
			if(xmlPath=="CVLT_II/c_tali"){
				this.CvltIi_cTali=value;
			} else 
			if(xmlPath=="CVLT_II/c_trd"){
				this.CvltIi_cTrd=value;
			} else 
			if(xmlPath=="CVLT_II/c_ird"){
				this.CvltIi_cIrd=value;
			} else 
			if(xmlPath=="CVLT_II/c_drd"){
				this.CvltIi_cDrd=value;
			} else 
			if(xmlPath=="CVLT_II/c_frd"){
				this.CvltIi_cFrd=value;
			} else 
			if(xmlPath=="CVLT_II/c_crd"){
				this.CvltIi_cCrd=value;
			} else 
			if(xmlPath=="CVLT_II/c_rechits"){
				this.CvltIi_cRechits=value;
			} else 
			if(xmlPath=="CVLT_II/c_recfp"){
				this.CvltIi_cRecfp=value;
			} else 
			if(xmlPath=="CVLT_II/c_recdisc"){
				this.CvltIi_cRecdisc=value;
			} else 
			if(xmlPath=="CVLT_II/c_bias"){
				this.CvltIi_cBias=value;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_recall"){
				this.Temporalordermemory_tomRecall=value;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_hits"){
				this.Temporalordermemory_tomHits=value;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_fa"){
				this.Temporalordermemory_tomFa=value;
			} else 
			if(xmlPath=="temporalOrderMemory/TOM_discrim"){
				this.Temporalordermemory_tomDiscrim=value;
			} else 
			if(xmlPath=="WMS_III/verbalPairedAssociates/vpa_im"){
				this.WmsIii_verbalpairedassociates_vpaIm=value;
			} else 
			if(xmlPath=="WMS_III/verbalPairedAssociates/vpa_del"){
				this.WmsIii_verbalpairedassociates_vpaDel=value;
			} else 
			if(xmlPath=="WMS_III/verbalPairedAssociates/vpa_recog"){
				this.WmsIii_verbalpairedassociates_vpaRecog=value;
			} else 
			if(xmlPath=="WMS_III/logicalMemory/lm_im"){
				this.WmsIii_logicalmemory_lmIm=value;
			} else 
			if(xmlPath=="WMS_III/logicalMemory/lm_del"){
				this.WmsIii_logicalmemory_lmDel=value;
			} else 
			if(xmlPath=="WMS_III/logicalMemory/lm_recog"){
				this.WmsIii_logicalmemory_lmRecog=value;
			} else 
			if(xmlPath=="WMS_III/aud_rec_delay"){
				this.WmsIii_audRecDelay=value;
			} else 
			if(xmlPath=="WMS_III/lnSeq"){
				this.WmsIii_lnseq=value;
			} else 
			if(xmlPath=="digSym"){
				this.Digsym=value;
			} else 
			if(xmlPath=="dotComparison/dcomp1"){
				this.Dotcomparison_dcomp1=value;
			} else 
			if(xmlPath=="dotComparison/dcomp2"){
				this.Dotcomparison_dcomp2=value;
			} else 
			if(xmlPath=="dotComparison/dcomp3"){
				this.Dotcomparison_dcomp3=value;
			} else 
			if(xmlPath=="dotComparison/dcompa"){
				this.Dotcomparison_dcompa=value;
			} else 
			if(xmlPath=="trailMaking/tma"){
				this.Trailmaking_tma=value;
			} else 
			if(xmlPath=="trailMaking/tmb"){
				this.Trailmaking_tmb=value;
			} else 
			if(xmlPath=="buildMem"){
				this.Buildmem=value;
			} else 
			if(xmlPath=="Stroop/strpc"){
				this.Stroop_strpc=value;
			} else 
			if(xmlPath=="Stroop/strpw"){
				this.Stroop_strpw=value;
			} else 
			if(xmlPath=="Stroop/strpcw"){
				this.Stroop_strpcw=value;
			} else 
			if(xmlPath=="Stroop/strpint"){
				this.Stroop_strpint=value;
			} else 
			if(xmlPath=="pf_a"){
				this.PfA=value;
			} else 
			if(xmlPath=="sf_round"){
				this.SfRound=value;
			} else 
			if(xmlPath=="Shipley"){
				this.Shipley=value;
			} else 
			if(xmlPath=="wjIII_lwi"){
				this.WjiiiLwi=value;
			} else 
			if(xmlPath=="WASI/wasi_vocab"){
				this.Wasi_wasiVocab=value;
			} else 
			if(xmlPath=="WASI/wasi_mr"){
				this.Wasi_wasiMr=value;
			} else 
			if(xmlPath=="WASI/wasi_sim"){
				this.Wasi_wasiSim=value;
			} else 
			if(xmlPath=="WASI/wasi_bd"){
				this.Wasi_wasiBd=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="testStatus/tasksMissing"){
			return "field_data";
		}else if (xmlPath=="testStatus/complete"){
			return "field_data";
		}else if (xmlPath=="source"){
			return "field_data";
		}else if (xmlPath=="oasisDate"){
			return "field_data";
		}else if (xmlPath=="MMSE"){
			return "field_data";
		}else if (xmlPath=="Blessed"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_ta1"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_ta5"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_total"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_listb"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_sdfree"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_sdcue"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_ldfree"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_ldcue"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_semclu"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_serclu"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_primac"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_middle"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_recenc"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_slope"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_consis"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_proint"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_retroint"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_persev"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_intrus"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tiri"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tdri"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tfri"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tcri"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tcnci"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tssi"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_tali"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_trd"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_ird"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_drd"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_frd"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_crd"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_rechits"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_recfp"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_recdisc"){
			return "field_data";
		}else if (xmlPath=="CVLT_II/c_bias"){
			return "field_data";
		}else if (xmlPath=="temporalOrderMemory/TOM_recall"){
			return "field_data";
		}else if (xmlPath=="temporalOrderMemory/TOM_hits"){
			return "field_data";
		}else if (xmlPath=="temporalOrderMemory/TOM_fa"){
			return "field_data";
		}else if (xmlPath=="temporalOrderMemory/TOM_discrim"){
			return "field_data";
		}else if (xmlPath=="WMS_III/verbalPairedAssociates/vpa_im"){
			return "field_data";
		}else if (xmlPath=="WMS_III/verbalPairedAssociates/vpa_del"){
			return "field_data";
		}else if (xmlPath=="WMS_III/verbalPairedAssociates/vpa_recog"){
			return "field_data";
		}else if (xmlPath=="WMS_III/logicalMemory/lm_im"){
			return "field_data";
		}else if (xmlPath=="WMS_III/logicalMemory/lm_del"){
			return "field_data";
		}else if (xmlPath=="WMS_III/logicalMemory/lm_recog"){
			return "field_data";
		}else if (xmlPath=="WMS_III/aud_rec_delay"){
			return "field_data";
		}else if (xmlPath=="WMS_III/lnSeq"){
			return "field_data";
		}else if (xmlPath=="digSym"){
			return "field_data";
		}else if (xmlPath=="dotComparison/dcomp1"){
			return "field_data";
		}else if (xmlPath=="dotComparison/dcomp2"){
			return "field_data";
		}else if (xmlPath=="dotComparison/dcomp3"){
			return "field_data";
		}else if (xmlPath=="dotComparison/dcompa"){
			return "field_data";
		}else if (xmlPath=="trailMaking/tma"){
			return "field_data";
		}else if (xmlPath=="trailMaking/tmb"){
			return "field_data";
		}else if (xmlPath=="buildMem"){
			return "field_data";
		}else if (xmlPath=="Stroop/strpc"){
			return "field_data";
		}else if (xmlPath=="Stroop/strpw"){
			return "field_data";
		}else if (xmlPath=="Stroop/strpcw"){
			return "field_data";
		}else if (xmlPath=="Stroop/strpint"){
			return "field_data";
		}else if (xmlPath=="pf_a"){
			return "field_data";
		}else if (xmlPath=="sf_round"){
			return "field_data";
		}else if (xmlPath=="Shipley"){
			return "field_data";
		}else if (xmlPath=="wjIII_lwi"){
			return "field_data";
		}else if (xmlPath=="WASI/wasi_vocab"){
			return "field_data";
		}else if (xmlPath=="WASI/wasi_mr"){
			return "field_data";
		}else if (xmlPath=="WASI/wasi_sim"){
			return "field_data";
		}else if (xmlPath=="WASI/wasi_bd"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<ls2:LS_Neuropsych";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</ls2:LS_Neuropsych>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		var TeststatusATT = ""
		if (this.Teststatus_complete!=null)
			TeststatusATT+=" complete=\"" + this.Teststatus_complete.replace(/>/g,"&gt;").replace(/</g,"&lt;") + "\"";
			var child0=0;
			var att0=0;
			if(this.Teststatus_complete!=null)
			att0++;
			if(this.Teststatus_tasksmissing!=null)
			child0++;
			if(child0>0 || att0>0){
				xmlTxt+="\n<ls2:testStatus";
				xmlTxt+=TeststatusATT;
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Teststatus_tasksmissing!=null){
			xmlTxt+="\n<ls2:tasksMissing";
			xmlTxt+=">";
			xmlTxt+=this.Teststatus_tasksmissing.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:tasksMissing>";
		}
				xmlTxt+="\n</ls2:testStatus>";
			}
			}

		if (this.Source!=null){
			xmlTxt+="\n<ls2:source";
			xmlTxt+=">";
			xmlTxt+=this.Source.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</ls2:source>";
		}
		if (this.Oasisdate!=null){
			xmlTxt+="\n<ls2:oasisDate";
			xmlTxt+=">";
			xmlTxt+=this.Oasisdate;
			xmlTxt+="</ls2:oasisDate>";
		}
		if (this.Mmse!=null){
			xmlTxt+="\n<ls2:MMSE";
			xmlTxt+=">";
			xmlTxt+=this.Mmse;
			xmlTxt+="</ls2:MMSE>";
		}
		if (this.Blessed!=null){
			xmlTxt+="\n<ls2:Blessed";
			xmlTxt+=">";
			xmlTxt+=this.Blessed;
			xmlTxt+="</ls2:Blessed>";
		}
			var child1=0;
			var att1=0;
			if(this.CvltIi_cRecfp!=null)
			child1++;
			if(this.CvltIi_cConsis!=null)
			child1++;
			if(this.CvltIi_cBias!=null)
			child1++;
			if(this.CvltIi_cTfri!=null)
			child1++;
			if(this.CvltIi_cPrimac!=null)
			child1++;
			if(this.CvltIi_cMiddle!=null)
			child1++;
			if(this.CvltIi_cCrd!=null)
			child1++;
			if(this.CvltIi_cTcnci!=null)
			child1++;
			if(this.CvltIi_cTali!=null)
			child1++;
			if(this.CvltIi_cRechits!=null)
			child1++;
			if(this.CvltIi_cTcri!=null)
			child1++;
			if(this.CvltIi_cRecenc!=null)
			child1++;
			if(this.CvltIi_cRetroint!=null)
			child1++;
			if(this.CvltIi_cIrd!=null)
			child1++;
			if(this.CvltIi_cSdfree!=null)
			child1++;
			if(this.CvltIi_cLdcue!=null)
			child1++;
			if(this.CvltIi_cTotal!=null)
			child1++;
			if(this.CvltIi_cTrd!=null)
			child1++;
			if(this.CvltIi_cLdfree!=null)
			child1++;
			if(this.CvltIi_cIntrus!=null)
			child1++;
			if(this.CvltIi_cTssi!=null)
			child1++;
			if(this.CvltIi_cSerclu!=null)
			child1++;
			if(this.CvltIi_cPersev!=null)
			child1++;
			if(this.CvltIi_cDrd!=null)
			child1++;
			if(this.CvltIi_cRecdisc!=null)
			child1++;
			if(this.CvltIi_cTiri!=null)
			child1++;
			if(this.CvltIi_cTa5!=null)
			child1++;
			if(this.CvltIi_cFrd!=null)
			child1++;
			if(this.CvltIi_cProint!=null)
			child1++;
			if(this.CvltIi_cListb!=null)
			child1++;
			if(this.CvltIi_cSdcue!=null)
			child1++;
			if(this.CvltIi_cTa1!=null)
			child1++;
			if(this.CvltIi_cSemclu!=null)
			child1++;
			if(this.CvltIi_cSlope!=null)
			child1++;
			if(this.CvltIi_cTdri!=null)
			child1++;
			if(child1>0 || att1>0){
				xmlTxt+="\n<ls2:CVLT_II";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.CvltIi_cTa1!=null){
			xmlTxt+="\n<ls2:c_ta1";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTa1;
			xmlTxt+="</ls2:c_ta1>";
		}
		if (this.CvltIi_cTa5!=null){
			xmlTxt+="\n<ls2:c_ta5";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTa5;
			xmlTxt+="</ls2:c_ta5>";
		}
		if (this.CvltIi_cTotal!=null){
			xmlTxt+="\n<ls2:c_total";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTotal;
			xmlTxt+="</ls2:c_total>";
		}
		if (this.CvltIi_cListb!=null){
			xmlTxt+="\n<ls2:c_listb";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cListb;
			xmlTxt+="</ls2:c_listb>";
		}
		if (this.CvltIi_cSdfree!=null){
			xmlTxt+="\n<ls2:c_sdfree";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cSdfree;
			xmlTxt+="</ls2:c_sdfree>";
		}
		if (this.CvltIi_cSdcue!=null){
			xmlTxt+="\n<ls2:c_sdcue";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cSdcue;
			xmlTxt+="</ls2:c_sdcue>";
		}
		if (this.CvltIi_cLdfree!=null){
			xmlTxt+="\n<ls2:c_ldfree";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cLdfree;
			xmlTxt+="</ls2:c_ldfree>";
		}
		if (this.CvltIi_cLdcue!=null){
			xmlTxt+="\n<ls2:c_ldcue";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cLdcue;
			xmlTxt+="</ls2:c_ldcue>";
		}
		if (this.CvltIi_cSemclu!=null){
			xmlTxt+="\n<ls2:c_semclu";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cSemclu;
			xmlTxt+="</ls2:c_semclu>";
		}
		if (this.CvltIi_cSerclu!=null){
			xmlTxt+="\n<ls2:c_serclu";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cSerclu;
			xmlTxt+="</ls2:c_serclu>";
		}
		if (this.CvltIi_cPrimac!=null){
			xmlTxt+="\n<ls2:c_primac";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cPrimac;
			xmlTxt+="</ls2:c_primac>";
		}
		if (this.CvltIi_cMiddle!=null){
			xmlTxt+="\n<ls2:c_middle";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cMiddle;
			xmlTxt+="</ls2:c_middle>";
		}
		if (this.CvltIi_cRecenc!=null){
			xmlTxt+="\n<ls2:c_recenc";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cRecenc;
			xmlTxt+="</ls2:c_recenc>";
		}
		if (this.CvltIi_cSlope!=null){
			xmlTxt+="\n<ls2:c_slope";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cSlope;
			xmlTxt+="</ls2:c_slope>";
		}
		if (this.CvltIi_cConsis!=null){
			xmlTxt+="\n<ls2:c_consis";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cConsis;
			xmlTxt+="</ls2:c_consis>";
		}
		if (this.CvltIi_cProint!=null){
			xmlTxt+="\n<ls2:c_proint";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cProint;
			xmlTxt+="</ls2:c_proint>";
		}
		if (this.CvltIi_cRetroint!=null){
			xmlTxt+="\n<ls2:c_retroint";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cRetroint;
			xmlTxt+="</ls2:c_retroint>";
		}
		if (this.CvltIi_cPersev!=null){
			xmlTxt+="\n<ls2:c_persev";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cPersev;
			xmlTxt+="</ls2:c_persev>";
		}
		if (this.CvltIi_cIntrus!=null){
			xmlTxt+="\n<ls2:c_intrus";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cIntrus;
			xmlTxt+="</ls2:c_intrus>";
		}
		if (this.CvltIi_cTiri!=null){
			xmlTxt+="\n<ls2:c_tiri";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTiri;
			xmlTxt+="</ls2:c_tiri>";
		}
		if (this.CvltIi_cTdri!=null){
			xmlTxt+="\n<ls2:c_tdri";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTdri;
			xmlTxt+="</ls2:c_tdri>";
		}
		if (this.CvltIi_cTfri!=null){
			xmlTxt+="\n<ls2:c_tfri";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTfri;
			xmlTxt+="</ls2:c_tfri>";
		}
		if (this.CvltIi_cTcri!=null){
			xmlTxt+="\n<ls2:c_tcri";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTcri;
			xmlTxt+="</ls2:c_tcri>";
		}
		if (this.CvltIi_cTcnci!=null){
			xmlTxt+="\n<ls2:c_tcnci";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTcnci;
			xmlTxt+="</ls2:c_tcnci>";
		}
		if (this.CvltIi_cTssi!=null){
			xmlTxt+="\n<ls2:c_tssi";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTssi;
			xmlTxt+="</ls2:c_tssi>";
		}
		if (this.CvltIi_cTali!=null){
			xmlTxt+="\n<ls2:c_tali";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTali;
			xmlTxt+="</ls2:c_tali>";
		}
		if (this.CvltIi_cTrd!=null){
			xmlTxt+="\n<ls2:c_trd";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cTrd;
			xmlTxt+="</ls2:c_trd>";
		}
		if (this.CvltIi_cIrd!=null){
			xmlTxt+="\n<ls2:c_ird";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cIrd;
			xmlTxt+="</ls2:c_ird>";
		}
		if (this.CvltIi_cDrd!=null){
			xmlTxt+="\n<ls2:c_drd";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cDrd;
			xmlTxt+="</ls2:c_drd>";
		}
		if (this.CvltIi_cFrd!=null){
			xmlTxt+="\n<ls2:c_frd";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cFrd;
			xmlTxt+="</ls2:c_frd>";
		}
		if (this.CvltIi_cCrd!=null){
			xmlTxt+="\n<ls2:c_crd";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cCrd;
			xmlTxt+="</ls2:c_crd>";
		}
		if (this.CvltIi_cRechits!=null){
			xmlTxt+="\n<ls2:c_rechits";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cRechits;
			xmlTxt+="</ls2:c_rechits>";
		}
		if (this.CvltIi_cRecfp!=null){
			xmlTxt+="\n<ls2:c_recfp";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cRecfp;
			xmlTxt+="</ls2:c_recfp>";
		}
		if (this.CvltIi_cRecdisc!=null){
			xmlTxt+="\n<ls2:c_recdisc";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cRecdisc;
			xmlTxt+="</ls2:c_recdisc>";
		}
		if (this.CvltIi_cBias!=null){
			xmlTxt+="\n<ls2:c_bias";
			xmlTxt+=">";
			xmlTxt+=this.CvltIi_cBias;
			xmlTxt+="</ls2:c_bias>";
		}
				xmlTxt+="\n</ls2:CVLT_II>";
			}
			}

			var child2=0;
			var att2=0;
			if(this.Temporalordermemory_tomFa!=null)
			child2++;
			if(this.Temporalordermemory_tomDiscrim!=null)
			child2++;
			if(this.Temporalordermemory_tomRecall!=null)
			child2++;
			if(this.Temporalordermemory_tomHits!=null)
			child2++;
			if(child2>0 || att2>0){
				xmlTxt+="\n<ls2:temporalOrderMemory";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Temporalordermemory_tomRecall!=null){
			xmlTxt+="\n<ls2:TOM_recall";
			xmlTxt+=">";
			xmlTxt+=this.Temporalordermemory_tomRecall;
			xmlTxt+="</ls2:TOM_recall>";
		}
		if (this.Temporalordermemory_tomHits!=null){
			xmlTxt+="\n<ls2:TOM_hits";
			xmlTxt+=">";
			xmlTxt+=this.Temporalordermemory_tomHits;
			xmlTxt+="</ls2:TOM_hits>";
		}
		if (this.Temporalordermemory_tomFa!=null){
			xmlTxt+="\n<ls2:TOM_fa";
			xmlTxt+=">";
			xmlTxt+=this.Temporalordermemory_tomFa;
			xmlTxt+="</ls2:TOM_fa>";
		}
		if (this.Temporalordermemory_tomDiscrim!=null){
			xmlTxt+="\n<ls2:TOM_discrim";
			xmlTxt+=">";
			xmlTxt+=this.Temporalordermemory_tomDiscrim;
			xmlTxt+="</ls2:TOM_discrim>";
		}
				xmlTxt+="\n</ls2:temporalOrderMemory>";
			}
			}

			var child3=0;
			var att3=0;
			if(this.WmsIii_audRecDelay!=null)
			child3++;
			if(this.WmsIii_verbalpairedassociates_vpaIm!=null)
			child3++;
			if(this.WmsIii_lnseq!=null)
			child3++;
			if(this.WmsIii_logicalmemory_lmIm!=null)
			child3++;
			if(this.WmsIii_logicalmemory_lmRecog!=null)
			child3++;
			if(this.WmsIii_logicalmemory_lmDel!=null)
			child3++;
			if(this.WmsIii_verbalpairedassociates_vpaRecog!=null)
			child3++;
			if(this.WmsIii_verbalpairedassociates_vpaDel!=null)
			child3++;
			if(child3>0 || att3>0){
				xmlTxt+="\n<ls2:WMS_III";
			if(child3==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
			var child4=0;
			var att4=0;
			if(this.WmsIii_verbalpairedassociates_vpaIm!=null)
			child4++;
			if(this.WmsIii_verbalpairedassociates_vpaRecog!=null)
			child4++;
			if(this.WmsIii_verbalpairedassociates_vpaDel!=null)
			child4++;
			if(child4>0 || att4>0){
				xmlTxt+="\n<ls2:verbalPairedAssociates";
			if(child4==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.WmsIii_verbalpairedassociates_vpaIm!=null){
			xmlTxt+="\n<ls2:vpa_im";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_verbalpairedassociates_vpaIm;
			xmlTxt+="</ls2:vpa_im>";
		}
		if (this.WmsIii_verbalpairedassociates_vpaDel!=null){
			xmlTxt+="\n<ls2:vpa_del";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_verbalpairedassociates_vpaDel;
			xmlTxt+="</ls2:vpa_del>";
		}
		if (this.WmsIii_verbalpairedassociates_vpaRecog!=null){
			xmlTxt+="\n<ls2:vpa_recog";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_verbalpairedassociates_vpaRecog;
			xmlTxt+="</ls2:vpa_recog>";
		}
				xmlTxt+="\n</ls2:verbalPairedAssociates>";
			}
			}

			var child5=0;
			var att5=0;
			if(this.WmsIii_logicalmemory_lmRecog!=null)
			child5++;
			if(this.WmsIii_logicalmemory_lmIm!=null)
			child5++;
			if(this.WmsIii_logicalmemory_lmDel!=null)
			child5++;
			if(child5>0 || att5>0){
				xmlTxt+="\n<ls2:logicalMemory";
			if(child5==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.WmsIii_logicalmemory_lmIm!=null){
			xmlTxt+="\n<ls2:lm_im";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_logicalmemory_lmIm;
			xmlTxt+="</ls2:lm_im>";
		}
		if (this.WmsIii_logicalmemory_lmDel!=null){
			xmlTxt+="\n<ls2:lm_del";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_logicalmemory_lmDel;
			xmlTxt+="</ls2:lm_del>";
		}
		if (this.WmsIii_logicalmemory_lmRecog!=null){
			xmlTxt+="\n<ls2:lm_recog";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_logicalmemory_lmRecog;
			xmlTxt+="</ls2:lm_recog>";
		}
				xmlTxt+="\n</ls2:logicalMemory>";
			}
			}

		if (this.WmsIii_audRecDelay!=null){
			xmlTxt+="\n<ls2:aud_rec_delay";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_audRecDelay;
			xmlTxt+="</ls2:aud_rec_delay>";
		}
		if (this.WmsIii_lnseq!=null){
			xmlTxt+="\n<ls2:lnSeq";
			xmlTxt+=">";
			xmlTxt+=this.WmsIii_lnseq;
			xmlTxt+="</ls2:lnSeq>";
		}
				xmlTxt+="\n</ls2:WMS_III>";
			}
			}

		if (this.Digsym!=null){
			xmlTxt+="\n<ls2:digSym";
			xmlTxt+=">";
			xmlTxt+=this.Digsym;
			xmlTxt+="</ls2:digSym>";
		}
		var DotcomparisonATT = ""
		if (this.Dotcomparison_dcompa!=null)
			DotcomparisonATT+=" dcompa=\"" + this.Dotcomparison_dcompa + "\"";
			var child6=0;
			var att6=0;
			if(this.Dotcomparison_dcompa!=null)
			att6++;
			if(this.Dotcomparison_dcomp3!=null)
			child6++;
			if(this.Dotcomparison_dcomp2!=null)
			child6++;
			if(this.Dotcomparison_dcomp1!=null)
			child6++;
			if(child6>0 || att6>0){
				xmlTxt+="\n<ls2:dotComparison";
				xmlTxt+=DotcomparisonATT;
			if(child6==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Dotcomparison_dcomp1!=null){
			xmlTxt+="\n<ls2:dcomp1";
			xmlTxt+=">";
			xmlTxt+=this.Dotcomparison_dcomp1;
			xmlTxt+="</ls2:dcomp1>";
		}
		if (this.Dotcomparison_dcomp2!=null){
			xmlTxt+="\n<ls2:dcomp2";
			xmlTxt+=">";
			xmlTxt+=this.Dotcomparison_dcomp2;
			xmlTxt+="</ls2:dcomp2>";
		}
		if (this.Dotcomparison_dcomp3!=null){
			xmlTxt+="\n<ls2:dcomp3";
			xmlTxt+=">";
			xmlTxt+=this.Dotcomparison_dcomp3;
			xmlTxt+="</ls2:dcomp3>";
		}
				xmlTxt+="\n</ls2:dotComparison>";
			}
			}

			var child7=0;
			var att7=0;
			if(this.Trailmaking_tmb!=null)
			child7++;
			if(this.Trailmaking_tma!=null)
			child7++;
			if(child7>0 || att7>0){
				xmlTxt+="\n<ls2:trailMaking";
			if(child7==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Trailmaking_tma!=null){
			xmlTxt+="\n<ls2:tma";
			xmlTxt+=">";
			xmlTxt+=this.Trailmaking_tma;
			xmlTxt+="</ls2:tma>";
		}
		if (this.Trailmaking_tmb!=null){
			xmlTxt+="\n<ls2:tmb";
			xmlTxt+=">";
			xmlTxt+=this.Trailmaking_tmb;
			xmlTxt+="</ls2:tmb>";
		}
				xmlTxt+="\n</ls2:trailMaking>";
			}
			}

		if (this.Buildmem!=null){
			xmlTxt+="\n<ls2:buildMem";
			xmlTxt+=">";
			xmlTxt+=this.Buildmem;
			xmlTxt+="</ls2:buildMem>";
		}
			var child8=0;
			var att8=0;
			if(this.Stroop_strpc!=null)
			child8++;
			if(this.Stroop_strpw!=null)
			child8++;
			if(this.Stroop_strpint!=null)
			child8++;
			if(this.Stroop_strpcw!=null)
			child8++;
			if(child8>0 || att8>0){
				xmlTxt+="\n<ls2:Stroop";
			if(child8==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Stroop_strpc!=null){
			xmlTxt+="\n<ls2:strpc";
			xmlTxt+=">";
			xmlTxt+=this.Stroop_strpc;
			xmlTxt+="</ls2:strpc>";
		}
		if (this.Stroop_strpw!=null){
			xmlTxt+="\n<ls2:strpw";
			xmlTxt+=">";
			xmlTxt+=this.Stroop_strpw;
			xmlTxt+="</ls2:strpw>";
		}
		if (this.Stroop_strpcw!=null){
			xmlTxt+="\n<ls2:strpcw";
			xmlTxt+=">";
			xmlTxt+=this.Stroop_strpcw;
			xmlTxt+="</ls2:strpcw>";
		}
		if (this.Stroop_strpint!=null){
			xmlTxt+="\n<ls2:strpint";
			xmlTxt+=">";
			xmlTxt+=this.Stroop_strpint;
			xmlTxt+="</ls2:strpint>";
		}
				xmlTxt+="\n</ls2:Stroop>";
			}
			}

		if (this.PfA!=null){
			xmlTxt+="\n<ls2:pf_a";
			xmlTxt+=">";
			xmlTxt+=this.PfA;
			xmlTxt+="</ls2:pf_a>";
		}
		if (this.SfRound!=null){
			xmlTxt+="\n<ls2:sf_round";
			xmlTxt+=">";
			xmlTxt+=this.SfRound;
			xmlTxt+="</ls2:sf_round>";
		}
		if (this.Shipley!=null){
			xmlTxt+="\n<ls2:Shipley";
			xmlTxt+=">";
			xmlTxt+=this.Shipley;
			xmlTxt+="</ls2:Shipley>";
		}
		if (this.WjiiiLwi!=null){
			xmlTxt+="\n<ls2:wjIII_lwi";
			xmlTxt+=">";
			xmlTxt+=this.WjiiiLwi;
			xmlTxt+="</ls2:wjIII_lwi>";
		}
			var child9=0;
			var att9=0;
			if(this.Wasi_wasiVocab!=null)
			child9++;
			if(this.Wasi_wasiMr!=null)
			child9++;
			if(this.Wasi_wasiSim!=null)
			child9++;
			if(this.Wasi_wasiBd!=null)
			child9++;
			if(child9>0 || att9>0){
				xmlTxt+="\n<ls2:WASI";
			if(child9==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		if (this.Wasi_wasiVocab!=null){
			xmlTxt+="\n<ls2:wasi_vocab";
			xmlTxt+=">";
			xmlTxt+=this.Wasi_wasiVocab;
			xmlTxt+="</ls2:wasi_vocab>";
		}
		if (this.Wasi_wasiMr!=null){
			xmlTxt+="\n<ls2:wasi_mr";
			xmlTxt+=">";
			xmlTxt+=this.Wasi_wasiMr;
			xmlTxt+="</ls2:wasi_mr>";
		}
		if (this.Wasi_wasiSim!=null){
			xmlTxt+="\n<ls2:wasi_sim";
			xmlTxt+=">";
			xmlTxt+=this.Wasi_wasiSim;
			xmlTxt+="</ls2:wasi_sim>";
		}
		if (this.Wasi_wasiBd!=null){
			xmlTxt+="\n<ls2:wasi_bd";
			xmlTxt+=">";
			xmlTxt+=this.Wasi_wasiBd;
			xmlTxt+="</ls2:wasi_bd>";
		}
				xmlTxt+="\n</ls2:WASI>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Teststatus_complete!=null)
			return true;
			if(this.Teststatus_complete!=null) return true;
			if(this.Teststatus_tasksmissing!=null) return true;
		if (this.Source!=null) return true;
		if (this.Oasisdate!=null) return true;
		if (this.Mmse!=null) return true;
		if (this.Blessed!=null) return true;
			if(this.CvltIi_cRecfp!=null) return true;
			if(this.CvltIi_cConsis!=null) return true;
			if(this.CvltIi_cBias!=null) return true;
			if(this.CvltIi_cTfri!=null) return true;
			if(this.CvltIi_cPrimac!=null) return true;
			if(this.CvltIi_cMiddle!=null) return true;
			if(this.CvltIi_cCrd!=null) return true;
			if(this.CvltIi_cTcnci!=null) return true;
			if(this.CvltIi_cTali!=null) return true;
			if(this.CvltIi_cRechits!=null) return true;
			if(this.CvltIi_cTcri!=null) return true;
			if(this.CvltIi_cRecenc!=null) return true;
			if(this.CvltIi_cRetroint!=null) return true;
			if(this.CvltIi_cIrd!=null) return true;
			if(this.CvltIi_cSdfree!=null) return true;
			if(this.CvltIi_cLdcue!=null) return true;
			if(this.CvltIi_cTotal!=null) return true;
			if(this.CvltIi_cTrd!=null) return true;
			if(this.CvltIi_cLdfree!=null) return true;
			if(this.CvltIi_cIntrus!=null) return true;
			if(this.CvltIi_cTssi!=null) return true;
			if(this.CvltIi_cSerclu!=null) return true;
			if(this.CvltIi_cPersev!=null) return true;
			if(this.CvltIi_cDrd!=null) return true;
			if(this.CvltIi_cRecdisc!=null) return true;
			if(this.CvltIi_cTiri!=null) return true;
			if(this.CvltIi_cTa5!=null) return true;
			if(this.CvltIi_cFrd!=null) return true;
			if(this.CvltIi_cProint!=null) return true;
			if(this.CvltIi_cListb!=null) return true;
			if(this.CvltIi_cSdcue!=null) return true;
			if(this.CvltIi_cTa1!=null) return true;
			if(this.CvltIi_cSemclu!=null) return true;
			if(this.CvltIi_cSlope!=null) return true;
			if(this.CvltIi_cTdri!=null) return true;
			if(this.Temporalordermemory_tomFa!=null) return true;
			if(this.Temporalordermemory_tomDiscrim!=null) return true;
			if(this.Temporalordermemory_tomRecall!=null) return true;
			if(this.Temporalordermemory_tomHits!=null) return true;
			if(this.WmsIii_audRecDelay!=null) return true;
			if(this.WmsIii_verbalpairedassociates_vpaIm!=null) return true;
			if(this.WmsIii_lnseq!=null) return true;
			if(this.WmsIii_logicalmemory_lmIm!=null) return true;
			if(this.WmsIii_logicalmemory_lmRecog!=null) return true;
			if(this.WmsIii_logicalmemory_lmDel!=null) return true;
			if(this.WmsIii_verbalpairedassociates_vpaRecog!=null) return true;
			if(this.WmsIii_verbalpairedassociates_vpaDel!=null) return true;
		if (this.Digsym!=null) return true;
		if (this.Dotcomparison_dcompa!=null)
			return true;
			if(this.Dotcomparison_dcompa!=null) return true;
			if(this.Dotcomparison_dcomp3!=null) return true;
			if(this.Dotcomparison_dcomp2!=null) return true;
			if(this.Dotcomparison_dcomp1!=null) return true;
			if(this.Trailmaking_tmb!=null) return true;
			if(this.Trailmaking_tma!=null) return true;
		if (this.Buildmem!=null) return true;
			if(this.Stroop_strpc!=null) return true;
			if(this.Stroop_strpw!=null) return true;
			if(this.Stroop_strpint!=null) return true;
			if(this.Stroop_strpcw!=null) return true;
		if (this.PfA!=null) return true;
		if (this.SfRound!=null) return true;
		if (this.Shipley!=null) return true;
		if (this.WjiiiLwi!=null) return true;
			if(this.Wasi_wasiVocab!=null) return true;
			if(this.Wasi_wasiMr!=null) return true;
			if(this.Wasi_wasiSim!=null) return true;
			if(this.Wasi_wasiBd!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
