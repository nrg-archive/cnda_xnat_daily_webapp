/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:01 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function sf_deficits(){
this.xsiType="sf:deficits";

	this.getSchemaElementName=function(){
		return "deficits";
	}

	this.getFullSchemaElementName=function(){
		return "sf:deficits";
	}

	this.Motorweakness=null;


	function getMotorweakness() {
		return this.Motorweakness;
	}
	this.getMotorweakness=getMotorweakness;


	function setMotorweakness(v){
		this.Motorweakness=v;
	}
	this.setMotorweakness=setMotorweakness;


	this.isMotorweakness=function(defaultValue) {
		if(this.Motorweakness==null)return defaultValue;
		if(this.Motorweakness=="1" || this.Motorweakness==true)return true;
		return false;
	}

	this.Sensorychange=null;


	function getSensorychange() {
		return this.Sensorychange;
	}
	this.getSensorychange=getSensorychange;


	function setSensorychange(v){
		this.Sensorychange=v;
	}
	this.setSensorychange=setSensorychange;


	this.isSensorychange=function(defaultValue) {
		if(this.Sensorychange==null)return defaultValue;
		if(this.Sensorychange=="1" || this.Sensorychange==true)return true;
		return false;
	}

	this.Severeheadache=null;


	function getSevereheadache() {
		return this.Severeheadache;
	}
	this.getSevereheadache=getSevereheadache;


	function setSevereheadache(v){
		this.Severeheadache=v;
	}
	this.setSevereheadache=setSevereheadache;


	this.isSevereheadache=function(defaultValue) {
		if(this.Severeheadache==null)return defaultValue;
		if(this.Severeheadache=="1" || this.Severeheadache==true)return true;
		return false;
	}

	this.Nausea=null;


	function getNausea() {
		return this.Nausea;
	}
	this.getNausea=getNausea;


	function setNausea(v){
		this.Nausea=v;
	}
	this.setNausea=setNausea;


	this.isNausea=function(defaultValue) {
		if(this.Nausea==null)return defaultValue;
		if(this.Nausea=="1" || this.Nausea==true)return true;
		return false;
	}

	this.Visualloss=null;


	function getVisualloss() {
		return this.Visualloss;
	}
	this.getVisualloss=getVisualloss;


	function setVisualloss(v){
		this.Visualloss=v;
	}
	this.setVisualloss=setVisualloss;


	this.isVisualloss=function(defaultValue) {
		if(this.Visualloss==null)return defaultValue;
		if(this.Visualloss=="1" || this.Visualloss==true)return true;
		return false;
	}

	this.Cognitivedecline=null;


	function getCognitivedecline() {
		return this.Cognitivedecline;
	}
	this.getCognitivedecline=getCognitivedecline;


	function setCognitivedecline(v){
		this.Cognitivedecline=v;
	}
	this.setCognitivedecline=setCognitivedecline;


	this.isCognitivedecline=function(defaultValue) {
		if(this.Cognitivedecline==null)return defaultValue;
		if(this.Cognitivedecline=="1" || this.Cognitivedecline==true)return true;
		return false;
	}

	this.Seizure=null;


	function getSeizure() {
		return this.Seizure;
	}
	this.getSeizure=getSeizure;


	function setSeizure(v){
		this.Seizure=v;
	}
	this.setSeizure=setSeizure;


	this.isSeizure=function(defaultValue) {
		if(this.Seizure==null)return defaultValue;
		if(this.Seizure=="1" || this.Seizure==true)return true;
		return false;
	}
	this.Otherdeficits_otherdeficit =new Array();

	function getOtherdeficits_otherdeficit() {
		return this.Otherdeficits_otherdeficit;
	}
	this.getOtherdeficits_otherdeficit=getOtherdeficits_otherdeficit;


	function addOtherdeficits_otherdeficit(v){
		this.Otherdeficits_otherdeficit.push(v);
	}
	this.addOtherdeficits_otherdeficit=addOtherdeficits_otherdeficit;

	this.SfDeficitsId=null;


	function getSfDeficitsId() {
		return this.SfDeficitsId;
	}
	this.getSfDeficitsId=getSfDeficitsId;


	function setSfDeficitsId(v){
		this.SfDeficitsId=v;
	}
	this.setSfDeficitsId=setSfDeficitsId;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="motorWeakness"){
				return this.Motorweakness ;
			} else 
			if(xmlPath=="sensoryChange"){
				return this.Sensorychange ;
			} else 
			if(xmlPath=="severeHeadache"){
				return this.Severeheadache ;
			} else 
			if(xmlPath=="nausea"){
				return this.Nausea ;
			} else 
			if(xmlPath=="visualLoss"){
				return this.Visualloss ;
			} else 
			if(xmlPath=="cognitiveDecline"){
				return this.Cognitivedecline ;
			} else 
			if(xmlPath=="seizure"){
				return this.Seizure ;
			} else 
			if(xmlPath=="otherDeficits/otherDeficit"){
				return this.Otherdeficits_otherdeficit ;
			} else 
			if(xmlPath.startsWith("otherDeficits/otherDeficit")){
				xmlPath=xmlPath.substring(26);
				if(xmlPath=="")return this.Otherdeficits_otherdeficit ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Otherdeficits_otherdeficit.length;whereCount++){

					var tempValue=this.Otherdeficits_otherdeficit[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Otherdeficits_otherdeficit[whereCount]);

					}

				}
				}else{

				whereArray=this.Otherdeficits_otherdeficit;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="sf_deficits_id"){
				return this.SfDeficitsId ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="motorWeakness"){
				this.Motorweakness=value;
			} else 
			if(xmlPath=="sensoryChange"){
				this.Sensorychange=value;
			} else 
			if(xmlPath=="severeHeadache"){
				this.Severeheadache=value;
			} else 
			if(xmlPath=="nausea"){
				this.Nausea=value;
			} else 
			if(xmlPath=="visualLoss"){
				this.Visualloss=value;
			} else 
			if(xmlPath=="cognitiveDecline"){
				this.Cognitivedecline=value;
			} else 
			if(xmlPath=="seizure"){
				this.Seizure=value;
			} else 
			if(xmlPath=="otherDeficits/otherDeficit"){
				this.Otherdeficits_otherdeficit=value;
			} else 
			if(xmlPath.startsWith("otherDeficits/otherDeficit")){
				xmlPath=xmlPath.substring(26);
				if(xmlPath=="")return this.Otherdeficits_otherdeficit ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Otherdeficits_otherdeficit.length;whereCount++){

					var tempValue=this.Otherdeficits_otherdeficit[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Otherdeficits_otherdeficit[whereCount]);

					}

				}
				}else{

				whereArray=this.Otherdeficits_otherdeficit;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("sf:deficits_otherDeficit");//omUtils.js
					}
					this.addOtherdeficits_otherdeficit(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="sf_deficits_id"){
				this.SfDeficitsId=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="otherDeficits/otherDeficit"){
			this.addOtherdeficits_otherdeficit(v);
		}
		else{
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="otherDeficits/otherDeficit"){
			return "http://nrg.wustl.edu/sf:deficits_otherDeficit";
		}
		else{
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="motorWeakness"){
			return "field_data";
		}else if (xmlPath=="sensoryChange"){
			return "field_data";
		}else if (xmlPath=="severeHeadache"){
			return "field_data";
		}else if (xmlPath=="nausea"){
			return "field_data";
		}else if (xmlPath=="visualLoss"){
			return "field_data";
		}else if (xmlPath=="cognitiveDecline"){
			return "field_data";
		}else if (xmlPath=="seizure"){
			return "field_data";
		}else if (xmlPath=="otherDeficits/otherDeficit"){
			return "field_multi_reference";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<sf:deficits";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</sf:deficits>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.SfDeficitsId!=null){
				if(hiddenCount++>0)str+=",";
				str+="sf_deficits_id=\"" + this.SfDeficitsId + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Motorweakness!=null){
			xmlTxt+="\n<sf:motorWeakness";
			xmlTxt+=">";
			xmlTxt+=this.Motorweakness;
			xmlTxt+="</sf:motorWeakness>";
		}
		if (this.Sensorychange!=null){
			xmlTxt+="\n<sf:sensoryChange";
			xmlTxt+=">";
			xmlTxt+=this.Sensorychange;
			xmlTxt+="</sf:sensoryChange>";
		}
		if (this.Severeheadache!=null){
			xmlTxt+="\n<sf:severeHeadache";
			xmlTxt+=">";
			xmlTxt+=this.Severeheadache;
			xmlTxt+="</sf:severeHeadache>";
		}
		if (this.Nausea!=null){
			xmlTxt+="\n<sf:nausea";
			xmlTxt+=">";
			xmlTxt+=this.Nausea;
			xmlTxt+="</sf:nausea>";
		}
		if (this.Visualloss!=null){
			xmlTxt+="\n<sf:visualLoss";
			xmlTxt+=">";
			xmlTxt+=this.Visualloss;
			xmlTxt+="</sf:visualLoss>";
		}
		if (this.Cognitivedecline!=null){
			xmlTxt+="\n<sf:cognitiveDecline";
			xmlTxt+=">";
			xmlTxt+=this.Cognitivedecline;
			xmlTxt+="</sf:cognitiveDecline>";
		}
		if (this.Seizure!=null){
			xmlTxt+="\n<sf:seizure";
			xmlTxt+=">";
			xmlTxt+=this.Seizure;
			xmlTxt+="</sf:seizure>";
		}
			var child0=0;
			var att0=0;
			child0+=this.Otherdeficits_otherdeficit.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<sf:otherDeficits";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Otherdeficits_otherdeficitCOUNT=0;Otherdeficits_otherdeficitCOUNT<this.Otherdeficits_otherdeficit.length;Otherdeficits_otherdeficitCOUNT++){
			xmlTxt +="\n<sf:otherDeficit";
			xmlTxt +=this.Otherdeficits_otherdeficit[Otherdeficits_otherdeficitCOUNT].getXMLAtts();
			if(this.Otherdeficits_otherdeficit[Otherdeficits_otherdeficitCOUNT].xsiType!="sf:deficits_otherDeficit"){
				xmlTxt+=" xsi:type=\"" + this.Otherdeficits_otherdeficit[Otherdeficits_otherdeficitCOUNT].xsiType + "\"";
			}
			if (this.Otherdeficits_otherdeficit[Otherdeficits_otherdeficitCOUNT].hasXMLBodyContent()){
				xmlTxt+=">";
				xmlTxt+=this.Otherdeficits_otherdeficit[Otherdeficits_otherdeficitCOUNT].getXMLBody(preventComments);
					xmlTxt+="</sf:otherDeficit>";
			}else {xmlTxt+="/>";}
		}
				xmlTxt+="\n</sf:otherDeficits>";
			}
			}

		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.SfDeficitsId!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Motorweakness!=null) return true;
		if (this.Sensorychange!=null) return true;
		if (this.Severeheadache!=null) return true;
		if (this.Nausea!=null) return true;
		if (this.Visualloss!=null) return true;
		if (this.Cognitivedecline!=null) return true;
		if (this.Seizure!=null) return true;
			if(this.Otherdeficits_otherdeficit.length>0)return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
