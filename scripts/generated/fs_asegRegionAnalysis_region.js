/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:02 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function fs_asegRegionAnalysis_region(){
this.xsiType="fs:asegRegionAnalysis_region";

	this.getSchemaElementName=function(){
		return "asegRegionAnalysis_region";
	}

	this.getFullSchemaElementName=function(){
		return "fs:asegRegionAnalysis_region";
	}

	this.Nvoxels=null;


	function getNvoxels() {
		return this.Nvoxels;
	}
	this.getNvoxels=getNvoxels;


	function setNvoxels(v){
		this.Nvoxels=v;
	}
	this.setNvoxels=setNvoxels;

	this.Volume=null;


	function getVolume() {
		return this.Volume;
	}
	this.getVolume=getVolume;


	function setVolume(v){
		this.Volume=v;
	}
	this.setVolume=setVolume;

	this.Normmean=null;


	function getNormmean() {
		return this.Normmean;
	}
	this.getNormmean=getNormmean;


	function setNormmean(v){
		this.Normmean=v;
	}
	this.setNormmean=setNormmean;

	this.Normstddev=null;


	function getNormstddev() {
		return this.Normstddev;
	}
	this.getNormstddev=getNormstddev;


	function setNormstddev(v){
		this.Normstddev=v;
	}
	this.setNormstddev=setNormstddev;

	this.Normmin=null;


	function getNormmin() {
		return this.Normmin;
	}
	this.getNormmin=getNormmin;


	function setNormmin(v){
		this.Normmin=v;
	}
	this.setNormmin=setNormmin;

	this.Normmax=null;


	function getNormmax() {
		return this.Normmax;
	}
	this.getNormmax=getNormmax;


	function setNormmax(v){
		this.Normmax=v;
	}
	this.setNormmax=setNormmax;

	this.Normrange=null;


	function getNormrange() {
		return this.Normrange;
	}
	this.getNormrange=getNormrange;


	function setNormrange(v){
		this.Normrange=v;
	}
	this.setNormrange=setNormrange;

	this.Segid=null;


	function getSegid() {
		return this.Segid;
	}
	this.getSegid=getSegid;


	function setSegid(v){
		this.Segid=v;
	}
	this.setSegid=setSegid;

	this.Name=null;


	function getName() {
		return this.Name;
	}
	this.getName=getName;


	function setName(v){
		this.Name=v;
	}
	this.setName=setName;

	this.Hemisphere=null;


	function getHemisphere() {
		return this.Hemisphere;
	}
	this.getHemisphere=getHemisphere;


	function setHemisphere(v){
		this.Hemisphere=v;
	}
	this.setHemisphere=setHemisphere;

	this.FsAsegregionanalysisRegionId=null;


	function getFsAsegregionanalysisRegionId() {
		return this.FsAsegregionanalysisRegionId;
	}
	this.getFsAsegregionanalysisRegionId=getFsAsegregionanalysisRegionId;


	function setFsAsegregionanalysisRegionId(v){
		this.FsAsegregionanalysisRegionId=v;
	}
	this.setFsAsegregionanalysisRegionId=setFsAsegregionanalysisRegionId;

	this.regions_region_fs_asegRegionAna_id_fk=null;


	this.getregions_region_fs_asegRegionAna_id=function() {
		return this.regions_region_fs_asegRegionAna_id_fk;
	}


	this.setregions_region_fs_asegRegionAna_id=function(v){
		this.regions_region_fs_asegRegionAna_id_fk=v;
	}


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NVoxels"){
				return this.Nvoxels ;
			} else 
			if(xmlPath=="Volume"){
				return this.Volume ;
			} else 
			if(xmlPath=="normMean"){
				return this.Normmean ;
			} else 
			if(xmlPath=="normStdDev"){
				return this.Normstddev ;
			} else 
			if(xmlPath=="normMin"){
				return this.Normmin ;
			} else 
			if(xmlPath=="normMax"){
				return this.Normmax ;
			} else 
			if(xmlPath=="normRange"){
				return this.Normrange ;
			} else 
			if(xmlPath=="SegId"){
				return this.Segid ;
			} else 
			if(xmlPath=="name"){
				return this.Name ;
			} else 
			if(xmlPath=="hemisphere"){
				return this.Hemisphere ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			if(xmlPath=="fs_asegRegionAnalysis_region_id"){
				return this.FsAsegregionanalysisRegionId ;
			} else 
			if(xmlPath=="regions_region_fs_asegRegionAna_id"){
				return this.regions_region_fs_asegRegionAna_id_fk ;
			} else 
			{
				return null;
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="NVoxels"){
				this.Nvoxels=value;
			} else 
			if(xmlPath=="Volume"){
				this.Volume=value;
			} else 
			if(xmlPath=="normMean"){
				this.Normmean=value;
			} else 
			if(xmlPath=="normStdDev"){
				this.Normstddev=value;
			} else 
			if(xmlPath=="normMin"){
				this.Normmin=value;
			} else 
			if(xmlPath=="normMax"){
				this.Normmax=value;
			} else 
			if(xmlPath=="normRange"){
				this.Normrange=value;
			} else 
			if(xmlPath=="SegId"){
				this.Segid=value;
			} else 
			if(xmlPath=="name"){
				this.Name=value;
			} else 
			if(xmlPath=="hemisphere"){
				this.Hemisphere=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			if(xmlPath=="fs_asegRegionAnalysis_region_id"){
				this.FsAsegregionanalysisRegionId=value;
			} else 
			if(xmlPath=="regions_region_fs_asegRegionAna_id"){
				this.regions_region_fs_asegRegionAna_id_fk=value;
			} else 
			{
				return null;
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="NVoxels"){
			return "field_data";
		}else if (xmlPath=="Volume"){
			return "field_data";
		}else if (xmlPath=="normMean"){
			return "field_data";
		}else if (xmlPath=="normStdDev"){
			return "field_data";
		}else if (xmlPath=="normMin"){
			return "field_data";
		}else if (xmlPath=="normMax"){
			return "field_data";
		}else if (xmlPath=="normRange"){
			return "field_data";
		}else if (xmlPath=="SegId"){
			return "field_data";
		}else if (xmlPath=="name"){
			return "field_data";
		}else if (xmlPath=="hemisphere"){
			return "field_data";
		}
		else{
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<fs:asegRegionAnalysis_region";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</fs:asegRegionAnalysis_region>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		str += "<!--hidden_fields[";
		var hiddenCount = 0;
			if(this.FsAsegregionanalysisRegionId!=null){
				if(hiddenCount++>0)str+=",";
				str+="fs_asegRegionAnalysis_region_id=\"" + this.FsAsegregionanalysisRegionId + "\"";
			}
			if(this.regions_region_fs_asegRegionAna_id_fk!=null){
				if(hiddenCount++>0)str+=",";
				str+="regions_region_fs_asegRegionAna_id=\"" + this.regions_region_fs_asegRegionAna_id_fk + "\"";
			}
		str +="]-->";
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = "";
		if (this.Segid!=null)
			attTxt+=" SegId=\"" +this.Segid +"\"";
		//NOT REQUIRED FIELD

		if (this.Name!=null)
			attTxt+=" name=\"" +this.Name +"\"";
		else attTxt+=" name=\"\"";//REQUIRED FIELD

		if (this.Hemisphere!=null)
			attTxt+=" hemisphere=\"" +this.Hemisphere +"\"";
		//NOT REQUIRED FIELD

		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		if (this.Nvoxels!=null){
			xmlTxt+="\n<fs:NVoxels";
			xmlTxt+=">";
			xmlTxt+=this.Nvoxels;
			xmlTxt+="</fs:NVoxels>";
		}
		if (this.Volume!=null){
			xmlTxt+="\n<fs:Volume";
			xmlTxt+=">";
			xmlTxt+=this.Volume;
			xmlTxt+="</fs:Volume>";
		}
		if (this.Normmean!=null){
			xmlTxt+="\n<fs:normMean";
			xmlTxt+=">";
			xmlTxt+=this.Normmean;
			xmlTxt+="</fs:normMean>";
		}
		if (this.Normstddev!=null){
			xmlTxt+="\n<fs:normStdDev";
			xmlTxt+=">";
			xmlTxt+=this.Normstddev;
			xmlTxt+="</fs:normStdDev>";
		}
		if (this.Normmin!=null){
			xmlTxt+="\n<fs:normMin";
			xmlTxt+=">";
			xmlTxt+=this.Normmin;
			xmlTxt+="</fs:normMin>";
		}
		if (this.Normmax!=null){
			xmlTxt+="\n<fs:normMax";
			xmlTxt+=">";
			xmlTxt+=this.Normmax;
			xmlTxt+="</fs:normMax>";
		}
		if (this.Normrange!=null){
			xmlTxt+="\n<fs:normRange";
			xmlTxt+=">";
			xmlTxt+=this.Normrange;
			xmlTxt+="</fs:normRange>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
			if (this.FsAsegregionanalysisRegionId!=null) return true;
			if (this.regions_region_fs_asegRegionAna_id_fk!=null) return true;
			return false;
	}


	this.hasXMLBodyContent=function(){
		if (this.Nvoxels!=null) return true;
		if (this.Volume!=null) return true;
		if (this.Normmean!=null) return true;
		if (this.Normstddev!=null) return true;
		if (this.Normmin!=null) return true;
		if (this.Normmax!=null) return true;
		if (this.Normrange!=null) return true;
		if(this.hasXMLComments())return true;
		return false;
	}
}
