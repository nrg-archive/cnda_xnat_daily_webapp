/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:02 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function tx_baseTreatment(){
this.xsiType="tx:baseTreatment";

	this.getSchemaElementName=function(){
		return "baseTreatment";
	}

	this.getFullSchemaElementName=function(){
		return "tx:baseTreatment";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Type=null;


	function getType() {
		return this.Type;
	}
	this.getType=getType;


	function setType(v){
		this.Type=v;
	}
	this.setType=setType;

	this.Unit=null;


	function getUnit() {
		return this.Unit;
	}
	this.getUnit=getUnit;


	function setUnit(v){
		this.Unit=v;
	}
	this.setUnit=setUnit;

	this.Dose=null;


	function getDose() {
		return this.Dose;
	}
	this.getDose=getDose;


	function setDose(v){
		this.Dose=v;
	}
	this.setDose=setDose;

	this.Timing=null;


	function getTiming() {
		return this.Timing;
	}
	this.getTiming=getTiming;


	function setTiming(v){
		this.Timing=v;
	}
	this.setTiming=setTiming;

	this.Startdate=null;


	function getStartdate() {
		return this.Startdate;
	}
	this.getStartdate=getStartdate;


	function setStartdate(v){
		this.Startdate=v;
	}
	this.setStartdate=setStartdate;

	this.Startdatedaynotreported=null;


	function getStartdatedaynotreported() {
		return this.Startdatedaynotreported;
	}
	this.getStartdatedaynotreported=getStartdatedaynotreported;


	function setStartdatedaynotreported(v){
		this.Startdatedaynotreported=v;
	}
	this.setStartdatedaynotreported=setStartdatedaynotreported;


	this.isStartdatedaynotreported=function(defaultValue) {
		if(this.Startdatedaynotreported==null)return defaultValue;
		if(this.Startdatedaynotreported=="1" || this.Startdatedaynotreported==true)return true;
		return false;
	}

	this.Startdatemonthnotreported=null;


	function getStartdatemonthnotreported() {
		return this.Startdatemonthnotreported;
	}
	this.getStartdatemonthnotreported=getStartdatemonthnotreported;


	function setStartdatemonthnotreported(v){
		this.Startdatemonthnotreported=v;
	}
	this.setStartdatemonthnotreported=setStartdatemonthnotreported;


	this.isStartdatemonthnotreported=function(defaultValue) {
		if(this.Startdatemonthnotreported==null)return defaultValue;
		if(this.Startdatemonthnotreported=="1" || this.Startdatemonthnotreported==true)return true;
		return false;
	}

	this.Startdateyearnotreported=null;


	function getStartdateyearnotreported() {
		return this.Startdateyearnotreported;
	}
	this.getStartdateyearnotreported=getStartdateyearnotreported;


	function setStartdateyearnotreported(v){
		this.Startdateyearnotreported=v;
	}
	this.setStartdateyearnotreported=setStartdateyearnotreported;


	this.isStartdateyearnotreported=function(defaultValue) {
		if(this.Startdateyearnotreported==null)return defaultValue;
		if(this.Startdateyearnotreported=="1" || this.Startdateyearnotreported==true)return true;
		return false;
	}

	this.Enddate=null;


	function getEnddate() {
		return this.Enddate;
	}
	this.getEnddate=getEnddate;


	function setEnddate(v){
		this.Enddate=v;
	}
	this.setEnddate=setEnddate;

	this.Enddatedaynotreported=null;


	function getEnddatedaynotreported() {
		return this.Enddatedaynotreported;
	}
	this.getEnddatedaynotreported=getEnddatedaynotreported;


	function setEnddatedaynotreported(v){
		this.Enddatedaynotreported=v;
	}
	this.setEnddatedaynotreported=setEnddatedaynotreported;


	this.isEnddatedaynotreported=function(defaultValue) {
		if(this.Enddatedaynotreported==null)return defaultValue;
		if(this.Enddatedaynotreported=="1" || this.Enddatedaynotreported==true)return true;
		return false;
	}

	this.Enddatemonthnotreported=null;


	function getEnddatemonthnotreported() {
		return this.Enddatemonthnotreported;
	}
	this.getEnddatemonthnotreported=getEnddatemonthnotreported;


	function setEnddatemonthnotreported(v){
		this.Enddatemonthnotreported=v;
	}
	this.setEnddatemonthnotreported=setEnddatemonthnotreported;


	this.isEnddatemonthnotreported=function(defaultValue) {
		if(this.Enddatemonthnotreported==null)return defaultValue;
		if(this.Enddatemonthnotreported=="1" || this.Enddatemonthnotreported==true)return true;
		return false;
	}

	this.Enddateyearnotreported=null;


	function getEnddateyearnotreported() {
		return this.Enddateyearnotreported;
	}
	this.getEnddateyearnotreported=getEnddateyearnotreported;


	function setEnddateyearnotreported(v){
		this.Enddateyearnotreported=v;
	}
	this.setEnddateyearnotreported=setEnddateyearnotreported;


	this.isEnddateyearnotreported=function(defaultValue) {
		if(this.Enddateyearnotreported==null)return defaultValue;
		if(this.Enddateyearnotreported=="1" || this.Enddateyearnotreported==true)return true;
		return false;
	}

	this.Completed=null;


	function getCompleted() {
		return this.Completed;
	}
	this.getCompleted=getCompleted;


	function setCompleted(v){
		this.Completed=v;
	}
	this.setCompleted=setCompleted;


	this.isCompleted=function(defaultValue) {
		if(this.Completed==null)return defaultValue;
		if(this.Completed=="1" || this.Completed==true)return true;
		return false;
	}

	this.Incompletereason=null;


	function getIncompletereason() {
		return this.Incompletereason;
	}
	this.getIncompletereason=getIncompletereason;


	function setIncompletereason(v){
		this.Incompletereason=v;
	}
	this.setIncompletereason=setIncompletereason;

	this.Incompleteextent=null;


	function getIncompleteextent() {
		return this.Incompleteextent;
	}
	this.getIncompleteextent=getIncompleteextent;


	function setIncompleteextent(v){
		this.Incompleteextent=v;
	}
	this.setIncompleteextent=setIncompleteextent;

	this.Clinicaltrialname=null;


	function getClinicaltrialname() {
		return this.Clinicaltrialname;
	}
	this.getClinicaltrialname=getClinicaltrialname;


	function setClinicaltrialname(v){
		this.Clinicaltrialname=v;
	}
	this.setClinicaltrialname=setClinicaltrialname;

	this.Clinicaltrialarm=null;


	function getClinicaltrialarm() {
		return this.Clinicaltrialarm;
	}
	this.getClinicaltrialarm=getClinicaltrialarm;


	function setClinicaltrialarm(v){
		this.Clinicaltrialarm=v;
	}
	this.setClinicaltrialarm=setClinicaltrialarm;

	this.Completednotes=null;


	function getCompletednotes() {
		return this.Completednotes;
	}
	this.getCompletednotes=getCompletednotes;


	function setCompletednotes(v){
		this.Completednotes=v;
	}
	this.setCompletednotes=setCompletednotes;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="type"){
				return this.Type ;
			} else 
			if(xmlPath=="unit"){
				return this.Unit ;
			} else 
			if(xmlPath=="dose"){
				return this.Dose ;
			} else 
			if(xmlPath=="timing"){
				return this.Timing ;
			} else 
			if(xmlPath=="startDate"){
				return this.Startdate ;
			} else 
			if(xmlPath=="startDateDayNotReported"){
				return this.Startdatedaynotreported ;
			} else 
			if(xmlPath=="startDateMonthNotReported"){
				return this.Startdatemonthnotreported ;
			} else 
			if(xmlPath=="startDateYearNotReported"){
				return this.Startdateyearnotreported ;
			} else 
			if(xmlPath=="endDate"){
				return this.Enddate ;
			} else 
			if(xmlPath=="endDateDayNotReported"){
				return this.Enddatedaynotreported ;
			} else 
			if(xmlPath=="endDateMonthNotReported"){
				return this.Enddatemonthnotreported ;
			} else 
			if(xmlPath=="endDateYearNotReported"){
				return this.Enddateyearnotreported ;
			} else 
			if(xmlPath=="completed"){
				return this.Completed ;
			} else 
			if(xmlPath=="incompleteReason"){
				return this.Incompletereason ;
			} else 
			if(xmlPath=="incompleteExtent"){
				return this.Incompleteextent ;
			} else 
			if(xmlPath=="clinicalTrialName"){
				return this.Clinicaltrialname ;
			} else 
			if(xmlPath=="clinicalTrialArm"){
				return this.Clinicaltrialarm ;
			} else 
			if(xmlPath=="completedNotes"){
				return this.Completednotes ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="type"){
				this.Type=value;
			} else 
			if(xmlPath=="unit"){
				this.Unit=value;
			} else 
			if(xmlPath=="dose"){
				this.Dose=value;
			} else 
			if(xmlPath=="timing"){
				this.Timing=value;
			} else 
			if(xmlPath=="startDate"){
				this.Startdate=value;
			} else 
			if(xmlPath=="startDateDayNotReported"){
				this.Startdatedaynotreported=value;
			} else 
			if(xmlPath=="startDateMonthNotReported"){
				this.Startdatemonthnotreported=value;
			} else 
			if(xmlPath=="startDateYearNotReported"){
				this.Startdateyearnotreported=value;
			} else 
			if(xmlPath=="endDate"){
				this.Enddate=value;
			} else 
			if(xmlPath=="endDateDayNotReported"){
				this.Enddatedaynotreported=value;
			} else 
			if(xmlPath=="endDateMonthNotReported"){
				this.Enddatemonthnotreported=value;
			} else 
			if(xmlPath=="endDateYearNotReported"){
				this.Enddateyearnotreported=value;
			} else 
			if(xmlPath=="completed"){
				this.Completed=value;
			} else 
			if(xmlPath=="incompleteReason"){
				this.Incompletereason=value;
			} else 
			if(xmlPath=="incompleteExtent"){
				this.Incompleteextent=value;
			} else 
			if(xmlPath=="clinicalTrialName"){
				this.Clinicaltrialname=value;
			} else 
			if(xmlPath=="clinicalTrialArm"){
				this.Clinicaltrialarm=value;
			} else 
			if(xmlPath=="completedNotes"){
				this.Completednotes=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="type"){
			return "field_data";
		}else if (xmlPath=="unit"){
			return "field_data";
		}else if (xmlPath=="dose"){
			return "field_data";
		}else if (xmlPath=="timing"){
			return "field_data";
		}else if (xmlPath=="startDate"){
			return "field_data";
		}else if (xmlPath=="startDateDayNotReported"){
			return "field_data";
		}else if (xmlPath=="startDateMonthNotReported"){
			return "field_data";
		}else if (xmlPath=="startDateYearNotReported"){
			return "field_data";
		}else if (xmlPath=="endDate"){
			return "field_data";
		}else if (xmlPath=="endDateDayNotReported"){
			return "field_data";
		}else if (xmlPath=="endDateMonthNotReported"){
			return "field_data";
		}else if (xmlPath=="endDateYearNotReported"){
			return "field_data";
		}else if (xmlPath=="completed"){
			return "field_data";
		}else if (xmlPath=="incompleteReason"){
			return "field_data";
		}else if (xmlPath=="incompleteExtent"){
			return "field_data";
		}else if (xmlPath=="clinicalTrialName"){
			return "field_data";
		}else if (xmlPath=="clinicalTrialArm"){
			return "field_data";
		}else if (xmlPath=="completedNotes"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<tx:baseTreatment";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</tx:baseTreatment>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Type!=null){
			xmlTxt+="\n<tx:type";
			xmlTxt+=">";
			xmlTxt+=this.Type.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:type>";
		}
		if (this.Unit!=null){
			xmlTxt+="\n<tx:unit";
			xmlTxt+=">";
			xmlTxt+=this.Unit.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:unit>";
		}
		if (this.Dose!=null){
			xmlTxt+="\n<tx:dose";
			xmlTxt+=">";
			xmlTxt+=this.Dose;
			xmlTxt+="</tx:dose>";
		}
		if (this.Timing!=null){
			xmlTxt+="\n<tx:timing";
			xmlTxt+=">";
			xmlTxt+=this.Timing.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:timing>";
		}
		if (this.Startdate!=null){
			xmlTxt+="\n<tx:startDate";
			xmlTxt+=">";
			xmlTxt+=this.Startdate;
			xmlTxt+="</tx:startDate>";
		}
		if (this.Startdatedaynotreported!=null){
			xmlTxt+="\n<tx:startDateDayNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Startdatedaynotreported;
			xmlTxt+="</tx:startDateDayNotReported>";
		}
		if (this.Startdatemonthnotreported!=null){
			xmlTxt+="\n<tx:startDateMonthNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Startdatemonthnotreported;
			xmlTxt+="</tx:startDateMonthNotReported>";
		}
		if (this.Startdateyearnotreported!=null){
			xmlTxt+="\n<tx:startDateYearNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Startdateyearnotreported;
			xmlTxt+="</tx:startDateYearNotReported>";
		}
		if (this.Enddate!=null){
			xmlTxt+="\n<tx:endDate";
			xmlTxt+=">";
			xmlTxt+=this.Enddate;
			xmlTxt+="</tx:endDate>";
		}
		if (this.Enddatedaynotreported!=null){
			xmlTxt+="\n<tx:endDateDayNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Enddatedaynotreported;
			xmlTxt+="</tx:endDateDayNotReported>";
		}
		if (this.Enddatemonthnotreported!=null){
			xmlTxt+="\n<tx:endDateMonthNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Enddatemonthnotreported;
			xmlTxt+="</tx:endDateMonthNotReported>";
		}
		if (this.Enddateyearnotreported!=null){
			xmlTxt+="\n<tx:endDateYearNotReported";
			xmlTxt+=">";
			xmlTxt+=this.Enddateyearnotreported;
			xmlTxt+="</tx:endDateYearNotReported>";
		}
		if (this.Completed!=null){
			xmlTxt+="\n<tx:completed";
			xmlTxt+=">";
			xmlTxt+=this.Completed;
			xmlTxt+="</tx:completed>";
		}
		if (this.Incompletereason!=null){
			xmlTxt+="\n<tx:incompleteReason";
			xmlTxt+=">";
			xmlTxt+=this.Incompletereason.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:incompleteReason>";
		}
		if (this.Incompleteextent!=null){
			xmlTxt+="\n<tx:incompleteExtent";
			xmlTxt+=">";
			xmlTxt+=this.Incompleteextent.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:incompleteExtent>";
		}
		if (this.Clinicaltrialname!=null){
			xmlTxt+="\n<tx:clinicalTrialName";
			xmlTxt+=">";
			xmlTxt+=this.Clinicaltrialname.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:clinicalTrialName>";
		}
		if (this.Clinicaltrialarm!=null){
			xmlTxt+="\n<tx:clinicalTrialArm";
			xmlTxt+=">";
			xmlTxt+=this.Clinicaltrialarm.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:clinicalTrialArm>";
		}
		if (this.Completednotes!=null){
			xmlTxt+="\n<tx:completedNotes";
			xmlTxt+=">";
			xmlTxt+=this.Completednotes.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</tx:completedNotes>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Type!=null) return true;
		if (this.Unit!=null) return true;
		if (this.Dose!=null) return true;
		if (this.Timing!=null) return true;
		if (this.Startdate!=null) return true;
		if (this.Startdatedaynotreported!=null) return true;
		if (this.Startdatemonthnotreported!=null) return true;
		if (this.Startdateyearnotreported!=null) return true;
		if (this.Enddate!=null) return true;
		if (this.Enddatedaynotreported!=null) return true;
		if (this.Enddatemonthnotreported!=null) return true;
		if (this.Enddateyearnotreported!=null) return true;
		if (this.Completed!=null) return true;
		if (this.Incompletereason!=null) return true;
		if (this.Incompleteextent!=null) return true;
		if (this.Clinicaltrialname!=null) return true;
		if (this.Clinicaltrialarm!=null) return true;
		if (this.Completednotes!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
