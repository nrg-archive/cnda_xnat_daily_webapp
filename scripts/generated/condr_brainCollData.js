/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:02 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function condr_brainCollData(){
this.xsiType="condr:brainCollData";

	this.getSchemaElementName=function(){
		return "brainCollData";
	}

	this.getFullSchemaElementName=function(){
		return "condr:brainCollData";
	}
this.extension=dynamicJSLoad('tissue_tissCollData','generated/tissue_tissCollData.js');
	this.Tumorlocs_tumorloc =new Array();

	function getTumorlocs_tumorloc() {
		return this.Tumorlocs_tumorloc;
	}
	this.getTumorlocs_tumorloc=getTumorlocs_tumorloc;


	function addTumorlocs_tumorloc(v){
		this.Tumorlocs_tumorloc.push(v);
	}
	this.addTumorlocs_tumorloc=addTumorlocs_tumorloc;

	this.Tumorhemisphere=null;


	function getTumorhemisphere() {
		return this.Tumorhemisphere;
	}
	this.getTumorhemisphere=getTumorhemisphere;


	function setTumorhemisphere(v){
		this.Tumorhemisphere=v;
	}
	this.setTumorhemisphere=setTumorhemisphere;
	this.Surgicalexts_surgicalext =new Array();

	function getSurgicalexts_surgicalext() {
		return this.Surgicalexts_surgicalext;
	}
	this.getSurgicalexts_surgicalext=getSurgicalexts_surgicalext;


	function addSurgicalexts_surgicalext(v){
		this.Surgicalexts_surgicalext.push(v);
	}
	this.addSurgicalexts_surgicalext=addSurgicalexts_surgicalext;

	this.Gliadelused=null;


	function getGliadelused() {
		return this.Gliadelused;
	}
	this.getGliadelused=getGliadelused;


	function setGliadelused(v){
		this.Gliadelused=v;
	}
	this.setGliadelused=setGliadelused;


	this.isGliadelused=function(defaultValue) {
		if(this.Gliadelused==null)return defaultValue;
		if(this.Gliadelused=="1" || this.Gliadelused==true)return true;
		return false;
	}
	this.Surgeons_surgeon =new Array();

	function getSurgeons_surgeon() {
		return this.Surgeons_surgeon;
	}
	this.getSurgeons_surgeon=getSurgeons_surgeon;


	function addSurgeons_surgeon(v){
		this.Surgeons_surgeon.push(v);
	}
	this.addSurgeons_surgeon=addSurgeons_surgeon;

	this.Bloodcoll=null;


	function getBloodcoll() {
		return this.Bloodcoll;
	}
	this.getBloodcoll=getBloodcoll;


	function setBloodcoll(v){
		this.Bloodcoll=v;
	}
	this.setBloodcoll=setBloodcoll;


	this.isBloodcoll=function(defaultValue) {
		if(this.Bloodcoll==null)return defaultValue;
		if(this.Bloodcoll=="1" || this.Bloodcoll==true)return true;
		return false;
	}

	this.Redtop=null;


	function getRedtop() {
		return this.Redtop;
	}
	this.getRedtop=getRedtop;


	function setRedtop(v){
		this.Redtop=v;
	}
	this.setRedtop=setRedtop;


	this.isRedtop=function(defaultValue) {
		if(this.Redtop==null)return defaultValue;
		if(this.Redtop=="1" || this.Redtop==true)return true;
		return false;
	}

	this.Purpletop=null;


	function getPurpletop() {
		return this.Purpletop;
	}
	this.getPurpletop=getPurpletop;


	function setPurpletop(v){
		this.Purpletop=v;
	}
	this.setPurpletop=setPurpletop;


	this.isPurpletop=function(defaultValue) {
		if(this.Purpletop==null)return defaultValue;
		if(this.Purpletop=="1" || this.Purpletop==true)return true;
		return false;
	}

	this.Senttolab=null;


	function getSenttolab() {
		return this.Senttolab;
	}
	this.getSenttolab=getSenttolab;


	function setSenttolab(v){
		this.Senttolab=v;
	}
	this.setSenttolab=setSenttolab;


	this.isSenttolab=function(defaultValue) {
		if(this.Senttolab==null)return defaultValue;
		if(this.Senttolab=="1" || this.Senttolab==true)return true;
		return false;
	}

	this.Senttolabdetails=null;


	function getSenttolabdetails() {
		return this.Senttolabdetails;
	}
	this.getSenttolabdetails=getSenttolabdetails;


	function setSenttolabdetails(v){
		this.Senttolabdetails=v;
	}
	this.setSenttolabdetails=setSenttolabdetails;

	this.Complications=null;


	function getComplications() {
		return this.Complications;
	}
	this.getComplications=getComplications;


	function setComplications(v){
		this.Complications=v;
	}
	this.setComplications=setComplications;


	this.isComplications=function(defaultValue) {
		if(this.Complications==null)return defaultValue;
		if(this.Complications=="1" || this.Complications==true)return true;
		return false;
	}

	this.Complicationsdetails=null;


	function getComplicationsdetails() {
		return this.Complicationsdetails;
	}
	this.getComplicationsdetails=getComplicationsdetails;


	function setComplicationsdetails(v){
		this.Complicationsdetails=v;
	}
	this.setComplicationsdetails=setComplicationsdetails;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="tissCollData"){
				return this.Tisscolldata ;
			} else 
			if(xmlPath.startsWith("tissCollData")){
				xmlPath=xmlPath.substring(12);
				if(xmlPath=="")return this.Tisscolldata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Tisscolldata!=undefined)return this.Tisscolldata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="tumorLocs/tumorLoc"){
				return this.Tumorlocs_tumorloc ;
			} else 
			if(xmlPath.startsWith("tumorLocs/tumorLoc")){
				xmlPath=xmlPath.substring(18);
				if(xmlPath=="")return this.Tumorlocs_tumorloc ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Tumorlocs_tumorloc.length;whereCount++){

					var tempValue=this.Tumorlocs_tumorloc[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Tumorlocs_tumorloc[whereCount]);

					}

				}
				}else{

				whereArray=this.Tumorlocs_tumorloc;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="tumorHemisphere"){
				return this.Tumorhemisphere ;
			} else 
			if(xmlPath=="surgicalExts/surgicalExt"){
				return this.Surgicalexts_surgicalext ;
			} else 
			if(xmlPath.startsWith("surgicalExts/surgicalExt")){
				xmlPath=xmlPath.substring(24);
				if(xmlPath=="")return this.Surgicalexts_surgicalext ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Surgicalexts_surgicalext.length;whereCount++){

					var tempValue=this.Surgicalexts_surgicalext[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Surgicalexts_surgicalext[whereCount]);

					}

				}
				}else{

				whereArray=this.Surgicalexts_surgicalext;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="gliadelUsed"){
				return this.Gliadelused ;
			} else 
			if(xmlPath=="surgeons/surgeon"){
				return this.Surgeons_surgeon ;
			} else 
			if(xmlPath.startsWith("surgeons/surgeon")){
				xmlPath=xmlPath.substring(16);
				if(xmlPath=="")return this.Surgeons_surgeon ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Surgeons_surgeon.length;whereCount++){

					var tempValue=this.Surgeons_surgeon[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Surgeons_surgeon[whereCount]);

					}

				}
				}else{

				whereArray=this.Surgeons_surgeon;
				}

			var typeArray;
				if (options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					return typeArray[index].getProperty(xmlPath);
				}else{
					return null;
				}
			} else 
			if(xmlPath=="bloodColl"){
				return this.Bloodcoll ;
			} else 
			if(xmlPath=="redTop"){
				return this.Redtop ;
			} else 
			if(xmlPath=="purpleTop"){
				return this.Purpletop ;
			} else 
			if(xmlPath=="sentToLab"){
				return this.Senttolab ;
			} else 
			if(xmlPath=="sentToLabDetails"){
				return this.Senttolabdetails ;
			} else 
			if(xmlPath=="complications"){
				return this.Complications ;
			} else 
			if(xmlPath=="complicationsDetails"){
				return this.Complicationsdetails ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="tissCollData"){
				this.Tisscolldata=value;
			} else 
			if(xmlPath.startsWith("tissCollData")){
				xmlPath=xmlPath.substring(12);
				if(xmlPath=="")return this.Tisscolldata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Tisscolldata!=undefined){
					this.Tisscolldata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Tisscolldata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Tisscolldata= instanciateObject("tissue:tissCollData");//omUtils.js
						}
						if(options && options.where)this.Tisscolldata.setProperty(options.where.field,options.where.value);
						this.Tisscolldata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tumorLocs/tumorLoc"){
				this.Tumorlocs_tumorloc=value;
			} else 
			if(xmlPath.startsWith("tumorLocs/tumorLoc")){
				xmlPath=xmlPath.substring(18);
				if(xmlPath=="")return this.Tumorlocs_tumorloc ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Tumorlocs_tumorloc.length;whereCount++){

					var tempValue=this.Tumorlocs_tumorloc[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Tumorlocs_tumorloc[whereCount]);

					}

				}
				}else{

				whereArray=this.Tumorlocs_tumorloc;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("condr:brainCollData_tumorLoc");//omUtils.js
					}
					this.addTumorlocs_tumorloc(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="tumorHemisphere"){
				this.Tumorhemisphere=value;
			} else 
			if(xmlPath=="surgicalExts/surgicalExt"){
				this.Surgicalexts_surgicalext=value;
			} else 
			if(xmlPath.startsWith("surgicalExts/surgicalExt")){
				xmlPath=xmlPath.substring(24);
				if(xmlPath=="")return this.Surgicalexts_surgicalext ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Surgicalexts_surgicalext.length;whereCount++){

					var tempValue=this.Surgicalexts_surgicalext[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Surgicalexts_surgicalext[whereCount]);

					}

				}
				}else{

				whereArray=this.Surgicalexts_surgicalext;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("condr:brainCollData_surgicalExt");//omUtils.js
					}
					this.addSurgicalexts_surgicalext(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="gliadelUsed"){
				this.Gliadelused=value;
			} else 
			if(xmlPath=="surgeons/surgeon"){
				this.Surgeons_surgeon=value;
			} else 
			if(xmlPath.startsWith("surgeons/surgeon")){
				xmlPath=xmlPath.substring(16);
				if(xmlPath=="")return this.Surgeons_surgeon ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				var index=0;
				if(options){
					if(options.index)index=options.index;
				}

			var whereArray;
				if (options && options.where){

				whereArray=new Array();

				for(var whereCount=0;whereCount<this.Surgeons_surgeon.length;whereCount++){

					var tempValue=this.Surgeons_surgeon[whereCount].getProperty(options.where.field);

					if(tempValue!=null)if(tempValue.toString()==options.where.value.toString()){

						whereArray.push(this.Surgeons_surgeon[whereCount]);

					}

				}
				}else{

				whereArray=this.Surgeons_surgeon;
				}

			var typeArray;
				if (options && options.xsiType){

				typeArray=new Array();

				for(var typeCount=0;typeCount<whereArray.length;typeCount++){

					if(whereArray[typeCount].getFullSchemaElementName()==options.xsiType){

						typeArray.push(whereArray[typeCount]);

					}

				}
				}else{

				typeArray=whereArray;
				}
				if (typeArray.length>index){
					typeArray[index].setProperty(xmlPath,value);
				}else{
					var newChild;
					if(options && options.xsiType){
						newChild= instanciateObject(options.xsiType);//omUtils.js
					}else{
						newChild= instanciateObject("condr:brainCollData_surgeon");//omUtils.js
					}
					this.addSurgeons_surgeon(newChild);
					if(options && options.where)newChild.setProperty(options.where.field,options.where.value);
					newChild.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="bloodColl"){
				this.Bloodcoll=value;
			} else 
			if(xmlPath=="redTop"){
				this.Redtop=value;
			} else 
			if(xmlPath=="purpleTop"){
				this.Purpletop=value;
			} else 
			if(xmlPath=="sentToLab"){
				this.Senttolab=value;
			} else 
			if(xmlPath=="sentToLabDetails"){
				this.Senttolabdetails=value;
			} else 
			if(xmlPath=="complications"){
				this.Complications=value;
			} else 
			if(xmlPath=="complicationsDetails"){
				this.Complicationsdetails=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
		if (xmlPath=="tumorLocs/tumorLoc"){
			this.addTumorlocs_tumorloc(v);
		}else if (xmlPath=="surgicalExts/surgicalExt"){
			this.addSurgicalexts_surgicalext(v);
		}else if (xmlPath=="surgeons/surgeon"){
			this.addSurgeons_surgeon(v);
		}
		else{
			this.extension.setReferenceField(xmlPath,v);
		}
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
		if (xmlPath=="tumorLocs/tumorLoc"){
			return "http://nrg.wustl.edu/condr:brainCollData_tumorLoc";
		}else if (xmlPath=="surgicalExts/surgicalExt"){
			return "http://nrg.wustl.edu/condr:brainCollData_surgicalExt";
		}else if (xmlPath=="surgeons/surgeon"){
			return "http://nrg.wustl.edu/condr:brainCollData_surgeon";
		}
		else{
			return this.extension.getReferenceFieldName(xmlPath);
		}
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="tumorLocs/tumorLoc"){
			return "field_inline_repeater";
		}else if (xmlPath=="tumorHemisphere"){
			return "field_data";
		}else if (xmlPath=="surgicalExts/surgicalExt"){
			return "field_inline_repeater";
		}else if (xmlPath=="gliadelUsed"){
			return "field_data";
		}else if (xmlPath=="surgeons/surgeon"){
			return "field_inline_repeater";
		}else if (xmlPath=="bloodColl"){
			return "field_data";
		}else if (xmlPath=="redTop"){
			return "field_data";
		}else if (xmlPath=="purpleTop"){
			return "field_data";
		}else if (xmlPath=="sentToLab"){
			return "field_data";
		}else if (xmlPath=="sentToLabDetails"){
			return "field_LONG_DATA";
		}else if (xmlPath=="complications"){
			return "field_data";
		}else if (xmlPath=="complicationsDetails"){
			return "field_LONG_DATA";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<condr:BrainColl";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</condr:BrainColl>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
			var child0=0;
			var att0=0;
			child0+=this.Tumorlocs_tumorloc.length;
			if(child0>0 || att0>0){
				xmlTxt+="\n<condr:tumorLocs";
			if(child0==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Tumorlocs_tumorlocCOUNT=0;Tumorlocs_tumorlocCOUNT<this.Tumorlocs_tumorloc.length;Tumorlocs_tumorlocCOUNT++){
			xmlTxt+=this.Tumorlocs_tumorloc[Tumorlocs_tumorlocCOUNT].getXMLBody(preventComments);
		}
				xmlTxt+="\n</condr:tumorLocs>";
			}
			}

		if (this.Tumorhemisphere!=null){
			xmlTxt+="\n<condr:tumorHemisphere";
			xmlTxt+=">";
			xmlTxt+=this.Tumorhemisphere.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:tumorHemisphere>";
		}
			var child1=0;
			var att1=0;
			child1+=this.Surgicalexts_surgicalext.length;
			if(child1>0 || att1>0){
				xmlTxt+="\n<condr:surgicalExts";
			if(child1==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Surgicalexts_surgicalextCOUNT=0;Surgicalexts_surgicalextCOUNT<this.Surgicalexts_surgicalext.length;Surgicalexts_surgicalextCOUNT++){
			xmlTxt+=this.Surgicalexts_surgicalext[Surgicalexts_surgicalextCOUNT].getXMLBody(preventComments);
		}
				xmlTxt+="\n</condr:surgicalExts>";
			}
			}

		if (this.Gliadelused!=null){
			xmlTxt+="\n<condr:gliadelUsed";
			xmlTxt+=">";
			xmlTxt+=this.Gliadelused;
			xmlTxt+="</condr:gliadelUsed>";
		}
			var child2=0;
			var att2=0;
			child2+=this.Surgeons_surgeon.length;
			if(child2>0 || att2>0){
				xmlTxt+="\n<condr:surgeons";
			if(child2==0){
				xmlTxt+="/>";
			}else{
				xmlTxt+=">";
		for(var Surgeons_surgeonCOUNT=0;Surgeons_surgeonCOUNT<this.Surgeons_surgeon.length;Surgeons_surgeonCOUNT++){
			xmlTxt+=this.Surgeons_surgeon[Surgeons_surgeonCOUNT].getXMLBody(preventComments);
		}
				xmlTxt+="\n</condr:surgeons>";
			}
			}

		if (this.Bloodcoll!=null){
			xmlTxt+="\n<condr:bloodColl";
			xmlTxt+=">";
			xmlTxt+=this.Bloodcoll;
			xmlTxt+="</condr:bloodColl>";
		}
		if (this.Redtop!=null){
			xmlTxt+="\n<condr:redTop";
			xmlTxt+=">";
			xmlTxt+=this.Redtop;
			xmlTxt+="</condr:redTop>";
		}
		if (this.Purpletop!=null){
			xmlTxt+="\n<condr:purpleTop";
			xmlTxt+=">";
			xmlTxt+=this.Purpletop;
			xmlTxt+="</condr:purpleTop>";
		}
		if (this.Senttolab!=null){
			xmlTxt+="\n<condr:sentToLab";
			xmlTxt+=">";
			xmlTxt+=this.Senttolab;
			xmlTxt+="</condr:sentToLab>";
		}
		if (this.Senttolabdetails!=null){
			xmlTxt+="\n<condr:sentToLabDetails";
			xmlTxt+=">";
			xmlTxt+=this.Senttolabdetails.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:sentToLabDetails>";
		}
		if (this.Complications!=null){
			xmlTxt+="\n<condr:complications";
			xmlTxt+=">";
			xmlTxt+=this.Complications;
			xmlTxt+="</condr:complications>";
		}
		if (this.Complicationsdetails!=null){
			xmlTxt+="\n<condr:complicationsDetails";
			xmlTxt+=">";
			xmlTxt+=this.Complicationsdetails.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</condr:complicationsDetails>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
			if(this.Tumorlocs_tumorloc.length>0)return true;
		if (this.Tumorhemisphere!=null) return true;
			if(this.Surgicalexts_surgicalext.length>0)return true;
		if (this.Gliadelused!=null) return true;
			if(this.Surgeons_surgeon.length>0)return true;
		if (this.Bloodcoll!=null) return true;
		if (this.Redtop!=null) return true;
		if (this.Purpletop!=null) return true;
		if (this.Senttolab!=null) return true;
		if (this.Senttolabdetails!=null) return true;
		if (this.Complications!=null) return true;
		if (this.Complicationsdetails!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
