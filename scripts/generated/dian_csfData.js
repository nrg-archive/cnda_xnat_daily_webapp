/*
 * GENERATED FILE
 * Created on Tue Jul 28 14:41:02 CDT 2015
 *
 */

/**
 * @author XDAT
 *
 */

function dian_csfData(){
this.xsiType="dian:csfData";

	this.getSchemaElementName=function(){
		return "csfData";
	}

	this.getFullSchemaElementName=function(){
		return "dian:csfData";
	}
this.extension=dynamicJSLoad('xnat_subjectAssessorData','generated/xnat_subjectAssessorData.js');

	this.Bldcollsess=null;


	function getBldcollsess() {
		return this.Bldcollsess;
	}
	this.getBldcollsess=getBldcollsess;


	function setBldcollsess(v){
		this.Bldcollsess=v;
	}
	this.setBldcollsess=setBldcollsess;

	this.Tau=null;


	function getTau() {
		return this.Tau;
	}
	this.getTau=getTau;


	function setTau(v){
		this.Tau=v;
	}
	this.setTau=setTau;

	this.Tauunits=null;


	function getTauunits() {
		return this.Tauunits;
	}
	this.getTauunits=getTauunits;


	function setTauunits(v){
		this.Tauunits=v;
	}
	this.setTauunits=setTauunits;

	this.Ptau=null;


	function getPtau() {
		return this.Ptau;
	}
	this.getPtau=getPtau;


	function setPtau(v){
		this.Ptau=v;
	}
	this.setPtau=setPtau;

	this.Ptauunits=null;


	function getPtauunits() {
		return this.Ptauunits;
	}
	this.getPtauunits=getPtauunits;


	function setPtauunits(v){
		this.Ptauunits=v;
	}
	this.setPtauunits=setPtauunits;

	this.Ab40=null;


	function getAb40() {
		return this.Ab40;
	}
	this.getAb40=getAb40;


	function setAb40(v){
		this.Ab40=v;
	}
	this.setAb40=setAb40;

	this.Ab40units=null;


	function getAb40units() {
		return this.Ab40units;
	}
	this.getAb40units=getAb40units;


	function setAb40units(v){
		this.Ab40units=v;
	}
	this.setAb40units=setAb40units;

	this.Ab40lot=null;


	function getAb40lot() {
		return this.Ab40lot;
	}
	this.getAb40lot=getAb40lot;


	function setAb40lot(v){
		this.Ab40lot=v;
	}
	this.setAb40lot=setAb40lot;

	this.Ab40plate=null;


	function getAb40plate() {
		return this.Ab40plate;
	}
	this.getAb40plate=getAb40plate;


	function setAb40plate(v){
		this.Ab40plate=v;
	}
	this.setAb40plate=setAb40plate;

	this.Ab40assaydate=null;


	function getAb40assaydate() {
		return this.Ab40assaydate;
	}
	this.getAb40assaydate=getAb40assaydate;


	function setAb40assaydate(v){
		this.Ab40assaydate=v;
	}
	this.setAb40assaydate=setAb40assaydate;

	this.Ab42=null;


	function getAb42() {
		return this.Ab42;
	}
	this.getAb42=getAb42;


	function setAb42(v){
		this.Ab42=v;
	}
	this.setAb42=setAb42;

	this.Ab42units=null;


	function getAb42units() {
		return this.Ab42units;
	}
	this.getAb42units=getAb42units;


	function setAb42units(v){
		this.Ab42units=v;
	}
	this.setAb42units=setAb42units;

	this.Ab42lot=null;


	function getAb42lot() {
		return this.Ab42lot;
	}
	this.getAb42lot=getAb42lot;


	function setAb42lot(v){
		this.Ab42lot=v;
	}
	this.setAb42lot=setAb42lot;

	this.Ab42plate=null;


	function getAb42plate() {
		return this.Ab42plate;
	}
	this.getAb42plate=getAb42plate;


	function setAb42plate(v){
		this.Ab42plate=v;
	}
	this.setAb42plate=setAb42plate;

	this.Ab42assaydate=null;


	function getAb42assaydate() {
		return this.Ab42assaydate;
	}
	this.getAb42assaydate=getAb42assaydate;


	function setAb42assaydate(v){
		this.Ab42assaydate=v;
	}
	this.setAb42assaydate=setAb42assaydate;

	this.Xmaptau=null;


	function getXmaptau() {
		return this.Xmaptau;
	}
	this.getXmaptau=getXmaptau;


	function setXmaptau(v){
		this.Xmaptau=v;
	}
	this.setXmaptau=setXmaptau;

	this.Xmaptauunits=null;


	function getXmaptauunits() {
		return this.Xmaptauunits;
	}
	this.getXmaptauunits=getXmaptauunits;


	function setXmaptauunits(v){
		this.Xmaptauunits=v;
	}
	this.setXmaptauunits=setXmaptauunits;

	this.Xmaptaulot=null;


	function getXmaptaulot() {
		return this.Xmaptaulot;
	}
	this.getXmaptaulot=getXmaptaulot;


	function setXmaptaulot(v){
		this.Xmaptaulot=v;
	}
	this.setXmaptaulot=setXmaptaulot;

	this.Xmaptauplate=null;


	function getXmaptauplate() {
		return this.Xmaptauplate;
	}
	this.getXmaptauplate=getXmaptauplate;


	function setXmaptauplate(v){
		this.Xmaptauplate=v;
	}
	this.setXmaptauplate=setXmaptauplate;

	this.Xmaptauassaydate=null;


	function getXmaptauassaydate() {
		return this.Xmaptauassaydate;
	}
	this.getXmaptauassaydate=getXmaptauassaydate;


	function setXmaptauassaydate(v){
		this.Xmaptauassaydate=v;
	}
	this.setXmaptauassaydate=setXmaptauassaydate;

	this.Xmapptau181=null;


	function getXmapptau181() {
		return this.Xmapptau181;
	}
	this.getXmapptau181=getXmapptau181;


	function setXmapptau181(v){
		this.Xmapptau181=v;
	}
	this.setXmapptau181=setXmapptau181;

	this.Xmapptau181units=null;


	function getXmapptau181units() {
		return this.Xmapptau181units;
	}
	this.getXmapptau181units=getXmapptau181units;


	function setXmapptau181units(v){
		this.Xmapptau181units=v;
	}
	this.setXmapptau181units=setXmapptau181units;

	this.Xmapptau181lot=null;


	function getXmapptau181lot() {
		return this.Xmapptau181lot;
	}
	this.getXmapptau181lot=getXmapptau181lot;


	function setXmapptau181lot(v){
		this.Xmapptau181lot=v;
	}
	this.setXmapptau181lot=setXmapptau181lot;

	this.Xmapptau181plate=null;


	function getXmapptau181plate() {
		return this.Xmapptau181plate;
	}
	this.getXmapptau181plate=getXmapptau181plate;


	function setXmapptau181plate(v){
		this.Xmapptau181plate=v;
	}
	this.setXmapptau181plate=setXmapptau181plate;

	this.Xmapptau181assaydate=null;


	function getXmapptau181assaydate() {
		return this.Xmapptau181assaydate;
	}
	this.getXmapptau181assaydate=getXmapptau181assaydate;


	function setXmapptau181assaydate(v){
		this.Xmapptau181assaydate=v;
	}
	this.setXmapptau181assaydate=setXmapptau181assaydate;

	this.Xmapab42=null;


	function getXmapab42() {
		return this.Xmapab42;
	}
	this.getXmapab42=getXmapab42;


	function setXmapab42(v){
		this.Xmapab42=v;
	}
	this.setXmapab42=setXmapab42;

	this.Xmapab42units=null;


	function getXmapab42units() {
		return this.Xmapab42units;
	}
	this.getXmapab42units=getXmapab42units;


	function setXmapab42units(v){
		this.Xmapab42units=v;
	}
	this.setXmapab42units=setXmapab42units;

	this.Xmapab42lot=null;


	function getXmapab42lot() {
		return this.Xmapab42lot;
	}
	this.getXmapab42lot=getXmapab42lot;


	function setXmapab42lot(v){
		this.Xmapab42lot=v;
	}
	this.setXmapab42lot=setXmapab42lot;

	this.Xmapab42plate=null;


	function getXmapab42plate() {
		return this.Xmapab42plate;
	}
	this.getXmapab42plate=getXmapab42plate;


	function setXmapab42plate(v){
		this.Xmapab42plate=v;
	}
	this.setXmapab42plate=setXmapab42plate;

	this.Xmapab42assaydate=null;


	function getXmapab42assaydate() {
		return this.Xmapab42assaydate;
	}
	this.getXmapab42assaydate=getXmapab42assaydate;


	function setXmapab42assaydate(v){
		this.Xmapab42assaydate=v;
	}
	this.setXmapab42assaydate=setXmapab42assaydate;


	this.getProperty=function(xmlPath){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				return this.Subjectassessordata ;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined)return this.Subjectassessordata.getProperty(xmlPath);
				else return null;
			} else 
			if(xmlPath=="bldCollSess"){
				return this.Bldcollsess ;
			} else 
			if(xmlPath=="tau"){
				return this.Tau ;
			} else 
			if(xmlPath=="tauUnits"){
				return this.Tauunits ;
			} else 
			if(xmlPath=="ptau"){
				return this.Ptau ;
			} else 
			if(xmlPath=="ptauUnits"){
				return this.Ptauunits ;
			} else 
			if(xmlPath=="Ab40"){
				return this.Ab40 ;
			} else 
			if(xmlPath=="Ab40Units"){
				return this.Ab40units ;
			} else 
			if(xmlPath=="Ab40Lot"){
				return this.Ab40lot ;
			} else 
			if(xmlPath=="Ab40Plate"){
				return this.Ab40plate ;
			} else 
			if(xmlPath=="Ab40AssayDate"){
				return this.Ab40assaydate ;
			} else 
			if(xmlPath=="Ab42"){
				return this.Ab42 ;
			} else 
			if(xmlPath=="Ab42Units"){
				return this.Ab42units ;
			} else 
			if(xmlPath=="Ab42Lot"){
				return this.Ab42lot ;
			} else 
			if(xmlPath=="Ab42Plate"){
				return this.Ab42plate ;
			} else 
			if(xmlPath=="Ab42AssayDate"){
				return this.Ab42assaydate ;
			} else 
			if(xmlPath=="XMAPtau"){
				return this.Xmaptau ;
			} else 
			if(xmlPath=="XMAPtauUnits"){
				return this.Xmaptauunits ;
			} else 
			if(xmlPath=="XMAPtauLot"){
				return this.Xmaptaulot ;
			} else 
			if(xmlPath=="XMAPtauPlate"){
				return this.Xmaptauplate ;
			} else 
			if(xmlPath=="XMAPtauAssayDate"){
				return this.Xmaptauassaydate ;
			} else 
			if(xmlPath=="XMAPptau181"){
				return this.Xmapptau181 ;
			} else 
			if(xmlPath=="XMAPptau181Units"){
				return this.Xmapptau181units ;
			} else 
			if(xmlPath=="XMAPptau181Lot"){
				return this.Xmapptau181lot ;
			} else 
			if(xmlPath=="XMAPptau181Plate"){
				return this.Xmapptau181plate ;
			} else 
			if(xmlPath=="XMAPptau181AssayDate"){
				return this.Xmapptau181assaydate ;
			} else 
			if(xmlPath=="XMAPAb42"){
				return this.Xmapab42 ;
			} else 
			if(xmlPath=="XMAPAb42Units"){
				return this.Xmapab42units ;
			} else 
			if(xmlPath=="XMAPAb42Lot"){
				return this.Xmapab42lot ;
			} else 
			if(xmlPath=="XMAPAb42Plate"){
				return this.Xmapab42plate ;
			} else 
			if(xmlPath=="XMAPAb42AssayDate"){
				return this.Xmapab42assaydate ;
			} else 
			if(xmlPath=="meta"){
				return this.Meta ;
			} else 
			{
				return this.extension.getProperty(xmlPath);
			}
	}


	this.setProperty=function(xmlPath,value){
			if(xmlPath.startsWith(this.getFullSchemaElementName())){
				xmlPath=xmlPath.substring(this.getFullSchemaElementName().length + 1);
			}
			if(xmlPath=="subjectAssessorData"){
				this.Subjectassessordata=value;
			} else 
			if(xmlPath.startsWith("subjectAssessorData")){
				xmlPath=xmlPath.substring(19);
				if(xmlPath=="")return this.Subjectassessordata ;
				if(xmlPath.startsWith("[")){
					if (xmlPath.indexOf("/")>-1){
						var optionString=xmlPath.substring(0,xmlPath.indexOf("/"));
						xmlPath=xmlPath.substring(xmlPath.indexOf("/")+1);
					}else{
						var optionString=xmlPath;
						xmlPath="";
					}
					
					var options = loadOptions(optionString);//omUtils.js
				}else{xmlPath=xmlPath.substring(1);}
				if(this.Subjectassessordata!=undefined){
					this.Subjectassessordata.setProperty(xmlPath,value);
				}else{
						if(options && options.xsiType){
							this.Subjectassessordata= instanciateObject(options.xsiType);//omUtils.js
						}else{
							this.Subjectassessordata= instanciateObject("xnat:subjectAssessorData");//omUtils.js
						}
						if(options && options.where)this.Subjectassessordata.setProperty(options.where.field,options.where.value);
						this.Subjectassessordata.setProperty(xmlPath,value);
				}
			} else 
			if(xmlPath=="bldCollSess"){
				this.Bldcollsess=value;
			} else 
			if(xmlPath=="tau"){
				this.Tau=value;
			} else 
			if(xmlPath=="tauUnits"){
				this.Tauunits=value;
			} else 
			if(xmlPath=="ptau"){
				this.Ptau=value;
			} else 
			if(xmlPath=="ptauUnits"){
				this.Ptauunits=value;
			} else 
			if(xmlPath=="Ab40"){
				this.Ab40=value;
			} else 
			if(xmlPath=="Ab40Units"){
				this.Ab40units=value;
			} else 
			if(xmlPath=="Ab40Lot"){
				this.Ab40lot=value;
			} else 
			if(xmlPath=="Ab40Plate"){
				this.Ab40plate=value;
			} else 
			if(xmlPath=="Ab40AssayDate"){
				this.Ab40assaydate=value;
			} else 
			if(xmlPath=="Ab42"){
				this.Ab42=value;
			} else 
			if(xmlPath=="Ab42Units"){
				this.Ab42units=value;
			} else 
			if(xmlPath=="Ab42Lot"){
				this.Ab42lot=value;
			} else 
			if(xmlPath=="Ab42Plate"){
				this.Ab42plate=value;
			} else 
			if(xmlPath=="Ab42AssayDate"){
				this.Ab42assaydate=value;
			} else 
			if(xmlPath=="XMAPtau"){
				this.Xmaptau=value;
			} else 
			if(xmlPath=="XMAPtauUnits"){
				this.Xmaptauunits=value;
			} else 
			if(xmlPath=="XMAPtauLot"){
				this.Xmaptaulot=value;
			} else 
			if(xmlPath=="XMAPtauPlate"){
				this.Xmaptauplate=value;
			} else 
			if(xmlPath=="XMAPtauAssayDate"){
				this.Xmaptauassaydate=value;
			} else 
			if(xmlPath=="XMAPptau181"){
				this.Xmapptau181=value;
			} else 
			if(xmlPath=="XMAPptau181Units"){
				this.Xmapptau181units=value;
			} else 
			if(xmlPath=="XMAPptau181Lot"){
				this.Xmapptau181lot=value;
			} else 
			if(xmlPath=="XMAPptau181Plate"){
				this.Xmapptau181plate=value;
			} else 
			if(xmlPath=="XMAPptau181AssayDate"){
				this.Xmapptau181assaydate=value;
			} else 
			if(xmlPath=="XMAPAb42"){
				this.Xmapab42=value;
			} else 
			if(xmlPath=="XMAPAb42Units"){
				this.Xmapab42units=value;
			} else 
			if(xmlPath=="XMAPAb42Lot"){
				this.Xmapab42lot=value;
			} else 
			if(xmlPath=="XMAPAb42Plate"){
				this.Xmapab42plate=value;
			} else 
			if(xmlPath=="XMAPAb42AssayDate"){
				this.Xmapab42assaydate=value;
			} else 
			if(xmlPath=="meta"){
				this.Meta=value;
			} else 
			{
				return this.extension.setProperty(xmlPath,value);
			}
	}

	/**
	 * Sets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.setReferenceField=function(xmlPath,v) {
			this.extension.setReferenceField(xmlPath,v);
	}

	/**
	 * Gets the value for a field via the XMLPATH.
	 * @param v Value to Set.
	 */
	this.getReferenceFieldName=function(xmlPath) {
			return this.extension.getReferenceFieldName(xmlPath);
	}

	/**
	 * Returns whether or not this is a reference field
	 */
	this.getFieldType=function(xmlPath){
		if (xmlPath=="bldCollSess"){
			return "field_data";
		}else if (xmlPath=="tau"){
			return "field_data";
		}else if (xmlPath=="tauUnits"){
			return "field_data";
		}else if (xmlPath=="ptau"){
			return "field_data";
		}else if (xmlPath=="ptauUnits"){
			return "field_data";
		}else if (xmlPath=="Ab40"){
			return "field_data";
		}else if (xmlPath=="Ab40Units"){
			return "field_data";
		}else if (xmlPath=="Ab40Lot"){
			return "field_data";
		}else if (xmlPath=="Ab40Plate"){
			return "field_data";
		}else if (xmlPath=="Ab40AssayDate"){
			return "field_data";
		}else if (xmlPath=="Ab42"){
			return "field_data";
		}else if (xmlPath=="Ab42Units"){
			return "field_data";
		}else if (xmlPath=="Ab42Lot"){
			return "field_data";
		}else if (xmlPath=="Ab42Plate"){
			return "field_data";
		}else if (xmlPath=="Ab42AssayDate"){
			return "field_data";
		}else if (xmlPath=="XMAPtau"){
			return "field_data";
		}else if (xmlPath=="XMAPtauUnits"){
			return "field_data";
		}else if (xmlPath=="XMAPtauLot"){
			return "field_data";
		}else if (xmlPath=="XMAPtauPlate"){
			return "field_data";
		}else if (xmlPath=="XMAPtauAssayDate"){
			return "field_data";
		}else if (xmlPath=="XMAPptau181"){
			return "field_data";
		}else if (xmlPath=="XMAPptau181Units"){
			return "field_data";
		}else if (xmlPath=="XMAPptau181Lot"){
			return "field_data";
		}else if (xmlPath=="XMAPptau181Plate"){
			return "field_data";
		}else if (xmlPath=="XMAPptau181AssayDate"){
			return "field_data";
		}else if (xmlPath=="XMAPAb42"){
			return "field_data";
		}else if (xmlPath=="XMAPAb42Units"){
			return "field_data";
		}else if (xmlPath=="XMAPAb42Lot"){
			return "field_data";
		}else if (xmlPath=="XMAPAb42Plate"){
			return "field_data";
		}else if (xmlPath=="XMAPAb42AssayDate"){
			return "field_data";
		}
		else{
			return this.extension.getFieldType(xmlPath);
		}
	}


	this.toXML=function(xmlTxt,preventComments){
		xmlTxt+="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlTxt+="\n<dian:CSF";
		xmlTxt+=this.getXMLAtts();
		xmlTxt+=" xmlns:adir=\"http://nrg.wustl.edu/adir\"";
		xmlTxt+=" xmlns:ados=\"http://nrg.wustl.edu/ados\"";
		xmlTxt+=" xmlns:adrc=\"http://nrg.wustl.edu/adrc\"";
		xmlTxt+=" xmlns:arc=\"http://nrg.wustl.edu/arc\"";
		xmlTxt+=" xmlns:bcl=\"http://nrg.wustl.edu/bcl\"";
		xmlTxt+=" xmlns:behavioral=\"http://nrg.wustl.edu/behavioral\"";
		xmlTxt+=" xmlns:cat=\"http://nrg.wustl.edu/catalog\"";
		xmlTxt+=" xmlns:cbat=\"http://nrg.wustl.edu/cbat\"";
		xmlTxt+=" xmlns:clin=\"http://nrg.wustl.edu/clin\"";
		xmlTxt+=" xmlns:cnda=\"http://nrg.wustl.edu/cnda\"";
		xmlTxt+=" xmlns:cnda_ext=\"http://nrg.wustl.edu/cnda_ext\"";
		xmlTxt+=" xmlns:cog=\"http://nrg.wustl.edu/cog\"";
		xmlTxt+=" xmlns:condr=\"http://nrg.wustl.edu/condr\"";
		xmlTxt+=" xmlns:condr_mets=\"http://nrg.wustl.edu/condr_mets\"";
		xmlTxt+=" xmlns:dian=\"http://nrg.wustl.edu/dian\"";
		xmlTxt+=" xmlns:fs=\"http://nrg.wustl.edu/fs\"";
		xmlTxt+=" xmlns:genetics=\"http://nrg.wustl.edu/genetics\"";
		xmlTxt+=" xmlns:ghf=\"http://nrg.wustl.edu/ghf\"";
		xmlTxt+=" xmlns:ipip=\"http://nrg.wustl.edu/ipip\"";
		xmlTxt+=" xmlns:iq=\"http://nrg.wustl.edu/iq\"";
		xmlTxt+=" xmlns:kblack=\"http://nrg.wustl.edu/wu_kblack\"";
		xmlTxt+=" xmlns:ls2=\"http://nrg.wustl.edu/ls2\"";
		xmlTxt+=" xmlns:mpet=\"http://nrg.wustl.edu/mpet\"";
		xmlTxt+=" xmlns:nihSS=\"http://nrg.wustl.edu/nihSS\"";
		xmlTxt+=" xmlns:opti=\"http://nrg.wustl.edu/opti\"";
		xmlTxt+=" xmlns:pet=\"http://nrg.wustl.edu/pet\"";
		xmlTxt+=" xmlns:pipe=\"http://nrg.wustl.edu/pipe\"";
		xmlTxt+=" xmlns:prov=\"http://www.nbirn.net/prov\"";
		xmlTxt+=" xmlns:pup=\"http://nrg.wustl.edu/pup\"";
		xmlTxt+=" xmlns:rad=\"http://nrg.wustl.edu/rad\"";
		xmlTxt+=" xmlns:scr=\"http://nrg.wustl.edu/scr\"";
		xmlTxt+=" xmlns:sf=\"http://nrg.wustl.edu/sf\"";
		xmlTxt+=" xmlns:srs=\"http://nrg.wustl.edu/srs\"";
		xmlTxt+=" xmlns:tissue=\"http://nrg.wustl.edu/tissue\"";
		xmlTxt+=" xmlns:tx=\"http://nrg.wustl.edu/tx\"";
		xmlTxt+=" xmlns:uds=\"http://nrg.wustl.edu/uds\"";
		xmlTxt+=" xmlns:val=\"http://nrg.wustl.edu/val\"";
		xmlTxt+=" xmlns:visit=\"http://nrg.wustl.edu/visit\"";
		xmlTxt+=" xmlns:wrk=\"http://nrg.wustl.edu/workflow\"";
		xmlTxt+=" xmlns:xdat=\"http://nrg.wustl.edu/security\"";
		xmlTxt+=" xmlns:xnat=\"http://nrg.wustl.edu/xnat\"";
		xmlTxt+=" xmlns:xnat_a=\"http://nrg.wustl.edu/xnat_assessments\"";
		xmlTxt+=" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"";
		xmlTxt+=">";
		xmlTxt+=this.getXMLBody(preventComments)
		xmlTxt+="\n</dian:CSF>";
		return xmlTxt;
	}


	this.getXMLComments=function(preventComments){
		var str ="";
		if((preventComments==undefined || !preventComments) && this.hasXMLComments()){
		}
		return str;
	}


	this.getXMLAtts=function(){
		var attTxt = this.extension.getXMLAtts();
		return attTxt;
	}


	this.getXMLBody=function(preventComments){
		var xmlTxt=this.getXMLComments(preventComments);
		xmlTxt+=this.extension.getXMLBody(preventComments);
		if (this.Bldcollsess!=null){
			xmlTxt+="\n<dian:bldCollSess";
			xmlTxt+=">";
			xmlTxt+=this.Bldcollsess.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:bldCollSess>";
		}
		if (this.Tau!=null){
			xmlTxt+="\n<dian:tau";
			xmlTxt+=">";
			xmlTxt+=this.Tau;
			xmlTxt+="</dian:tau>";
		}
		if (this.Tauunits!=null){
			xmlTxt+="\n<dian:tauUnits";
			xmlTxt+=">";
			xmlTxt+=this.Tauunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:tauUnits>";
		}
		if (this.Ptau!=null){
			xmlTxt+="\n<dian:ptau";
			xmlTxt+=">";
			xmlTxt+=this.Ptau;
			xmlTxt+="</dian:ptau>";
		}
		if (this.Ptauunits!=null){
			xmlTxt+="\n<dian:ptauUnits";
			xmlTxt+=">";
			xmlTxt+=this.Ptauunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:ptauUnits>";
		}
		if (this.Ab40!=null){
			xmlTxt+="\n<dian:Ab40";
			xmlTxt+=">";
			xmlTxt+=this.Ab40;
			xmlTxt+="</dian:Ab40>";
		}
		if (this.Ab40units!=null){
			xmlTxt+="\n<dian:Ab40Units";
			xmlTxt+=">";
			xmlTxt+=this.Ab40units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab40Units>";
		}
		if (this.Ab40lot!=null){
			xmlTxt+="\n<dian:Ab40Lot";
			xmlTxt+=">";
			xmlTxt+=this.Ab40lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab40Lot>";
		}
		if (this.Ab40plate!=null){
			xmlTxt+="\n<dian:Ab40Plate";
			xmlTxt+=">";
			xmlTxt+=this.Ab40plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab40Plate>";
		}
		if (this.Ab40assaydate!=null){
			xmlTxt+="\n<dian:Ab40AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Ab40assaydate;
			xmlTxt+="</dian:Ab40AssayDate>";
		}
		if (this.Ab42!=null){
			xmlTxt+="\n<dian:Ab42";
			xmlTxt+=">";
			xmlTxt+=this.Ab42;
			xmlTxt+="</dian:Ab42>";
		}
		if (this.Ab42units!=null){
			xmlTxt+="\n<dian:Ab42Units";
			xmlTxt+=">";
			xmlTxt+=this.Ab42units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab42Units>";
		}
		if (this.Ab42lot!=null){
			xmlTxt+="\n<dian:Ab42Lot";
			xmlTxt+=">";
			xmlTxt+=this.Ab42lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab42Lot>";
		}
		if (this.Ab42plate!=null){
			xmlTxt+="\n<dian:Ab42Plate";
			xmlTxt+=">";
			xmlTxt+=this.Ab42plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:Ab42Plate>";
		}
		if (this.Ab42assaydate!=null){
			xmlTxt+="\n<dian:Ab42AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Ab42assaydate;
			xmlTxt+="</dian:Ab42AssayDate>";
		}
		if (this.Xmaptau!=null){
			xmlTxt+="\n<dian:XMAPtau";
			xmlTxt+=">";
			xmlTxt+=this.Xmaptau;
			xmlTxt+="</dian:XMAPtau>";
		}
		if (this.Xmaptauunits!=null){
			xmlTxt+="\n<dian:XMAPtauUnits";
			xmlTxt+=">";
			xmlTxt+=this.Xmaptauunits.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPtauUnits>";
		}
		if (this.Xmaptaulot!=null){
			xmlTxt+="\n<dian:XMAPtauLot";
			xmlTxt+=">";
			xmlTxt+=this.Xmaptaulot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPtauLot>";
		}
		if (this.Xmaptauplate!=null){
			xmlTxt+="\n<dian:XMAPtauPlate";
			xmlTxt+=">";
			xmlTxt+=this.Xmaptauplate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPtauPlate>";
		}
		if (this.Xmaptauassaydate!=null){
			xmlTxt+="\n<dian:XMAPtauAssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Xmaptauassaydate;
			xmlTxt+="</dian:XMAPtauAssayDate>";
		}
		if (this.Xmapptau181!=null){
			xmlTxt+="\n<dian:XMAPptau181";
			xmlTxt+=">";
			xmlTxt+=this.Xmapptau181;
			xmlTxt+="</dian:XMAPptau181>";
		}
		if (this.Xmapptau181units!=null){
			xmlTxt+="\n<dian:XMAPptau181Units";
			xmlTxt+=">";
			xmlTxt+=this.Xmapptau181units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPptau181Units>";
		}
		if (this.Xmapptau181lot!=null){
			xmlTxt+="\n<dian:XMAPptau181Lot";
			xmlTxt+=">";
			xmlTxt+=this.Xmapptau181lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPptau181Lot>";
		}
		if (this.Xmapptau181plate!=null){
			xmlTxt+="\n<dian:XMAPptau181Plate";
			xmlTxt+=">";
			xmlTxt+=this.Xmapptau181plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPptau181Plate>";
		}
		if (this.Xmapptau181assaydate!=null){
			xmlTxt+="\n<dian:XMAPptau181AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Xmapptau181assaydate;
			xmlTxt+="</dian:XMAPptau181AssayDate>";
		}
		if (this.Xmapab42!=null){
			xmlTxt+="\n<dian:XMAPAb42";
			xmlTxt+=">";
			xmlTxt+=this.Xmapab42;
			xmlTxt+="</dian:XMAPAb42>";
		}
		if (this.Xmapab42units!=null){
			xmlTxt+="\n<dian:XMAPAb42Units";
			xmlTxt+=">";
			xmlTxt+=this.Xmapab42units.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPAb42Units>";
		}
		if (this.Xmapab42lot!=null){
			xmlTxt+="\n<dian:XMAPAb42Lot";
			xmlTxt+=">";
			xmlTxt+=this.Xmapab42lot.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPAb42Lot>";
		}
		if (this.Xmapab42plate!=null){
			xmlTxt+="\n<dian:XMAPAb42Plate";
			xmlTxt+=">";
			xmlTxt+=this.Xmapab42plate.replace(/>/g,"&gt;").replace(/</g,"&lt;");
			xmlTxt+="</dian:XMAPAb42Plate>";
		}
		if (this.Xmapab42assaydate!=null){
			xmlTxt+="\n<dian:XMAPAb42AssayDate";
			xmlTxt+=">";
			xmlTxt+=this.Xmapab42assaydate;
			xmlTxt+="</dian:XMAPAb42AssayDate>";
		}
		return xmlTxt;
	}


	this.hasXMLComments=function(){
	}


	this.hasXMLBodyContent=function(){
		if (this.Bldcollsess!=null) return true;
		if (this.Tau!=null) return true;
		if (this.Tauunits!=null) return true;
		if (this.Ptau!=null) return true;
		if (this.Ptauunits!=null) return true;
		if (this.Ab40!=null) return true;
		if (this.Ab40units!=null) return true;
		if (this.Ab40lot!=null) return true;
		if (this.Ab40plate!=null) return true;
		if (this.Ab40assaydate!=null) return true;
		if (this.Ab42!=null) return true;
		if (this.Ab42units!=null) return true;
		if (this.Ab42lot!=null) return true;
		if (this.Ab42plate!=null) return true;
		if (this.Ab42assaydate!=null) return true;
		if (this.Xmaptau!=null) return true;
		if (this.Xmaptauunits!=null) return true;
		if (this.Xmaptaulot!=null) return true;
		if (this.Xmaptauplate!=null) return true;
		if (this.Xmaptauassaydate!=null) return true;
		if (this.Xmapptau181!=null) return true;
		if (this.Xmapptau181units!=null) return true;
		if (this.Xmapptau181lot!=null) return true;
		if (this.Xmapptau181plate!=null) return true;
		if (this.Xmapptau181assaydate!=null) return true;
		if (this.Xmapab42!=null) return true;
		if (this.Xmapab42units!=null) return true;
		if (this.Xmapab42lot!=null) return true;
		if (this.Xmapab42plate!=null) return true;
		if (this.Xmapab42assaydate!=null) return true;
		if(this.hasXMLComments())return true;
		if(this.extension.hasXMLBodyContent())return true;
		return false;
	}
}
