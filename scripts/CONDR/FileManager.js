function FileManager(instanceNameArg) {
	"use strict";
	
	this.projectId = "";
	this.subjectId = "";
	
	this.instanceName = instanceNameArg;
	this.restGetURL = "";
	this.restUploadURL = ""; // ??
	
	this.fileVarName = "";
	this.resourceDirectory = ""; // screen shots, path images, etc.
	this.containerDiv = ""; // brainSpecDiv_XXX
	this.label = ""; // Screen Shots

	this.imageExtentions = ["jpg", "jpeg", "gif", "png", "bmp"];

	this.canDelete = false;
	this.canEdit = false;
	// ADDED
	this.deleteIcon = ""; // $deleteIcon
	this.brainDataRowHeight = new Array();
	
	//ADDED
	
	this.uploadFile = function (specId) {
		this.uploadFileHandler = {
			//handle upload case. This function will be invoked after file upload is finished.
			upload: function (response) {
				this.toggleCell(specId, false);
				this.getFiles(specId);
			},
			scope: this
		};

		var formObject = document.getElementById(this.fileVarName + "FileUploadForm_" + specId);
		var fileObject = document.getElementById(this.fileVarName + "File_" + specId);
		var fileName = fileObject.value.split('\\')[fileObject.value.split('\\').length - 1];
		var fileCaption = document.getElementById(this.fileVarName + "Caption_" + specId).value;
		var restURL = "/REST/projects/" + this.projectId + "/subjects/" + this.subjectId + "/experiments/" + specId + "/resources/" + this.resourceDirectory + "/files/" + fileName + "?content=" + fileCaption;
		restURL += "&XNAT_CSRF=" + csrfToken;
	   // argument form can be the id or name attribute value of the HTML form, or an HTML form object.
	   // the second argument is true to indicate file upload.

		YAHOO.util.Connect.setForm(formObject, true);
		YAHOO.util.Connect.asyncRequest("PUT", serverRoot + restURL, this.uploadFileHandler, null, this);
	};

	this.updateUploadFile = function (realFileInput, brainSpecId) {
		document.getElementById(this.fileVarName + "File_" + brainSpecId).value = realFileInput.value;
	}
	
	
	this.buildUploadFileForm = function (contents) {
		var uploadForm = document.createElement('form');
		uploadForm.action = "javascript:" + this.instanceName + ".uploadFile('" + contents['condr:braindata/id'] + "')";
		uploadForm.enctype = "multipart/form-data";
		uploadForm.method = "POST";
		uploadForm.id = this.fileVarName + "FileUploadForm_" + contents['condr:braindata/id'];
		uploadForm.className = "uploadForm"
		uploadForm.XNAT_CSRF = csrfToken;
		
		var uploadFormContainer = document.createElement('div');
		uploadFormContainer.className = "uploadContainer";
		
		var uploadFile = document.createElement('input');
		uploadFile.type = "file";
		uploadFile.className = "uploadFileReal";
		uploadFile.id = this.fileVarName + "File_" + contents['condr:braindata/id'];
		uploadFile.name = this.fileVarName + "File_" + contents['condr:braindata/id'];
		
		if (uploadFile.addEventListener) {
			eval("uploadFile.addEventListener('change', function() {" + this.instanceName + ".updateUploadFile(this, '" + contents['condr:braindata/id'] + "')}, false)");
		} else {
			eval("uploadFile.attachEvent('onchange', function() {" + this.instanceName + ".updateUploadFile(this, '" + contents['condr:braindata/id'] + "')})");
		}
		
		var uploadFileFakeDiv = document.createElement('div');
		uploadFileFakeDiv.className = "uploadFileFake";
		
		var fakeFileInput = document.createElement('input');
		fakeFileInput.type = "text";
		fakeFileInput.id = this.fileVarName + "FakeFileInput_" + contents['condr:braindata/id'];
		fakeFileInput.className = "fakeFileInput";

		var uploadIcon = document.createElement('img');
		uploadIcon.src = serverRoot + "/images/up18.gif";
		uploadIcon.className = "uploadButton";
		
		uploadForm.appendChild(uploadFormContainer);
		uploadFormContainer.appendChild(uploadFile);
		uploadFormContainer.appendChild(uploadFileFakeDiv);
		uploadFileFakeDiv.appendChild(fakeFileInput);
		uploadFileFakeDiv.appendChild(uploadIcon);
		
		return uploadForm;
	};

	this.buildFileTable = function (contents) {
		var table = document.createElement('table');
		table.className = "fileImageTable"
		
		var tableHead = document.createElement('thead');
		table.appendChild(tableHead);
		
		var headRow = document.createElement('tr');
		var headCell = document.createElement('th');
		headCell.innerHTML = this.label;
		headRow.appendChild(headCell);
		tableHead.appendChild(headRow);
		
//SHOULD BE REPLACED WITH VELOCITY TO COMPLETELY REMOVE THIS CODE. MUCH SAFER
		if (this.canEdit) {
			var row = document.createElement('tr');
			row.id = this.fileVarName + "UploadButtonTR_" + contents["condr:braindata/id"];
			
			var cell = document.createElement('td');		
			cell.align = "center";
			row.appendChild(cell);
			
			var buttonInput = document.createElement('input');
			buttonInput.type = "button";
			buttonInput.value = "Upload";
			
			if (buttonInput.addEventListener) {
				eval("buttonInput.addEventListener('click', function() {" + this.instanceName + ".toggleCell('" + contents['condr:braindata/id'] + "', true)}, false)");
			} else {
				eval("buttonInput.attachEvent('onclick', function() {" + this.instanceName + ".toggleCell('" + contents['condr:braindata/id'] + "', true)})");
			}		
			
			cell.appendChild(buttonInput);

			tableHead.appendChild(row);
			
			row = document.createElement('tr');
			row.id = this.fileVarName + "FileBrowseTR_" + contents["condr:braindata/id"];
			
			cell = document.createElement('td');
			
			var form = this.buildUploadFileForm(contents);

			cell.appendChild(form);
			
			row.appendChild(cell);
			tableHead.appendChild(row);
		
			row = document.createElement('tr');
			row.id = this.fileVarName + "CaptionTitleTR_" + contents["condr:braindata/id"];
			var td = document.createElement('td');
			td.className = "label";
			td.align = "center";
		
			tableHead.appendChild(row);
			row.appendChild(td);
			td.innerHTML = "Caption";
				
			row = document.createElement('tr');
			row.id = this.fileVarName + "CaptionInputTR_" + contents["condr:braindata/id"];
			td = document.createElement('td');
			
			var fileCaption = document.createElement('input');
			fileCaption.type = "text";
			fileCaption.id = this.fileVarName + "Caption_" + contents['condr:braindata/id'];
			fileCaption.className = "fileTextInput";
			
			tableHead.appendChild(row);
			row.appendChild(td);
			td.appendChild(fileCaption);
			
			row = document.createElement('tr');
			row.id = this.fileVarName + "FileActionTR_" + contents["condr:braindata/id"];
			td = document.createElement('td');
			
			var uploadButton = document.createElement('input');
			uploadButton.type = "button";
			uploadButton.value = "Upload";
			uploadButton.className = "fileActionButton";
			
			if (uploadButton.addEventListener) {
				eval("uploadButton.addEventListener('click', function() {" + this.instanceName + ".uploadFile('" + contents['condr:braindata/id'] + "')}, false)");
			} else {
				eval("uploadButton.attachEvent('onclick', function() {" + this.instanceName + ".uploadFile('" + contents['condr:braindata/id'] + "')})");
			}
					
			var cancelButton = document.createElement('input');
			cancelButton.type = "button";
			cancelButton.value = "Cancel";
			cancelButton.className = "fileActionButton";
			
			if (cancelButton.addEventListener) {
				eval("cancelButton.addEventListener('click', function() {" + this.instanceName + ".toggleCell('" + contents['condr:braindata/id'] + "', false)}, false)");
			} else {
				eval("cancelButton.attachEvent('onclick', function() {" + this.instanceName + ".toggleCell('" + contents['condr:braindata/id'] + "', false)})");
			}
						
			tableHead.appendChild(row);
			row.appendChild(td);
			td.appendChild(uploadButton);
			td.appendChild(cancelButton);
		}
		
		var tableBody = document.createElement('tbody');
		tableBody.id = this.fileVarName + "FileBody_" + contents["condr:braindata/id"];
		table.appendChild(tableBody);
		
		return table;
	};
	
	this.getFiles = function (specId) {
		this.getFilesCallback = {
			cache: false,
			success: function (o) {this.getFilesSuccess(o, specId); },
			failure: this.getFilesFailed,
			scope: this
		};
			
		var restURL = "/REST/experiments/" + specId + "/resources/" + this.resourceDirectory + "/files?format=json";
		YAHOO.util.Connect.asyncRequest('GET', serverRoot + restURL, this.getFilesCallback, null, this);
	};

	this.getFilesSuccess = function (o, specId) {
		var jsonResponse;
		
		try {
			jsonResponse = YAHOO.lang.JSON.parse(o.responseText);
		} catch (x) {
			alert("Error: Unable to load screen shots."); 
			return; 
		}
		
		var contents = jsonResponse.ResultSet.Result;

		var tbody = document.getElementById(this.fileVarName + "FileBody_" + specId);
		this.clearChildren(this.fileVarName + "FileBody_" + specId);
		var fileIconDiv = document.getElementById(this.fileVarName + "IconDiv_" + specId);
		fileIconDiv.className = "smallButton noImage";
		
		var i;

		for (i = 0; i < contents.length; i++) {
			var URI = contents[i].URI;
			
			fileIconDiv.className = "smallButton hasImage";
			var row = document.createElement('tr');

			var cell = document.createElement('td');
			cell.style.position = "relative"; // QUESTIONABLE
			cell.align = "center";

			var imageContainerDiv = document.createElement('div');
			imageContainerDiv.className = "fileContainer";
			
			var fileName = URI.split("/")[URI.split("/").length - 1];
			var fileExtension = fileName.split(".")[fileName.split(".").length - 1];
				
			var deleteButtonImg = document.createElement('img');
			deleteButtonImg.src = serverRoot + this.deleteIcon;
			deleteButtonImg.border = "0";
			
			var fileLink = document.createElement('a');
			fileLink.className = "mediaLink";
			fileLink.target = "_blank";
			fileLink.border = "0";
			fileLink.href = serverRoot + URI;
			
			var captionText = document.createTextNode(contents[i].file_content); 
			
			var fileObject;
			
// NOT ID FRIENDLY			if (this.imageExtentions.indexOf(fileExtension.toLowerCase()) != -1)
			if (this.indexOf(this.imageExtentions, fileExtension.toLowerCase()) !== -1) {
				fileObject = document.createElement('img');

				if (fileObject.addEventListener) {
					eval("fileObject.addEventListener('load', function() {" + this.instanceName + ".resizeContainerHeight('" + specId + "')}, false)");
				} else {
					eval("fileObject.attachEvent('onload', function() {" + this.instanceName + ".resizeContainerHeight('" + specId + "')})");
				}
				
				fileObject.className = "thumbnailImg";
				
				fileObject.src = serverRoot + URI;
				
				deleteButtonImg.className = "deleteImageImg";
			} else {
				deleteButtonImg.className = "deleteFileImg";
				
				fileObject = document.createTextNode(fileName);
			}
			
			cell.appendChild(imageContainerDiv);

			if (this.canDelete) {
				var deleteButtonLink = document.createElement('a');			
				deleteButtonLink.href = "javascript:" + this.instanceName + ".deleteFile('" + specId + "','" + fileName + "')";
				
				deleteButtonLink.appendChild(deleteButtonImg);
				imageContainerDiv.appendChild(deleteButtonLink);
			}
			imageContainerDiv.appendChild(fileLink);

			fileLink.appendChild(fileObject);
			
			row.appendChild(cell)
			tbody.appendChild(row);
			
			row = document.createElement('tr');
			cell = document.createElement('td');
			cell.appendChild(captionText);
			cell.className = "captionTD";
			
			row.appendChild(cell);
			tbody.appendChild(row);
		}
		
		var hiddenDiv = document.getElementById(this.containerDiv + "_" + specId);
		var startingHeight = hiddenDiv.offsetHeight;
		
		if (!this.brainDataRowHeight[specId]) {
			this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
		} else {
			hiddenDiv.style.cssText = "";
			
			if (this.brainDataRowHeight[specId] < hiddenDiv.offsetHeight) {
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			}
			
			hiddenDiv.style.overflow = "hidden";
			if (startingHeight === "0") {
				hiddenDiv.style.height = '1px';
				hiddenDiv.style.display = "none";
			} else {
				hiddenDiv.style.height = startingHeight + 'px';
				hiddenDiv.style.display = "block";
			}
		}
	
		this.toggleCell(specId, false);
		
		this.resizeContainerHeight(specId);
	};

	this.getFilesFailed = function (o) {
		alert("Failed to load files");
	};
	
	this.deleteFile = function (specId, fileName) {
		var confirmDelete = confirm("Are you sure you want to delete the file?");
		
		if (confirmDelete) {
			this.deleteFileCallback = {
				success: function (o) {this.deleteFileSuccess(o, specId); },
				failure: this.deleteFileFailed,
				scope: this
			};
			
			var restURL = "/REST/experiments/" + specId + "/resources/" + this.resourceDirectory + "/files/" + fileName;
			restURL += "?XNAT_CSRF=" + csrfToken;

			YAHOO.util.Connect.asyncRequest('DELETE', serverRoot + restURL, this.deleteFileCallback, null, this);
		}
	};
	
	this.deleteFileSuccess = function (o, specId) {
		
		this.getFiles(specId);
	};
	
	this.deleteFileFailed = function (o) {
		alert("Error: Failed to delete file.");
	};
	
	this.toggleCell = function (specId, toggleArg) {
//		var hiddenDiv;
//		var cell;
		//alert(specId);
//		var togTxt = this.fileVarName + "FormDiv_" + specId;
//		var dispTxt = "displayText" + specId;
//		var ele = document.getElementById(togTxt);
//		var text = document.getElementById(dispTxt);

		if (toggleArg) {
			document.getElementById(this.fileVarName + "UploadButtonTR_" + specId).className = "uploadTRHidden";
			document.getElementById(this.fileVarName + "FileBrowseTR_" + specId).className = "uploadTRVisible";
			document.getElementById(this.fileVarName + "CaptionTitleTR_" + specId).className = "uploadTRVisible";
			document.getElementById(this.fileVarName + "CaptionInputTR_" + specId).className = "uploadTRVisible";
			document.getElementById(this.fileVarName + "FileActionTR_" + specId).className = "uploadTRVisible";
		} else {
			document.getElementById(this.fileVarName + "UploadButtonTR_" + specId).className = "uploadTRVisible";
			document.getElementById(this.fileVarName + "FileBrowseTR_" + specId).className = "uploadTRHidden";
			document.getElementById(this.fileVarName + "CaptionTitleTR_" + specId).className = "uploadTRHidden";
			document.getElementById(this.fileVarName + "CaptionInputTR_" + specId).className = "uploadTRHidden";
			document.getElementById(this.fileVarName + "FileActionTR_" + specId).className = "uploadTRHidden";
		}

return;


		if (ele.style.display === "block") {
			cell = document.getElementById(this.fileVarName + "FormCell_" + specId);
			cell.style.display = "block";
			
			ele.style.display = "none";
//			text.innerHTML = "Attach image";
			
			hiddenDiv = document.getElementById(this.containerDiv + "_" + specId);
				
			if (!this.brainDataRowHeight[specId]) {
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			} else {
				hiddenDiv.style.cssText = "";
				
				//if (this.brainDataRowHeight[specId] < hiddenDiv.offsetHeight)
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			}
			hiddenDiv.style.overflow = "hidden";
			hiddenDiv.style.height = this.brainDataRowHeight[specId] + 'px';
			
			document.getElementById(this.fileVarName + "FormDiv_" + specId).style.display = "none";
			document.getElementById(this.fileVarName + "File_" + specId).value = "";
			document.getElementById(this.fileVarName + "Caption_" + specId).value = "";
		} else {
			cell = document.getElementById(this.fileVarName + "FormCell_" + specId);
			cell.style.display = "none";
			
			ele.style.display = "block";
//			text.innerHTML = "Hide attach";
			
			hiddenDiv = document.getElementById(this.containerDiv + "_" + specId);
				
			if (!this.brainDataRowHeight[specId]) {
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			} else {
				hiddenDiv.style.cssText = "";
					
				//if (this.brainDataRowHeight[specId] < hiddenDiv.offsetHeight)
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			}
			hiddenDiv.style.overflow = "hidden";
			hiddenDiv.style.height = this.brainDataRowHeight[specId] + 'px';
			hiddenDiv.style.display = "block"; // TESTIMAGES
			
			document.getElementById(this.fileVarName + "FormDiv_" + specId).style.display = "block";
			
			document.getElementById(this.fileVarName + "File_" + specId).value = "";
			document.getElementById(this.fileVarName + "Caption_" + specId).value = "";
		}
	};
	
	this.resizeContainerHeight = function (specId) {
//			var cell = document.getElementById(this.fileVarName + "FormCell_" + specId);
//			cell.setAttribute("style", "display: block;");
//			
//			ele.style.display = "none";
//			text.innerHTML = "Attach image";
			
		var hiddenDiv = document.getElementById(this.containerDiv + "_" + specId);
			
		if (hiddenDiv.offsetHeight > 0) {
			if (!this.brainDataRowHeight[specId]) {
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			} else {
				hiddenDiv.style.cssText = "";
				
				//if (this.brainDataRowHeight[specId] < hiddenDiv.offsetHeight)
				this.brainDataRowHeight[specId] = hiddenDiv.offsetHeight;
			}
			hiddenDiv.style.overflow = "hidden";
			hiddenDiv.style.height = this.brainDataRowHeight[specId] + 'px';
		}
	};

	this.indexOf = function (hayStack, needle) {
		var index = -1;
		var i = 0;
		for (i = 0; i < hayStack.length; i++) {
			if (hayStack[i] === needle) {
				index = 1;
				break;
			}
		}
	
		return index;
	};

	this.clearChildren = function (elementId) {	
		var element = document.getElementById(elementId);
		
		while(element.hasChildNodes()) {
			element.removeChild(element.firstChild);
		}
	};
}
